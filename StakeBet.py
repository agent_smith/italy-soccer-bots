# -*- coding: utf-8 -*-
import cookielib
from cookielib import CookieJar
import copy
import sys
import os
import codecs
import mysql.connector
import json
import datetime
import pytz
import requests
import time
import re
import random
import urllib
import urllib2
from BeautifulSoup import *
import hashlib
import threading
import urlparse
import dateparser
from fractions import Fraction
import ssl
from xml.dom import minidom
import dateparser
import distance
reload(sys)
sys.setdefaultencoding('utf-8')
class Stakebet(threading.Thread):
    def __init__(self,settings,lock,proxies):
        threading.Thread.__init__(self)
        self.db = mysql.connector.connect(user=settings['username'], password=settings['password'], database=settings['database'])
        self.lock = lock
        self.all_proxies = proxies
        cur = self.db.cursor()
        cur.execute("SET NAMES 'utf8'") 
        cur.execute("SET CHARACTER SET utf8") 
        cur.close()
        self._stop = threading.Event()
        self.all_odds = dict()
        self.matched_events = dict()
    
    def stop(self):
        self._stop.set()

    def loadmatchedevents(self):
        sql = "select betfair,bookie from matched_events where bookie_name like '{0}'".format(self.__class__.__name__.lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        if(len(rows)>0):
            for row in rows:
                if(row[1] not in self.matched_events):
                    self.matched_events[row[1]] = row[0]
                    
        return self.matched_events
        
    def setbookie(self):
        sql = "select id,name,full_name,website,coupon_url from providers where name like '{0}'".format(self.__class__.__name__.lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        self.bookie = dict()
        self.bookie["id"] = str(rows[0][0])
        self.bookie["name"] = str(rows[0][1])
        self.bookie["full_name"] = str(rows[0][2])
        self.bookie["website"] = str(rows[0][3])
        self.bookie["coupon_url"] = str(rows[0][4])
        
    def stopped(self):
        return self._stop.isSet()
    
    def getbetfairmatches(self):
        sql = "select o.id,name,home,away,open_date,home_odds,home_price,away_price,draw_price,o_25,o_25_price,u_25,u_25_price,goal,goal_price,no_goal,no_goal_price,country_code,competition,away_odds,draw_odds,market_id,market_id2,market_id3 from bf_match_odds as o natural join bf_events as e"
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        bf_data = []
        for row in rows:
            bf = dict()
            bf["id"] = str(row[0])
            bf["name"] = row[1]
            bf["home"] = row[2]
            bf["away"] = row[3]
            bf["open_date"] = str(row[4])
            bf["home_odds"] = str(row[5])
            bf["home_price"] = str(row[6])
            bf["away_price"] = str(row[7])
            bf["draw_price"] = str(row[8])
            bf["o_25"] = str(row[9])
            bf["o_25_price"] = str(row[10])
            bf["u_25"] = str(row[11])
            bf["u_25_price"] = str(row[12])
            bf["goal"] = str(row[13])
            bf["goal_price"] = str(row[14])
            bf["no_goal"] = str(row[15])
            bf["no_goal_price"] = str(row[16])
            bf["country_code"] = str(row[17])
            bf["competition"] = str(row[18])
            bf["away_odds"] = str(row[19])
            bf["draw_odds"] = str(row[20])
            bf["market_id"] = str(row[21])
            bf["market_id2"] = str(row[22])
            bf["market_id3"] = str(row[23])
            bf_data.append(bf)
        return bf_data
            
    def run(self):
        self.setbookie()
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        #all_leagues = ['682', '21', '50', '9', '326', '903', '151', '256', '287', '93', '215', '271', '445', '64', '60', '221', '475', '344', '1', '53', '265', '823', '361', '2', '404', '283', '430', '24', '763', '408', '145', '282', '541', '858', '310', '312', '367', '851', '301', '302', '370', '453', '260', '121', '150', '92', '440', '426', '212', '480', '540', '405', '406', '47', '308', '598', '119', '96', '7', '625', '369', '349', '6', '136', '85', '619', '298', '305', '309', '311', '277', '407', '259', '353', '63', '556', '431', '427', '488', '133']
        all_leagues = dict()
        url = "https://www.stakebet.it/sport-lite.html"
        self.cj = CookieJar()
        req = urllib2.Request(url)
        x = random.randint(0,79)
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Connection', 'keep-alive')]            
        resp = self.opener.open(req) 
        resp = resp.read()
        matches = re.findall(r'(boot_params.menu.*[^;]+);',resp)
        resp = json.loads(matches[0].split('=')[1].replace("boot_params.codes","").replace(";","").strip())
        for sport in resp["lista"]:
            if(sport["name"]=="CALCIO"):
                for c in sport["events"].keys():
                    for l in sport["events"][c]:
                        try:
                            league = "{0}@{1}".format(c,l["name"].replace(" ","%20"))
                            all_leagues[str(l["id"])]=league
                        except:
                            pass
                        
                        
        blacklist=[]        
        while self._stop.isSet() == False:
            SQL = ""
            all_events = []
            self.cj = CookieJar()
            x = random.randint(0,79)
            self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx),urllib2.HTTPCookieProcessor(self.cj),urllib2.ProxyHandler({'http': 'http://ninjabet:NINJAfrancavilla1987@'+self.all_proxies[x]}))
            self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Connection', 'keep-alive')]            
            url = "https://www.stakebet.it/sport-lite.html"
            req = urllib2.Request(url)
            resp = self.opener.open(req)
            try:
                for key,value in all_leagues.iteritems():
                    
                    try:
                        if(self._stop.isSet()):
                            return
                        time.sleep(random.randint(1,5))
                        postdata = '{"data":null,"range":[],"bonus_id":-1}'
                        url = "https://www.stakebet.it/scommesse/lista/{0}/1/{1}?limiTemp=0".format(value,key)
                        
                        req = urllib2.Request(url,data=postdata,headers={'Content-Type': 'application/json;charset=UTF-8','Referer':'https://www.stakebet.it/sport-lite.html','Accept':'application/json, text/plain, */*'})
                        x = random.randint(0,79)
                        self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx),urllib2.HTTPCookieProcessor(self.cj),urllib2.ProxyHandler({'http': 'http://ninjabet:NINJAfrancavilla1987@'+self.all_proxies[x]}))
                        self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://www.stakebet.it/sport-lite.html'),('Connection', 'keep-alive')]            
                        resp = self.opener.open(req) 
                        resp = resp.read()
                        resp = json.loads(resp)
                        
                        if(resp["events"]==None or len(resp["events"])==0):
                            continue
                        betfair_data = self.getbetfairmatches()
                        all_data=dict()
                        for e in resp["events"][0]["eventi"]:
                            home = self.db.converter.escape(e["home"])
                            away = self.db.converter.escape(e["away"])
                            name = home+" - "+away
                            open_date = e["datesort"]+" "+e["hour"]+":00"
                            id = hashlib.md5(e["id"]+open_date).hexdigest()
                            home_odds = ""
                            draw_odds = ""
                            away_odds = ""
                            for q in e["quote"]:
                                if(q["desctype"] == "ESITO FINALE 1X2"):
                                    if(q["description"] == "1"):
                                        home_odds = q["value"]
                                    elif(q["description"] == "X"):
                                        draw_odds = q["value"]
                                    elif(q["description"] == "2"):
                                        away_odds = q["value"]
                            #print name,open_date
                            if(home_odds == "" or away_odds == "" or draw_odds == ""):
                                continue
                            #if(id in self.all_odds and self.all_odds[id]["home_odds"] == home_odds and self.all_odds[id]["draw_odds"] == draw_odds and self.all_odds[id]["away_odds"] == away_odds):
                            #    continue
                            self.all_odds[id] = dict()
                            self.all_odds[id]["home_odds"] = home_odds
                            self.all_odds[id]["draw_odds"] = draw_odds
                            self.all_odds[id]["away_odds"] = away_odds
                            changed = False
                            if home.upper()+" v "+away.upper() in self.matched_events:
                                old_event = home.upper()+" v "+away.upper()
                                home = self.matched_events[old_event].split(" v ")[0].strip()
                                away = self.matched_events[old_event].split(" v ")[1].strip()
                                changed = True
                            bf = None
                            for bf_row in betfair_data:
                                if(open_date != bf_row["open_date"]):
                                    continue
                                if(home.lower() == bf_row["home"].lower() and away.lower() == bf_row["away"].lower()):# or home.lower().startswith(bf_row["home"].lower()) and away.lower().startswith(bf_row["away"].lower())  or bf_row["home"].lower().startswith(home.lower()) and bf_row["away"].lower().startswith(away.lower())):
                                    bf = bf_row
                                    #print home,away,open_date,bf["name"],bf["open_date"]
                                    break
                            if(bf==None and changed == False and id not in blacklist):
                                try:
                                    url = "https://www.betfair.com/exchange/search?&modules=instantsearch-ebs&isAjax=True&container=false&query="+urllib.quote_plus(name.encode('utf8'))+"&alt=json"
                                    req = urllib2.Request(url)
                                    resp = self.opener.open(req)
                                    if resp.info().get('Content-Encoding') == 'gzip':
                                        buf = StringIO(resp.read())
                                        resp = gzip.GzipFile(fileobj=buf)
                                    resp = resp.read()
                                    soup = BeautifulSoup(resp)
                                    spans = soup.find("span")
                                    
                                    if(spans == None):
                                        blacklist.append(id)
                                        continue
                                    spans = spans.findAll("span")
                                    
                                    if(spans != None and len(spans)>1):
                                        evnt = spans[0].text.replace(" v "," - ")
                                        if(len(evnt.split(" - "))>1):
                                            h = evnt.split(" - ")[0].strip()
                                            a = evnt.split(" - ")[1].strip()
                                            d = spans[1].text.split('|')
                                            d = d[len(d)-1].strip()+":00"
                                            
                                            SQL1=""
                                            if(open_date == dateparser.parse(d).strftime("%Y-%m-%d %H:%M:%S") and distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())<13):#or  abs((dateparser.parse(str(open_date)) - dateparser.parse(d)).total_seconds())<900 and distance.levenshtein(evnt.upper(),name.upper())<13):
                                                for bf_row in betfair_data:
                                                    if(h.lower() == bf_row["home"].lower() and a.lower() == bf_row["away"].lower()):
                                                        bf = bf_row
                                                        self.matched_events[home.upper()+" v "+away.upper()] = h.upper()+" v "+a.upper()
                                                        id1 = hashlib.md5(h+a+self.__class__.__name__.upper()).hexdigest()
                                                        SQL1 = "insert ignore into matched_events (id,betfair,bookie,bookie_name) values ('{0}','{1}','{2}','{3}'); ".format(id1,h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper(),self.__class__.__name__.upper())
                                                        cur = self.db.cursor() 
                                                        for result in cur.execute(SQL1,multi = True):
                                                            pass
                                                        self.db.commit()            
                                                        cur.close()
                                                        break
                                            elif(open_date == dateparser.parse(d).strftime("%Y-%m-%d %H:%M:%S") and distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())<25 and (h.lower()== home.lower() or a.lower() == away.lower() or (home.lower() in h.lower() or away.lower() in a.lower()))):
                                                for bf_row in betfair_data:
                                                    if(h.lower() == bf_row["home"].lower() and a.lower() == bf_row["away"].lower()):
                                                        bf = bf_row
                                                        bf_stake =  (float(home_odds)*100)/(float(bf["home_odds"])-0.05)
                                                        profit = ((bf_stake*(1-0.05))-100)
                                                        rating = round(100+profit,2)
                                                        if(rating<100):
                                                            print home+" v "+away,h+" v "+a,distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper()),distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())
                                                            self.matched_events[home.upper()+" v "+away.upper()] = h.upper()+" v "+a.upper()
                                                            id1 = hashlib.md5(h+a+self.__class__.__name__.upper()).hexdigest()
                                                            SQL1 = "insert ignore into matched_events (id,betfair,bookie,bookie_name) values ('{0}','{1}','{2}','{3}'); ".format(id1,h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper(),self.__class__.__name__.upper())
                                                            cur = self.db.cursor() 
                                                            for result in cur.execute(SQL1,multi = True):
                                                                pass
                                                            self.db.commit()            
                                                            cur.close()
                                                        break
                                                
                                except Exception,msg:
                                    exc_type, exc_obj, exc_tb = sys.exc_info()
                                    print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                                        
                                if bf == None:
                                    blacklist.append(id)
                                    continue   
                               
                            if(bf==None):
                                continue
                            if(home_odds!= 90 and bf["home_odds"]!="None" and bf["home_odds"]!=""):
                                bf_stake =  (float(home_odds)*100)/(float(bf["home_odds"])-0.05)
                                profit = ((bf_stake*(1-0.05))-100)
                                rating = round(100+profit,2)

                                backOdds = float(home_odds)
                                layOdds = float(bf["home_odds"])
                                backCommission = 0
                                layCommission = 0.05
                                backStake = 100
                                layStake =  backStake*backOdds/(layOdds-layCommission)
                                refund = 25

                                layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                netresultlaybackwin  = refund-backStake
                                netresultbackbackwin = ((backStake * backOdds) - backStake)
                                liability = layStake*layOdds
                                netresultbacklaywin =  (-1 * liability)
                                netresultlaylaywin = (layStake * (1-layCommission))
                                laywinprofit = netresultlaybackwin + netresultlaylaywin
                                backwinprofit = netresultbackbackwin + netresultbacklaywin
                                snr = laywinprofit
                                if(snr>backwinprofit):
                                    snr = backwinprofit

                                snr = round((((float)(snr*100))/refund),2)
                                key = self.__class__.__name__.lower()+"_"+id+"home"+bf["id"]
                                SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],bf["home"],"1x2",rating,self.bookie["id"],self.bookie["full_name"],home_odds,bf["home_odds"],bf["home_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],"betfair",snr,rating,home_odds,bf["home_odds"],bf["home_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])
                                
                            if(away_odds!= 90 and bf["away_odds"]!="None") and bf["away_odds"]!="":
                                bf_stake =  (float(away_odds)*100)/(float(bf["away_odds"])-0.05)
                                profit = ((bf_stake*(1-0.05))-100)
                                rating = round(100+profit,2)
                                
                                backOdds = float(away_odds)
                                layOdds = float(bf["away_odds"])
                                backCommission = 0
                                layCommission = 0.05
                                backStake = 100
                                layStake =  backStake*backOdds/(layOdds-layCommission)
                                refund = 25

                                layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                netresultlaybackwin  = refund-backStake
                                netresultbackbackwin = ((backStake * backOdds) - backStake)
                                liability = layStake*layOdds
                                netresultbacklaywin =  (-1 * liability)
                                netresultlaylaywin = (layStake * (1-layCommission))
                                laywinprofit = netresultlaybackwin + netresultlaylaywin
                                backwinprofit = netresultbackbackwin + netresultbacklaywin
                                snr = laywinprofit
                                if(snr>backwinprofit):
                                    snr = backwinprofit
                                snr = round((((float)(snr*100))/refund),2)
                                key = self.__class__.__name__.lower()+"_"+id+"away"+bf["id"]
                                SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],bf["away"],"1x2",rating,self.bookie["id"],self.bookie["full_name"],away_odds,bf["away_odds"],bf["away_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],"betfair",snr,rating,away_odds,bf["away_odds"],bf["away_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])
                                
                            if(draw_odds!= 90 and bf["draw_odds"]!="None" and bf["draw_odds"]!=""):
                                bf_stake =  (float(draw_odds)*100)/(float(bf["draw_odds"])-0.05)
                                profit = ((bf_stake*(1-0.05))-100)
                                rating = round(100+profit,2)

                                backOdds = float(draw_odds)
                                layOdds = float(bf["draw_odds"])
                                backCommission = 0
                                layCommission = 0.05
                                backStake = 100
                                layStake =  backStake*backOdds/(layOdds-layCommission)
                                refund = 25

                                layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                netresultlaybackwin  = refund-backStake
                                netresultbackbackwin = ((backStake * backOdds) - backStake)
                                liability = layStake*layOdds
                                netresultbacklaywin =  (-1 * liability)
                                netresultlaylaywin = (layStake * (1-layCommission))
                                laywinprofit = netresultlaybackwin + netresultlaylaywin
                                backwinprofit = netresultbackbackwin + netresultbacklaywin
                                snr = laywinprofit
                                if(snr>backwinprofit):
                                    snr = backwinprofit

                                snr = round((((float)(snr*100))/refund),2)
                                key = self.__class__.__name__.lower()+"_"+id+"draw"+bf["id"]
                                SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"Pareggio","1x2",rating,self.bookie["id"],self.bookie["full_name"],draw_odds,bf["draw_odds"],bf["draw_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],"betfair",snr,rating,draw_odds,bf["draw_odds"],bf["draw_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])
                            
                               
                            try:
                                if SQL != "" and len(SQL.split(';'))>5:
                                    cur = self.db.cursor() 
                                    for result in cur.execute(SQL,multi = True):
                                        pass
                                    self.db.commit()            
                                    cur.close()
                                    SQL = ""
                            except Exception,msg:
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                                SQL = ""
                        
                        if SQL != "":
                            cur = self.db.cursor() 
                            for result in cur.execute(SQL,multi = True):
                                pass
                            self.db.commit()            
                            cur.close()
                            SQL = ""
                    except Exception,msg:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                        SQL = ""
                        
                    
                        
                    
            except Exception,msg:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                                   
settings = {
    'username': 'root',
    'password': 'welSOFT1010',
    'database': 'ninjabet'
}
file = os.path.join(os.path.dirname(os.path.abspath(__file__)),"proxies.txt")
file = open(file,"r")
proxies = [line.strip() for line in file.readlines()]
lock = threading.Lock()
b = Stakebet(settings,lock,proxies)
b.run()        