import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Stanleybet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
            "X-Requested-With":"XMLHttpRequest",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://sportsbook.stanleybet.it/XSport/dwr/call/plaincall/IF_GetMenu.getMenuElements.dwr"
            post_data = "callCount=1&nextReverseAjaxIndex=0&c0-scriptName=IF_GetMenu&c0-methodName=getMenuElements&c0-id=0&c0-param0=number:5&c0-param1=string:&c0-param2=string:&c0-param3=string:&c0-param4=number:0&c0-param5=number:0&c0-param6=string:it&batchId=0&instanceId=0&page=%2FXSport%2Fpages%2Fprematch.jsp%3Ftoken%3D%26system_code%3DSTANLEYBET%26language%3Dit&scriptSessionId="
            # main_page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False)
            main_page_request = requests.post(url, data=post_data, headers=self.custom_headers, verify=False)
            main_page_soup = BeautifulSoup(main_page_request.content, "lxml")

            all_leagues = [str(league[0]).replace('"', "") for league in re.findall("codiceManifestazione(?::|=)([^,;]+)(?:,|;)(?:s\d+\.)?codiceSport(?::|=)([^,;]+)(?:,|;)", main_page_soup.prettify()) if league[1] == '"1"' and league[0] != '"0"']

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://sportsbook.stanleybet.it/XSport/dwr/call/plaincall/IF_GetAvvenimenti.getEventi.dwr"
                    post_data = "callCount=1\r\nnextReverseAjaxIndex=0\r\nc0-scriptName=IF_GetAvvenimenti\r\nc0-methodName=getEventi\r\nc0-id=0\r\nc0-param0=number:6\r\nc0-param1=string:\r\nc0-param2=string:\r\nc0-param3=number:1\r\nc0-param4=number:{0}\r\nc0-param5=boolean:false\r\nc0-param6=string:\r\nc0-param7=number:0\r\nc0-param8=number:0\r\nc0-param9=string:it\r\nbatchId=9\r\ninstanceId=0\r\npage=%2FXSport%2Fpages%2Fprematch.jsp%3Ftoken%3D%26ip%3D{1}%26system_code%3DSTANLEYBET%26language%3Dit\r\nscriptSessionId=gfwwO5zX2~7OBCTiok42rXS9dYBksd1RC6m/nyrTC6m-Z5$XjOL$f".format(league, "127.0.0.1")
                    # league_page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False)
                    league_page_request = requests.post(url, data=post_data, headers=self.custom_headers, verify=False)
                    league_page_soup = BeautifulSoup(league_page_request.content, "lxml")

                    url = "https://sportsbook.stanleybet.it/XSport/dwr/call/plaincall/IF_GetAvvenimentiPerGruppo.getEventi.dwr"
                    post_data = "callCount=1\r\nnextReverseAjaxIndex=0\r\nc0-scriptName=IF_GetAvvenimentiPerGruppo\r\nc0-methodName=getEventi\r\nc0-id=0\r\nc0-param0=string:\r\nc0-param1=string:\r\nc0-param2=number:6\r\nc0-param3=string:8\r\nc0-param4=string:28\r\nc0-param5=string:1\r\nc0-param6=string:{0}\r\nc0-param7=string:STANLEYBET\r\nc0-param8=number:0\r\nc0-param9=number:0\r\nc0-param10=string:it\r\nbatchId=10\r\ninstanceId=0\r\npage=%2FXSport%2Fpages%2Fprematch.jsp%3Ftoken%3D%26ip%3D{1}%26system_code%3DSTANLEYBET%26language%3Dit\r\nscriptSessionId=gfwwO5zX2~7OBCTiok42rXS9dYBksd1RC6m/nyrTC6m-Z5$XjOL$f".format(league, "127.0.0.1")
                    # ou_page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False)
                    ou_page_request = requests.post(url, data=post_data, headers=self.custom_headers, verify=False)
                    ou_page_soup = BeautifulSoup(ou_page_request.content, "lxml")

                    self.init_betfair_events()

                    events = league_page_soup.prettify().replace("\n", "").replace("\r", "").replace("\t", "").replace('dwr.engine.remote.newObject("AvvenimentoDTO"', '|').replace('\"', '').split('|')
                    for event in events:
                        name = re.match(".*desc_avv:([^,]+),", event)
                        if not name:
                            continue
                        event_id = re.match(".*id_avv:([^,]+),", event).group(1)
                        name = name.group(1)
                        home = name.split("-")[0].strip()
                        away = name.split("-")[1].strip()
                        datestamp = re.match(".*timeStamp:([^,]+),", event).group(1).strip()
                        open_date = datetime.datetime.strptime(datestamp, '%Y%m%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()

                        outcomes = event.replace('dwr.engine.remote.newObject(EsitoDTO', '|').split('|')
                        for outcome in outcomes:
                            desc_outcome = re.match(".*desc_esito:([^,]+),", outcome)
                            if desc_outcome:
                                desc_outcome_text = desc_outcome.group(1)
                                odds = re.match(".*quota:([^,]+),", outcome).group(1)
                                if desc_outcome_text == "1":
                                    all_book_odds["home"] = float(odds) / 100
                                elif desc_outcome_text == "2":
                                    all_book_odds["away"] = float(odds) / 100
                                elif desc_outcome_text == "X":
                                    all_book_odds["draw"] = float(odds) / 100

                        uo_odds = []
                        ggng_odds = []
                        outcomes_additional = event.replace('dwr.engine.remote.newObject(ScommessaDTO', '|').split('|')
                        for outcome in outcomes_additional:
                            desc_outcome = re.match(".*desc_scom:([^,]+),", outcome)
                            if desc_outcome:
                                desc_outcome_text = desc_outcome.group(1)
                                odds = re.match(".*quote:\[([^]]+?)\]", outcome).group(1)
                                if desc_outcome_text == "U\/O 2.5":
                                    uo_odds = odds.split(",")
                                if desc_outcome_text == "GG\/NG":
                                    ggng_odds = odds.split(",")

                        all_odds = {}
                        outcomes_additional_odds = league_page_soup.prettify().replace("\n", "").replace("\r", "").replace("\t", "").replace('.attivo=', '|').replace('\"', '').split('|')
                        for odds in outcomes_additional_odds:
                            desc_outcome = re.search(";(s\d+)\['desc_esito'\]=([^;]+);", odds)
                            if desc_outcome:
                                id_outcome = desc_outcome.group(1)
                                desc_outcome_text = desc_outcome.group(2)
                                odd = float(re.match(".*quota=([^;]+);", odds).group(1)) / 100
                                all_odds[id_outcome] = {"desc_outcome_text":desc_outcome_text, "odd":odd}

                        for odd in uo_odds:
                            data = all_odds[odd]
                            if data["desc_outcome_text"] == "UNDER":
                                all_book_odds["under_25"] = data["odd"]
                            if data["desc_outcome_text"] == "OVER":
                                all_book_odds["over_25"] = data["odd"]

                        for odd in ggng_odds:
                            data = all_odds[odd]
                            if data["desc_outcome_text"] == "GOAL":
                                all_book_odds["goal"] = data["odd"]
                            if data["desc_outcome_text"] == "NOGOAL":
                                all_book_odds["no_goal"] = data["odd"]

                        uo_events = ou_page_soup.prettify().replace("\n", "").replace("\r", "").replace("\t", "").replace('dwr.engine.remote.newObject("AvvenimentoDTO"', '|').replace('\"', '').split('|')
                        for uo_event in uo_events:
                            uo_event_id = re.match(".*id_avv:([^,]+),", uo_event)
                            if not uo_event_id:
                                continue
                            if uo_event_id.group(1) == event_id:
                                uo_outcomes = uo_event.replace('dwr.engine.remote.newObject(ScommessaDTO', '|').split('|')
                                for outcome in uo_outcomes:
                                    desc_outcome = re.match(".*desc_scom:([^,]+),", outcome)
                                    if desc_outcome:
                                        desc_outcome_text = desc_outcome.group(1)
                                        odd_outcomes = outcome.replace('dwr.engine.remote.newObject(EsitoDTO', '|').split('|')
                                        for odd_outcome in odd_outcomes:
                                            odd_desc_outcome = re.match(".*desc_esito:([^,]+),", odd_outcome)
                                            if odd_desc_outcome:
                                                odd_desc_outcome_text = odd_desc_outcome.group(1)
                                                odds = re.match(".*quota:([^,]+),", odd_outcome).group(1)
                                                if desc_outcome_text == "U\/O 0.5":
                                                    if odd_desc_outcome_text == "UNDER":
                                                        all_book_odds["under_05"] = float(odds) / 100
                                                    if odd_desc_outcome_text == "OVER":
                                                        all_book_odds["over_05"] = float(odds) / 100
                                                if desc_outcome_text == "U\/O 1.5":
                                                    if odd_desc_outcome_text == "UNDER":
                                                        all_book_odds["under_15"] = float(odds) / 100
                                                    if odd_desc_outcome_text == "OVER":
                                                        all_book_odds["over_15"] = float(odds) / 100
                                                if desc_outcome_text == "U\/O 3.5":
                                                    if odd_desc_outcome_text == "UNDER":
                                                        all_book_odds["under_35"] = float(odds) / 100
                                                    if odd_desc_outcome_text == "OVER":
                                                        all_book_odds["over_35"] = float(odds) / 100
                                                if desc_outcome_text == "U\/O 4.5":
                                                    if odd_desc_outcome_text == "UNDER":
                                                        all_book_odds["under_45"] = float(odds) / 100
                                                    if odd_desc_outcome_text == "OVER":
                                                        all_book_odds["over_45"] = float(odds) / 100

                        bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                        if not bf_event:
                            print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Stanleybet(bot_data)
t.run()
