import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from xml.dom import minidom
from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Scommettendo(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.scommettendo.it/sports/"
            main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False)
            main_page_soup = BeautifulSoup(main_page_request.content, "lxml")

            all_leagues = [str(input["value"]) for input in main_page_soup.find("div",{"id":"innerleagues_1"}).findAll("input",{"name":"ls[]"}) if input.has_key("class") and "sport_1" in input["class"]]

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    proxy = self.get_proxy()

                    url = "https://www.scommettendo.it/plugins/betting/sports_v5/_d/load_odds.php?ls={0}&sport=1".format(league)
                    league_page_request = requests.get(url, headers=self.custom_headers, proxies=proxy, verify=False)
                    league_page_soup = BeautifulSoup(league_page_request.content, "lxml")

                    dt = ""
                    self.init_betfair_events()
                    for tr in league_page_soup.findAll("tr",{"class":"myrow"}):
                        tds = tr.findAll("td")
                        dt = tds[1].text.split("|")
                        if(len(dt)<2):
                            continue
                        name = tds[2].findAll("a")[1].text.strip()
                        if(len(name.split("-"))<2):
                            continue
                        open_date = "{0}-{1}-{2} {3}:00".format(str(datetime.datetime.now().year),dt[0].split("/")[1].strip(),dt[0].split("/")[0].strip(),dt[1].strip())
                        open_date = dateparser.parse(open_date).strftime('%Y-%m-%d %H:%M:%S')
                        team_link = tds[2].findAll("a")[1]
                        team_link = team_link["href"].replace("'","").split("(")[1]
                        team_link = team_link.split(")")[0]
                        team_link = "https://www.scommettendo.it/plugins/betting/sports_v5/_d/load_odds.php?event_id={0}&league_id={1}".format(team_link.split(",")[0],team_link.split(",")[1])
                        
                        home = name.split("-")[0].strip()
                        away = name.split("-")[1].strip()
                        
                        all_book_odds = self.get_empty_odds_dict()
                        all_book_odds["home"] = tds[4].text.strip()
                        all_book_odds["draw"] = tds[5].text.strip()
                        all_book_odds["away"] = tds[6].text.strip()
                        event_hash = hashlib.md5(name+open_date).hexdigest()

                        if all_book_odds["home"] == "NULL" or all_book_odds["away"] == "NULL" or all_book_odds["draw"] == "NULL":
                            continue

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":
                            url = team_link
                            event_page_request = requests.post(url, headers=self.custom_headers, proxies=proxy, verify=False)
                            event_page_soup = BeautifulSoup(event_page_request.content, "lxml")
                            
                            for div1 in event_page_soup.findAll("div",{"class":"bt_div2 bt_div_holder bt_div_holder_showall"}):
                                odds = div1.find("table",{"class":"events_tbl"}).findAll("span")
                                if len(odds)>2:
                                    if(div1.find("div",{"class":"bt_name"}).text.strip() == "GG-NG"):
                                        all_book_odds["goal"] = odds[2].text.strip()
                                        all_book_odds["no_goal"] = odds[3].text.strip()
                                    elif(div1.find("div",{"class":"bt_name"}).text.strip() == "Under/Over (0.5)"):
                                        all_book_odds["under_05"] = odds[2].text.strip()
                                        all_book_odds["over_05"] = odds[3].text.strip()
                                    elif(div1.find("div",{"class":"bt_name"}).text.strip() == "Under/Over (1.5)"):
                                        all_book_odds["under_15"] = odds[2].text.strip()
                                        all_book_odds["over_15"] = odds[3].text.strip()
                                    elif(div1.find("div",{"class":"bt_name"}).text.strip() == "Under/Over (2.5)"):
                                        all_book_odds["under_25"] = odds[2].text.strip()
                                        all_book_odds["over_25"] = odds[3].text.strip()
                                    elif(div1.find("div",{"class":"bt_name"}).text.strip() == "Under/Over (3.5)"):
                                        all_book_odds["under_35"] = odds[2].text.strip()
                                        all_book_odds["over_35"] = odds[3].text.strip()
                                    elif(div1.find("div",{"class":"bt_name"}).text.strip() == "Under/Over (4.5)"):
                                        all_book_odds["under_45"] = odds[2].text.strip()
                                        all_book_odds["over_45"] = odds[3].text.strip()

                        if not self.check_rating(all_book_odds, bf_event):
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Scommettendo(bot_data)
t.run()
