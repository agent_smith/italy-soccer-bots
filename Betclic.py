import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Betclic(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "it,en-US;q=0.7,en;q=0.3",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.betclic.it/calcio-s1"
            main_page_request = requests.get(url, headers=self.custom_headers)
            main_page_soup = BeautifulSoup(main_page_request.content, "lxml")

            all_leagues = ["https://www.betclic.it" + league["href"] for league in main_page_soup.find("ul", {"class":"listAllCompet"}).find_all("a")]

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league_url in all_leagues:
                if league_url in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    self.init_betfair_events()

                    league = re.search(".*e(\d+)$", league_url).group(1)
                    url = "https://www.betclic.it/competition/competitionmarkettype"
                    post_data = {"id":league, "isMarketTypeExpand":False, "marketTypeCategoryCode":"All", "marketTypeCode":"Ftb_Mr3"}
                    league_page_request = requests.post(url, data=post_data, headers=self.custom_headers)
                    league_page_json = json.loads(league_page_request.content)
                    league_page_soup = BeautifulSoup(league_page_json["Data"]["CompetitionEvents"], "lxml")

                    event_wrapper = league_page_soup.find("div", {"id":"event-wrapper"})
                    if event_wrapper:
                        url = "https://www.betclic.it/competition/competitionmarkettype"
                        post_data = {"id":league, "isMarketTypeExpand":False, "marketTypeCategoryCode":"All", "marketTypeCode":"Ftb_10"}
                        ou_page_request = requests.post(url, data=post_data, headers=self.custom_headers)
                        ou_page_json = json.loads(ou_page_request.content)
                        ou_page_soup = BeautifulSoup(ou_page_json["Data"]["CompetitionEvents"], "lxml")

                        url = "https://www.betclic.it/competition/competitionmarkettype"
                        post_data = {"id":league, "isMarketTypeExpand":False, "marketTypeCategoryCode":"All", "marketTypeCode":"Ftb_Bts"}
                        ggng_page_request = requests.post(url, data=post_data, headers=self.custom_headers)
                        ggng_page_json = json.loads(ggng_page_request.content)
                        ggng_page_soup = BeautifulSoup(ggng_page_json["Data"]["CompetitionEvents"], "lxml")

                        for date_section in event_wrapper.find_all("div", {"class":"day-entry"}):
                            event_date = date_section["data-date"]
                            for time_section in date_section.find_all("div", {"class":"schedule"}):
                                event_time = time_section.find("div", {"class":"hour"}).text
                                for event in time_section.find_all("div", {"class":"match-entry"}):
                                    event_id = event["id"].strip("match_")
                                    home, away = [team.strip() for team in event["data-track-event-name"].split(" - ")]
                                    open_date = datetime.datetime.strptime(event_date + " " + event_time, "%Y-%m-%d %H:%M").strftime("%Y-%m-%d %H:%M:%S")
                                    name = home + " - " + away
                                    event_hash = hashlib.md5(open_date + name).hexdigest()

                                    all_book_odds = self.get_empty_odds_dict()

                                    odds = event.find_all("span", {"class":"odd-button"})
                                    all_book_odds["home"] = float(odds[0].text.strip().replace(",", "."))
                                    all_book_odds["away"] = float(odds[2].text.strip().replace(",", "."))
                                    all_book_odds["draw"] = float(odds[1].text.strip().replace(",", "."))

                                    if ou_page_soup.find("div", {"data-track-bloc-title":"Under/Over"}):
                                        for ou_event in ou_page_soup.find_all("div", {"class":"match-entry"}):
                                            if ou_event["id"].strip("match_") == event_id:
                                                for odd_label in ou_event.find_all("span", {"class":"odd-label"}):
                                                    odd = float(odd_label.parent.find("span", {"class":"odd-button"}).text.strip().replace(",", "."))
                                                    if odd_label.text.strip() == "Under 0.5":
                                                        all_book_odds["under_05"] = odd
                                                    if odd_label.text.strip() == "Over 0.5":
                                                        all_book_odds["over_05"] = odd
                                                    if odd_label.text.strip() == "Under 1.5":
                                                        all_book_odds["under_15"] = odd
                                                    if odd_label.text.strip() == "Over 1.5":
                                                        all_book_odds["over_15"] = odd
                                                    if odd_label.text.strip() == "Under 2.5":
                                                        all_book_odds["under_25"] = odd
                                                    if odd_label.text.strip() == "Over 2.5":
                                                        all_book_odds["over_25"] = odd
                                                    if odd_label.text.strip() == "Under 3.5":
                                                        all_book_odds["under_35"] = odd
                                                    if odd_label.text.strip() == "Over 3.5":
                                                        all_book_odds["over_35"] = odd
                                                    if odd_label.text.strip() == "Under 4.5":
                                                        all_book_odds["under_45"] = odd
                                                    if odd_label.text.strip() == "Over 4.5":
                                                        all_book_odds["over_45"] = odd

                                    if ggng_page_soup.find("div", {"data-track-bloc-title":"Entrambe a segno"}):
                                        for ggng_event in ggng_page_soup.find_all("div", {"class":"match-entry"}):
                                            if ggng_event["id"].strip("match_") == event_id:
                                                odds = ggng_event.find_all("span", {"class":"odd-button"})
                                                all_book_odds["goal"] = float(odds[0].text.strip().replace(",", "."))
                                                all_book_odds["no_goal"] = float(odds[1].text.strip().replace(",", "."))

                                    bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                                    if not bf_event:
                                        print name, "no bf_event, skipping"
                                        if event_hash not in self.bot_data["blacklist"]:
                                            self.bot_data["blacklist"].append(event_hash)
                                        continue

                                    print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                                    event_found = True
                                    self.save_data(event_hash, all_book_odds, bf_event)

                    else:
                        single_event_page_request = requests.get(league_url, headers=self.custom_headers)
                        single_event_page_soup = BeautifulSoup(single_event_page_request.content, "lxml")

                        info_section = single_event_page_soup.find("div", {"class":"section-header-left"})
                        if not info_section:
                            continue

                        home, away = [team.strip() for team in info_section.h1.text.split(" - ")]
                        open_date = dateparser.parse(info_section.time.text + ":00").strftime("%Y-%m-%d %H:%M:%S")
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        section_1x2 = single_event_page_soup.find("span", string=re.compile(".*Esito incontro.*"))
                        if not section_1x2:
                            continue

                        all_book_odds = self.get_empty_odds_dict()

                        odds_1x2 = section_1x2.parent.parent.find_all("span", {"class":"odd-button"})
                        all_book_odds["home"] = float(odds_1x2[0].text.strip().replace(",", "."))
                        all_book_odds["away"] = float(odds_1x2[2].text.strip().replace(",", "."))
                        all_book_odds["draw"] = float(odds_1x2[1].text.strip().replace(",", "."))

                        section_uo = single_event_page_soup.find("span", string=re.compile(".*Under/Over.*"))
                        if section_uo:
                            for odd_label in section_uo.parent.parent.find_all("span", {"class":"odd-label"}):
                                odd = float(odd_label.parent.find("span", {"class":"odd-button"}).text.strip().replace(",", "."))
                                if odd_label.text.strip() == "Under 0.5":
                                    all_book_odds["under_05"] = odd
                                if odd_label.text.strip() == "Over 0.5":
                                    all_book_odds["over_05"] = odd
                                if odd_label.text.strip() == "Under 1.5":
                                    all_book_odds["under_15"] = odd
                                if odd_label.text.strip() == "Over 1.5":
                                    all_book_odds["over_15"] = odd
                                if odd_label.text.strip() == "Under 2.5":
                                    all_book_odds["under_25"] = odd
                                if odd_label.text.strip() == "Over 2.5":
                                    all_book_odds["over_25"] = odd
                                if odd_label.text.strip() == "Under 3.5":
                                    all_book_odds["under_35"] = odd
                                if odd_label.text.strip() == "Over 3.5":
                                    all_book_odds["over_35"] = odd
                                if odd_label.text.strip() == "Under 4.5":
                                    all_book_odds["under_45"] = odd
                                if odd_label.text.strip() == "Over 4.5":
                                    all_book_odds["over_45"] = odd

                        section_ggng = single_event_page_soup.find("span", string=re.compile(".*Entrambe a segno.*"))
                        if section_ggng:
                            odds_ggng = section_ggng.parent.parent.find_all("span", {"class":"odd-button"})
                            all_book_odds["goal"] = float(odds_ggng[0].text.strip().replace(",", "."))
                            all_book_odds["no_goal"] = float(odds_ggng[1].text.strip().replace(",", "."))

                        bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                        if not bf_event:
                            print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league_url not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league_url)
                        self.bot_data["blacklist_leagues"].append(league_url)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Betclic(bot_data)
t.run()
