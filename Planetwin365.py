import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Planetwin365(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
            "X-Requested-With":"XMLHttpRequest",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.planetwin365.it/Sport/Groups.aspx?IDSport=342&Antepost=0"
            main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
            main_page_soup = BeautifulSoup(main_page_request.content, "lxml")

            football_section = main_page_soup.find("div", {"class":"divNSport", "title":"Calcio"})
            if football_section:
                all_leagues = [re.search("EventID=(\d+)", a["href"]).group(1) for a in football_section.parent.parent.parent.find_all("a", {"id":"lE"})]

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://www.planetwin365.it/ControlsSkin/OddsEvent.aspx?ShowLinkFastBet=0&showDate=1&showGQ=1&rnd=0{0}&EventID={1}&GroupSep=undefined".format(random.randint(10 ** (16 - 1), (10 ** 16) - 1), league)
                    league_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                    league_page_soup = BeautifulSoup(league_page_request.content, "lxml")

                    self.init_betfair_events()
                    for event in league_page_soup.find_all("tr", {"data-sottoevento-name":True}):
                        event_id = event["data-sottoevento-id"]
                        date = event.parent.find("td", {"class":"cqDateTbl"}).text.strip()
                        hour = event.find("td", {"class":"dtInc"}).text.strip()
                        open_date = dateparser.parse(date + " " + hour).strftime('%Y-%m-%d %H:%M:%S')
                        name = event.find("td", {"class":"nmInc"}).text.strip()
                        home, away = [team.strip() for team in name.split(" - ")]
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()

                        home_section = event.find("td", {"data-tipoquota":"1"})
                        if home_section:
                            all_book_odds["home"] = float(home_section.text.strip().replace(",", "."))
                        away_section = event.find("td", {"data-tipoquota":"2"})
                        if away_section:
                            all_book_odds["away"] = float(away_section.text.strip().replace(",", "."))
                        draw_section = event.find("td", {"data-tipoquota":"X"})
                        if draw_section:
                            all_book_odds["draw"] = float(draw_section.text.strip().replace(",", "."))
                        under15_section = event.find("td", {"data-tipoquota":"Under 1.5"})
                        if under15_section:
                            all_book_odds["under_15"] = float(under15_section.text.strip().replace(",", "."))
                        over15_section = event.find("td", {"data-tipoquota":"Over 1.5"})
                        if over15_section:
                            all_book_odds["over_15"] = float(over15_section.text.strip().replace(",", "."))
                        under25_section = event.find("td", {"data-tipoquota":"Un. 2.5"})
                        if under25_section:
                            all_book_odds["under_25"] = float(under25_section.text.strip().replace(",", "."))
                        over25_section = event.find("td", {"data-tipoquota":"Ov. 2.5"})
                        if over25_section:
                            all_book_odds["over_25"] = float(over25_section.text.strip().replace(",", "."))
                        goal_section = event.find("td", {"data-tipoquota":"GG"})
                        if goal_section:
                            all_book_odds["goal"] = float(goal_section.text.strip().replace(",", "."))
                        no_goal_section = event.find("td", {"data-tipoquota":"NG"})
                        if no_goal_section:
                            all_book_odds["no_goal"] = float(no_goal_section.text.strip().replace(",", "."))

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        if bf_event["under_05"] != "None" or bf_event["over_05"] != "None" or bf_event["under_35"] != "None" or bf_event["over_35"] != "None" or bf_event["under_45"] != "None" or bf_event["over_45"] != "None":
                            url = "https://www.planetwin365.it/ControlsSkin/OddsSubEvent.aspx?SubEventID={0}&IDGruppoQuota=101&btnID=s_w_PC_cCoupon_btnAddCoupon&txtID=s_w_PC_cCoupon_txtIDQuota&rnd=0{1}".format(event_id, random.randint(10 ** (16 - 1), (10 ** 16) - 1))
                            event_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                            event_page_soup = BeautifulSoup(event_page_request.content, "lxml")

                            under05_section = event_page_soup.find("div", {"data-tipoquota":"Under 0.5"})
                            if under05_section:
                                try:
                                    all_book_odds["under_05"] = float(under05_section.a.text.strip().replace(",", "."))
                                except ValueError:
                                    pass
                            over05_section = event_page_soup.find("div", {"data-tipoquota":"Over 0.5"})
                            if over05_section:
                                try:
                                    all_book_odds["over_05"] = float(over05_section.a.text.strip().replace(",", "."))
                                except ValueError:
                                    pass
                            under35_section = event_page_soup.find("div", {"data-tipoquota":"Under 3.5"})
                            if under35_section:
                                try:
                                    all_book_odds["under_35"] = float(under35_section.a.text.strip().replace(",", "."))
                                except ValueError:
                                    pass
                            over35_section = event_page_soup.find("div", {"data-tipoquota":"Over 3.5"})
                            if over35_section:
                                try:
                                    all_book_odds["over_35"] = float(over35_section.a.text.strip().replace(",", "."))
                                except ValueError:
                                    pass
                            under45_section = event_page_soup.find("div", {"data-tipoquota":"Under 4.5"})
                            if under45_section:
                                try:
                                    all_book_odds["under_45"] = float(under45_section.a.text.strip().replace(",", "."))
                                except ValueError:
                                    pass
                            over45_section = event_page_soup.find("div", {"data-tipoquota":"Over 4.5"})
                            if over45_section:
                                try:
                                    all_book_odds["over_45"] = float(over45_section.a.text.strip().replace(",", "."))
                                except ValueError:
                                    pass

                        if not self.check_rating(all_book_odds, bf_event):
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Planetwin365(bot_data)
t.run()
