import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Lottomatica(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"application/json, text/plain, */*",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "context":"2:0:en_US",
            "Host":"scommesse.lottomatica.it",
            "Origin":"https://www.lottomatica.it",
            "Referer":"https://www.lottomatica.it/scommesse/avvenimenti/scommesse-sportive.html",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def get_leagues(self):
        all_leagues = []
        try:
            url = "https://scommesse.lottomatica.it/ww-adapter/api/v2/c2/cnom/navigation/depth/14989"
            main_page_request = requests.get(url, headers=self.custom_headers,  verify=False)
            main_page_json = json.loads(main_page_request.content)

            for sport in main_page_json["extNode"]["nodes"]:
                if sport["id"] == 15587:
                    for state in sport["nodes"]:
                        for league in state["nodes"]:
                            all_leagues.append(league["id"])

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://scommesse.lottomatica.it/ww-adapter/api/v1/c2/cnom/node/{0}/categorymarkettype".format(league)
                    league_page_request = requests.get(url, headers=self.custom_headers,  verify=False)
                    league_page_json = json.loads(league_page_request.content)

                    if not ("events" in league_page_json and "boGroup" in league_page_json["events"] and "events" in league_page_json["events"]["boGroup"]):
                        continue

                    url = "https://scommesse.lottomatica.it/ww-adapter/api/v1/c2/cnom/node/{0}/markettype/FBL0N02TNSCFT/0@5/0/LINE".format(league)
                    uo05_page_request = requests.get(url, headers=self.custom_headers,  verify=False)
                    uo05_page_json = json.loads(uo05_page_request.content)

                    url = "https://scommesse.lottomatica.it/ww-adapter/api/v1/c2/cnom/node/{0}/markettype/FBL0N02TNSCFT/1@5/0/LINE".format(league)
                    uo15_page_request = requests.get(url, headers=self.custom_headers,  verify=False)
                    uo15_page_json = json.loads(uo15_page_request.content)

                    url = "https://scommesse.lottomatica.it/ww-adapter/api/v1/c2/cnom/node/{0}/markettype/FBL0N02TNSCFT/3@5/0/LINE".format(league)
                    uo35_page_request = requests.get(url, headers=self.custom_headers,  verify=False)
                    uo35_page_json = json.loads(uo35_page_request.content)

                    url = "https://scommesse.lottomatica.it/ww-adapter/api/v1/c2/cnom/node/{0}/markettype/FBL0N02TNSCFT/4@5/0/LINE".format(league)
                    uo45_page_request = requests.get(url, headers=self.custom_headers,  verify=False)
                    uo45_page_json = json.loads(uo45_page_request.content)

                    self.init_betfair_events()
                    for event in league_page_json["events"]["boGroup"]["events"]:
                        event_id = event["id"]
                        if "name" not in event or len(event["name"].split(" - "))<2:
                            continue
                        home, away = [team.strip() for team in event["name"].split(" - ")]
                        open_date = (dateparser.parse(str(event["startTime"])) + datetime.timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S")
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()
                        all_book_odds = self.get_empty_odds_dict()

                        if "eventMarketGroups" in event:
                            for bet_container in event["eventMarketGroups"]:
                                if "detailedMarkets" in bet_container:
                                    for bet_type in bet_container["detailedMarkets"]:
                                        for bet in bet_type["selections"]:
                                            if "priceValues" in bet and bet["priceValues"]:
                                                if bet["name"] == "1":
                                                    all_book_odds["home"] = float(bet["priceValues"][0]["formatValue"])
                                                elif bet["name"] == "2":
                                                    all_book_odds["away"] = float(bet["priceValues"][0]["formatValue"])
                                                elif bet["name"] == "X":
                                                    all_book_odds["draw"] = float(bet["priceValues"][0]["formatValue"])
                                                elif bet["name"] == "UNDER":
                                                    all_book_odds["under_25"] = float(bet["priceValues"][0]["formatValue"])
                                                elif bet["name"] == "OVER":
                                                    all_book_odds["over_25"] = float(bet["priceValues"][0]["formatValue"])
                                                elif bet["name"] == "GOAL":
                                                    all_book_odds["goal"] = float(bet["priceValues"][0]["formatValue"])
                                                elif bet["name"] == "NOGOAL":
                                                    all_book_odds["no_goal"] = float(bet["priceValues"][0]["formatValue"])

                        for extra_json in [uo05_page_json, uo15_page_json, uo35_page_json, uo45_page_json]:
                            if "extNode" in extra_json and "boGroup" in extra_json["extNode"] and "events" in extra_json["extNode"]["boGroup"]:
                                for uo_event in extra_json["extNode"]["boGroup"]["events"]:
                                    if uo_event["id"] == event_id and "eventMarketGroups" in uo_event:
                                        for bet_container in uo_event["eventMarketGroups"]:
                                            if "detailedMarkets" in bet_container:
                                                for bet_type in bet_container["detailedMarkets"]:
                                                    if bet_type["name"] == "UNDER / OVER 0,5":
                                                        for bet in bet_type["selections"]:
                                                            if bet["priceValues"]:
                                                                if bet["name"] == "UNDER":
                                                                    all_book_odds["under_05"] = float(bet["priceValues"][0]["formatValue"])
                                                                elif bet["name"] == "OVER":
                                                                    all_book_odds["over_05"] = float(bet["priceValues"][0]["formatValue"])
                                                    elif bet_type["name"] == "UNDER / OVER 1,5":
                                                        for bet in bet_type["selections"]:
                                                            if bet["priceValues"]:
                                                                if bet["name"] == "UNDER":
                                                                    all_book_odds["under_15"] = float(bet["priceValues"][0]["formatValue"])
                                                                elif bet["name"] == "OVER":
                                                                    all_book_odds["over_15"] = float(bet["priceValues"][0]["formatValue"])
                                                    elif bet_type["name"] == "UNDER / OVER 3,5":
                                                        for bet in bet_type["selections"]:
                                                            if bet["priceValues"]:
                                                                if bet["name"] == "UNDER":
                                                                    all_book_odds["under_35"] = float(bet["priceValues"][0]["formatValue"])
                                                                elif bet["name"] == "OVER":
                                                                    all_book_odds["over_35"] = float(bet["priceValues"][0]["formatValue"])
                                                    elif bet_type["name"] == "UNDER / OVER 4,5":
                                                        for bet in bet_type["selections"]:
                                                            if bet["priceValues"]:
                                                                if bet["name"] == "UNDER":
                                                                    all_book_odds["under_45"] = float(bet["priceValues"][0]["formatValue"])
                                                                elif bet["name"] == "OVER":
                                                                    all_book_odds["over_45"] = float(bet["priceValues"][0]["formatValue"])

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name + " no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Lottomatica(bot_data)
t.run()
