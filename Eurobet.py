import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Eurobet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "it,en-US;q=0.7,en;q=0.3",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
            "X-EB-Accept-Language":"IT",
            "X-EB-MarketId":"5",
            "X-EB-PlatformId":"1",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.eurobet.it/prematch-menu-service/sport-schedule/services/prematch-menu?prematch=1&live=0"
            main_page_request = requests.get(url, headers=self.custom_headers)
            main_page_json = json.loads(main_page_request.content)

            for sport in main_page_json["result"]["sportList"]:
                if sport["description"] == "Calcio":
                    for category in sport["itemList"]:
                        if "Speciali" not in category["description"]:
                            for league in category["itemList"]:
                                all_leagues.append(league["aliasUrl"])

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://www.eurobet.it/detail-service/sport-schedule/services/meeting/calcio/{0}?prematch=1&live=0".format(league)
                    league_page_request = requests.get(url, headers=self.custom_headers)
                    league_page_json = json.loads(league_page_request.content)

                    url = "https://www.eurobet.it/detail-service/sport-schedule/services/meeting/calcio/{0}/u-o/Tutti-under?prematch=1&live=0".format(league)
                    under_page_request = requests.get(url, headers=self.custom_headers)
                    under_page_json = json.loads(under_page_request.content)

                    url = "https://www.eurobet.it/detail-service/sport-schedule/services/meeting/calcio/{0}/u-o/Tutti-over?prematch=1&live=0".format(league)
                    over_page_request = requests.get(url, headers=self.custom_headers)
                    over_page_json = json.loads(over_page_request.content)

                    self.init_betfair_events()
                    for section in league_page_json["result"]["dataGroupList"]:
                        for event in section["itemList"]:

                            event_id = event["eventInfo"]["programBetradarInfo"]["matchId"]
                            home = event["eventInfo"]["teamHome"]["description"]
                            away = event["eventInfo"]["teamAway"]["description"]
                            open_date = dateparser.parse(str(event["eventInfo"]["eventData"])).strftime("%Y-%m-%d %H:%M:%S")
                            name = home + " - " + away
                            event_hash = hashlib.md5(open_date + name).hexdigest()

                            all_book_odds = self.get_empty_odds_dict()

                            for bet_type in event["betGroupList"]:
                                if bet_type["betId"] ==  1549:
                                    for market in bet_type["oddGroupList"]:
                                        if market["oddGroupDescription"] == "1X2":
                                            for odd in market["oddList"]:
                                                if odd["oddDescription"] == "1":
                                                    all_book_odds["home"] = float(odd["oddValue"]) / 100
                                                elif odd["oddDescription"] == "2":
                                                    all_book_odds["away"] = float(odd["oddValue"]) / 100
                                                elif odd["oddDescription"] == "X":
                                                    all_book_odds["draw"] = float(odd["oddValue"]) / 100
                                        elif market["oddGroupDescription"] == "GG/NG":
                                            for odd in market["oddList"]:
                                                if odd["oddDescription"] == "Goal":
                                                    all_book_odds["goal"] = float(odd["oddValue"]) / 100
                                                elif odd["oddDescription"] == "Nogoal":
                                                    all_book_odds["no_goal"] = float(odd["oddValue"]) / 100

                            for section_under in under_page_json["result"]["dataGroupList"]:
                                for event_under in section_under["itemList"]:
                                    if event_under["eventInfo"]["programBetradarInfo"]["matchId"] == event_id:
                                        for bet_type in event_under["betGroupList"]:
                                            if bet_type["betId"] == 4626:
                                                for market in bet_type["oddGroupList"]:
                                                    if market["oddGroupDescription"] == "U/O 0.5":
                                                        all_book_odds["under_05"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 1.5":
                                                        all_book_odds["under_15"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 2.5":
                                                        all_book_odds["under_25"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 3.5":
                                                        all_book_odds["under_35"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 4.5":
                                                        all_book_odds["under_45"] = float(market["oddList"][0]["oddValue"]) / 100

                            for section_over in over_page_json["result"]["dataGroupList"]:
                                for event_over in section_over["itemList"]:
                                    if event_over["eventInfo"]["programBetradarInfo"]["matchId"] == event_id:
                                        for bet_type in event_over["betGroupList"]:
                                            if bet_type["betId"] == 4625:
                                                for market in bet_type["oddGroupList"]:
                                                    if market["oddGroupDescription"] == "U/O 0.5":
                                                        all_book_odds["over_05"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 1.5":
                                                        all_book_odds["over_15"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 2.5":
                                                        all_book_odds["over_25"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 3.5":
                                                        all_book_odds["over_35"] = float(market["oddList"][0]["oddValue"]) / 100
                                                    if market["oddGroupDescription"] == "U/O 4.5":
                                                        all_book_odds["over_45"] = float(market["oddList"][0]["oddValue"]) / 100

                            bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                            if not bf_event:
                                print name + " no bf_event, skipping"
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue

                            print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                            event_found = True
                            self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Eurobet(bot_data)
t.run()
