import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time
import cookielib
from cookielib import CookieJar
import urllib2
import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from xml.dom import minidom
import ssl
from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Snai(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
            "X-Requested-With":"XMLHttpRequest",
        }
        self.ctx = ssl.create_default_context()
        self.ctx.check_hostname = False
        self.ctx.verify_mode = ssl.CERT_NONE

    def get_leagues(self):
        all_leagues = []

        try:
            self.cj = CookieJar()
            self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
            self.opener.addheaders = [('Connection', 'keep-alive'),('Cookie2','$Version=1')]            
            postdata = "op=cfg&tf=and"
            url = "https://snaigatewaybettingand.game360.it:7243/BettingMobileGatewaySnai/service30?op=lsc&fante=0&tf=and"
            req = urllib2.Request(url,postdata)
            resp = self.opener.open(req).read()
            resp = resp.encode('utf-8')
            xmldoc = minidom.parseString(resp)
            for sp in xmldoc.getElementsByTagName('sp'):
                if(sp.attributes["d"].value == "CALCIO"):
                    for gm in sp.getElementsByTagName("gm"):
                        if(gm.attributes["id"].value != "300"):
                            url = "https://snaigatewaybettingand.game360.it:7243/BettingMobileGatewaySnai/service30?op=lmc&sp={0}&gm={1}&fante=0&tf=and".format(sp.attributes["id"].value,gm.attributes["id"].value)
                            req = urllib2.Request(url,postdata)
                            resp = self.opener.open(req).read()
                            resp = resp.encode('utf-8')
                            xmldoc2 = minidom.parseString(resp)
                            l = [str(gm.attributes["id"].value)+":"+str(m.attributes["id"].value) for m in xmldoc2.getElementsByTagName('m')]
                            all_leagues.extend(l)
                    break
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)
        #print all_leagues
        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    self.cj = CookieJar()
                    self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
                    self.opener.addheaders = [('Connection', 'keep-alive'),('Cookie2','$Version=1')]  
                           
                    url = "https://snaigatewaybettingand.game360.it:7243/BettingMobileGatewaySnai/service30?op=lav2&sp=1&gm={0}&m={1}&idAgg=&fante=0&tf=and".format(league.split(":")[0],league.split(":")[1])
                                        
                    req = urllib2.Request(url)
                    resp = self.opener.open(req)
                    req = urllib2.Request(url)
                    resp = self.opener.open(req)
                    dt = ""
                    self.init_betfair_events()
                    resp = resp.read().encode('utf-8')
                    xmldoc = minidom.parseString(resp)   
                    for ev in xmldoc.getElementsByTagName('ev'):
                        name = self.db.converter.escape(ev.attributes["d"].value.strip())                            
                        if(len(name.split(" - "))<2):
                            continue
                        home = name.split(" - ")[0].strip()
                        away = name.split(" - ")[1].strip()
                        dt = ev.attributes["dt"].value
                        tm = dt.split(" ")[1].strip()
                        dt = dt.split(" ")[0].strip()
                        dt = "20{0}-{1}-{2}".format(dt.split("/")[2],dt.split("/")[1],dt.split("/")[0])
                        all_book_odds = self.get_empty_odds_dict()
                        for o in ev.getElementsByTagName('o'):
                            if(o.attributes["d"].value == "1"):
                                all_book_odds["home"] = float(o.attributes["odd"].value)/100
                            elif(o.attributes["d"].value == "X"):
                                all_book_odds["draw"] = float(o.attributes["odd"].value)/100
                            elif(o.attributes["d"].value == "2"):
                                all_book_odds["away"] = float(o.attributes["odd"].value)/100
 
                        if(all_book_odds["home"] != "" and all_book_odds["draw"] != "" and all_book_odds["away"] != ""):  
                            open_date = (dateparser.parse(dt+" "+tm+":00")).strftime('%Y-%m-%d %H:%M:%S')
                            event_hash = hashlib.md5(open_date+name).hexdigest()
                            #print name,open_date
                            bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                            if not bf_event:
                                print name, "no bf_event, skipping"
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue    
                            if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":
                                url = "https://snaigatewaybettingand.game360.it:7243/BettingMobileGatewaySnai/service30?op=ls&a={0}&fante=0&tf=and".format(ev.attributes["id"].value.strip())
                                req = urllib2.Request(url)
                                self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
                                self.opener.addheaders = [('Connection', 'keep-alive'),('Cookie2','$Version=1')]  
                                resp1 = self.opener.open(req)
                                resp1 = resp1.read()
                                resp1 = resp1.encode('utf-8')
                                xmldoc1 = minidom.parseString(resp1)
                                for b in xmldoc1.getElementsByTagName('b'):
                                    if(b.attributes["d"].value == "G/NG" or b.attributes["d"].value == "GOL/NOGOL"):
                                        for o in b.getElementsByTagName('o'):
                                            if(o.attributes["d"].value == "GOL"):
                                                all_book_odds["goal"] = float(o.attributes["odd"].value)/100
                                            elif(o.attributes["d"].value == "NOGOL"):
                                                all_book_odds["no_goal"] = float(o.attributes["odd"].value)/100
                                    elif(b.attributes["d"].value == "U/O 0.5" or b.attributes["d"].value == "UNDER/OVER 0.5"):
                                        for o in b.getElementsByTagName('o'):
                                            if(o.attributes["d"].value == "UNDER"):
                                                all_book_odds["under_05"] = float(o.attributes["odd"].value)/100
                                            elif(o.attributes["d"].value == "OVER"):
                                                all_book_odds["over_05"] = float(o.attributes["odd"].value)/100
                                    elif(b.attributes["d"].value == "U/O 1.5" or b.attributes["d"].value == "UNDER/OVER 1.5"):
                                        for o in b.getElementsByTagName('o'):
                                            if(o.attributes["d"].value == "UNDER"):
                                                all_book_odds["under_15"] = float(o.attributes["odd"].value)/100
                                            elif(o.attributes["d"].value == "OVER"):
                                                all_book_odds["over_15"] = float(o.attributes["odd"].value)/100
                                    elif(b.attributes["d"].value == "U/O 2.5" or b.attributes["d"].value == "UNDER/OVER 2.5"):
                                        for o in b.getElementsByTagName('o'):
                                            if(o.attributes["d"].value == "UNDER"):
                                                all_book_odds["under_25"] = float(o.attributes["odd"].value)/100
                                            elif(o.attributes["d"].value == "OVER"):
                                                all_book_odds["over_25"] = float(o.attributes["odd"].value)/100
                                    elif(b.attributes["d"].value == "U/O 3.5" or b.attributes["d"].value == "UNDER/OVER 3.5"):
                                        for o in b.getElementsByTagName('o'):
                                            if(o.attributes["d"].value == "UNDER"):
                                                all_book_odds["under_35"] = float(o.attributes["odd"].value)/100
                                            elif(o.attributes["d"].value == "OVER"):
                                                all_book_odds["over_35"] = float(o.attributes["odd"].value)/100
                                    elif(b.attributes["d"].value == "U/O 4.5" or b.attributes["d"].value == "UNDER/OVER 4.5"):
                                        for o in b.getElementsByTagName('o'):
                                            if(o.attributes["d"].value == "UNDER"):
                                                all_book_odds["under_45"] = float(o.attributes["odd"].value)/100
                                            elif(o.attributes["d"].value == "OVER"):
                                                all_book_odds["over_45"] = float(o.attributes["odd"].value)/100

                            if not self.check_rating(all_book_odds, bf_event):
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue

                            print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]
                            event_found = True
                            self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        #print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Snai(bot_data)
t.run()