# -*- coding: utf-8 -*-
import cookielib
from cookielib import CookieJar
import copy
import sys
import os
import codecs
import mysql.connector
import json
import datetime
import pytz
import requests
from time import time, sleep
import re
import random
import urllib
import urllib2
from BeautifulSoup import *
import hashlib
import threading
import urlparse
import dateparser
from fractions import Fraction
import ssl
from xml.dom import minidom
from StringIO import StringIO
import gzip
import dateparser
import distance
reload(sys)
sys.setdefaultencoding('utf-8')

class BetflagApi():
    def __init__(self):
        self.username = "test1tap"
        self.password = "test1tap"
        self.appkey = "abtest-rest-k"


    def login(self):
        url = "http://exchange.staging.betflag.it/api/rest/Security/Session"
        postdata = '{"Username":"'+self.username+'","Pwd":"'+self.password+'"}'
        allHeaders = {'X-BFkey':self.appkey,'Content-Type': 'application/json'}
        r = requests.post(url, data=postdata, headers=allHeaders).json()
        self.sessionToken = r["SessionToken"]
        print r
    def KeepAlive(self):
        url = "http://exchange.staging.betflag.it/api/rest/Security/KeepAlive/"
        allHeaders = {'X-BFkey':self.appkey,'Content-Type': 'application/json'}
        r = requests.post(url, data=postdata, headers=allHeaders).text
        if("True" not in r):
            self.login()

    def GetEvents(self):
        url = "http://exchange.staging.betflag.it/api/rest/Navigation/Avv/Inarrivo/"
        allHeaders = {'X-BFkey':self.appkey,'Content-Type': 'application/json'}
        r = requests.get(url, headers=allHeaders).json()
        print r

    def GetOpenOffers(self):
        url = "http://exchange.staging.betflag.it/api/rest/Offers/Opened/"
        allHeaders = {'X-BFkey':self.appkey,'X-BF-Session-Token': self.sessionToken}
        r = requests.get(url, headers=allHeaders).json()
        print r

    def GetMarketOffers(self):
        url = "http://exchange.staging.betflag.it/api/rest/Offers/Summary/233_51_3_0/"
        allHeaders = {'X-BFkey':self.appkey,'X-BF-Session-Token': self.sessionToken}
        r = requests.get(url, headers=allHeaders).json()
        print r


    def PlaceBet(self):
        url = "http://exchange.staging.betflag.it/api/rest/Offers/"
        postdata = '{"offerta":[{"tipo":0,"modalita":0,"esito":1,"quota":150,"handicap":0,"importo":100,"max_esposizione":50}],"sezione":null,"operatore":0,"palinsesto":27380,"avvenimento":1467,"scommessa":63,   "puntata":100,   "n_offerte":1,   "ritira":1,   "mantieni":1}'
        allHeaders = {'X-BFkey':self.appkey,'X-BF-Session-Token': self.sessionToken,'Content-Type': 'application/json'}
        r = requests.post(url, data=postdata, headers=allHeaders).json()
        print r


class BetflagExchange(threading.Thread):
    def __init__(self,settings,lock,proxies):
        threading.Thread.__init__(self)
        self.all_proxies = proxies
        self.db = mysql.connector.connect(user=settings['username'], password=settings['password'], database=settings['database'])
        self.lock = lock
        cur = self.db.cursor()
        cur.execute("SET NAMES 'utf8'")
        cur.execute("SET CHARACTER SET utf8")
        cur.close()
        self._stop = threading.Event()
        self.all_odds = dict()
        self.matched_events = dict()

    def stop(self):
        self._stop.set()

    def loadmatchedevents(self):
        sql = "select betfair,bookie from matched_events where bookie_name like '{0}'".format(self.__class__.__name__.lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        if(len(rows)>0):
            for row in rows:
                if(row[1] not in self.matched_events):
                    self.matched_events[row[1]] = row[0]

        return self.matched_events

    def setbookie(self):
        sql = "select id,name,full_name,website,coupon_url from providers where name like '{0}'".format(self.__class__.__name__.lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        self.bookie = dict()
        self.bookie["id"] = str(rows[0][0])
        self.bookie["name"] = str(rows[0][1])
        self.bookie["full_name"] = str(rows[0][2])
        self.bookie["website"] = str(rows[0][3])
        self.bookie["coupon_url"] = str(rows[0][4])

    def stopped(self):
        return self._stop.isSet()

    def getcomparedmatches(self,country_code):
        sql = "select id ,bf_id,event_name,bet,market_type,rating,snr_rating,country_code,competition ,odds_provider_fk,odds_provider,back_odds ,lay_odds ,availability,DATE_ADD(opendate,INTERVAL 1 HOUR) as opendate,bookie_bet_url,betfair_bet_url ,exchange,sport ,update_time,hide  from compare_table where exchange='betfair' and sport='soccer' and rating <100 and country_code like '{0}'".format(country_code)
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        bf_data = []
        for row in rows:
            bf = dict()
            bf["id"] = str(row[0])
            bf["event_name"] = row[2]
            bf["home"] = row[2].split(" v ")[0]
            bf["away"] = row[2].split(" v ")[1]
            bf["bet"] = row[3]
            bf["market_type"] = str(row[4])
            bf["rating"] = str(row[5])
            bf["snr_rating"] = str(row[6])
            bf["country_code"] = str(row[7])
            bf["competition"] = str(row[8])
            bf["odds_provider_fk"] = str(row[9])
            bf["odds_provider"] = str(row[10])
            bf["back_odds"] = str(row[11])
            bf["lay_odds"] = str(row[12])
            bf["availability"] = str(row[13])
            bf["opendate"] = str(row[14])
            bf["bookie_bet_url"] = str(row[15])
            bf["betfair_bet_url"] = str(row[16])
            bf["exchange"] = str(row[17])
            bf["sport"] = str(row[18])
            bf["update_time"] = str(row[19])
            bf["hide"] = str(row[20])
            bf_data.append(bf)
        if(len(rows) ==0):
            sql = "select id ,bf_id,event_name,bet,market_type,rating,snr_rating,country_code,competition ,odds_provider_fk,odds_provider,back_odds ,lay_odds ,availability,DATE_ADD(opendate,INTERVAL 1 HOUR) as opendate,bookie_bet_url,betfair_bet_url ,exchange,sport ,update_time,hide  from compare_table where exchange='betfair' and sport='soccer' and rating <100"
            cur = self.db.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            cur.close()
            for row in rows:
                bf = dict()
                bf["id"] = str(row[0])
                bf["event_name"] = row[2]
                bf["home"] = row[2].split(" v ")[0]
                bf["away"] = row[2].split(" v ")[1]
                bf["bet"] = row[3]
                bf["market_type"] = str(row[4])
                bf["rating"] = str(row[5])
                bf["snr_rating"] = str(row[6])
                bf["country_code"] = str(row[7])
                bf["competition"] = str(row[8])
                bf["odds_provider_fk"] = str(row[9])
                bf["odds_provider"] = str(row[10])
                bf["back_odds"] = str(row[11])
                bf["lay_odds"] = str(row[12])
                bf["availability"] = str(row[13])
                bf["opendate"] = str(row[14])
                bf["bookie_bet_url"] = str(row[15])
                bf["betfair_bet_url"] = str(row[16])
                bf["exchange"] = str(row[17])
                bf["sport"] = str(row[18])
                bf["update_time"] = str(row[19])
                bf["hide"] = str(row[20])
                bf_data.append(bf)
        return bf_data

    def getbetfairmatches(self):
        sql = "select o.id,name,home,away,open_date,home_odds,home_price,away_price,draw_price,o_25,o_25_price,u_25,u_25_price,goal,goal_price,no_goal,no_goal_price,country_code,competition,away_odds,draw_odds,market_id,market_id2,market_id3 from bf_match_odds as o natural join bf_events as e"
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        bf_data = []
        for row in rows:
            bf = dict()
            bf["id"] = str(row[0])
            bf["name"] = row[1]
            bf["home"] = row[2]
            bf["away"] = row[3]
            bf["open_date"] = str(row[4])
            bf["home_odds"] = str(row[5])
            bf["home_price"] = str(row[6])
            bf["away_price"] = str(row[7])
            bf["draw_price"] = str(row[8])
            bf["over_odds"] = str(row[9])
            bf["over_price"] = str(row[10])
            bf["under_odds"] = str(row[11])
            bf["under_price"] = str(row[12])
            bf["goal_odds"] = str(row[13])
            bf["goal_price"] = str(row[14])
            bf["no_goal_odds"] = str(row[15])
            bf["no_goal_price"] = str(row[16])
            bf["country_code"] = str(row[17])
            bf["competition"] = str(row[18])
            bf["away_odds"] = str(row[19])
            bf["draw_odds"] = str(row[20])
            bf["market_id"] = str(row[21])
            bf["market_id2"] = str(row[22])
            bf["market_id3"] = str(row[23])
            bf_data.append(bf)
        return bf_data

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def savedata(self,id,name,home,away,open_date,home_odds,draw_odds,away_odds,betfair_data,bookie_id,bookie_name,bookie_url,exchange_name):
        SQL = ""
        if home.upper()+" v "+away.upper() in self.matched_events:
            old_event = home.upper()+" v "+away.upper()
            home = self.matched_events[old_event].split(" v ")[0].strip()
            away = self.matched_events[old_event].split(" v ")[1].strip()
        bf = None
        for bf_row in betfair_data:
            if(open_date.split(" ")[0] != bf_row["open_date"].split(" ")[0]):
                continue
            if(home.lower() == bf_row["home"].lower() or away.lower() == bf_row["away"].lower() or home.lower().startswith(bf_row["home"].lower()) and away.lower().startswith(bf_row["away"].lower())  or bf_row["home"].lower().startswith(home.lower()) and bf_row["away"].lower().startswith(away.lower())):
                bf = bf_row
                break
        if(bf==None and id not in self.blacklist):
            try:
                url = "https://www.betfair.com/exchange/search?&modules=instantsearch-ebs&isAjax=True&container=false&query="+urllib.quote_plus(name.encode('utf8'))+"&alt=json"
                req = urllib2.Request(url)
                resp = self.opener.open(req)
                if resp.info().get('Content-Encoding') == 'gzip':
                    buf = StringIO(resp.read())
                    resp = gzip.GzipFile(fileobj=buf)
                resp = resp.read()
                soup = BeautifulSoup(resp)
                spans = soup.find("span")

                if(spans == None):
                    self.blacklist.append(id)
                    return
                spans = spans.findAll("span")

                if(spans != None and len(spans)>1):
                    evnt = spans[0].text.replace(" v "," - ")
                    if(len(evnt.split(" - "))>1):
                        h = evnt.split(" - ")[0].strip()
                        a = evnt.split(" - ")[1].strip()
                        d = spans[1].text.split('|')
                        d = d[len(d)-1].strip()+":00"

                        SQL1=""
                        if(open_date == dateparser.parse(d).strftime("%Y-%m-%d %H:%M:%S") and distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())<13):#or  abs((dateparser.parse(str(open_date)) - dateparser.parse(d)).total_seconds())<900 and distance.levenshtein(evnt.upper(),name.upper())<13):
                            for bf_row in betfair_data:
                                if(h.lower() == bf_row["home"].lower() and a.lower() == bf_row["away"].lower()):
                                    bf = bf_row
                                    self.matched_events[home.upper()+" v "+away.upper()] = h.upper()+" v "+a.upper()
                                    id1 = hashlib.md5(h+a+self.__class__.__name__.upper()).hexdigest()
                                    SQL1 = "insert ignore into matched_events (id,betfair,bookie,bookie_name) values ('{0}','{1}','{2}','{3}'); ".format(id1,h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper(),self.__class__.__name__.upper())
                                    cur = self.db.cursor()
                                    for result in cur.execute(SQL1,multi = True):
                                        pass
                                    self.db.commit()
                                    cur.close()
                                    break

            except Exception,msg:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)

        if(bf==None):
            self.blacklist.append(id)
            return
        if(bf["home_odds"]!="None" and bf["home_odds"]!="" and home_odds!= ""):
            bf_stake =  (float(home_odds)*100)/(float(bf["home_odds"])-0.05)
            profit = ((bf_stake*(1-0.05))-100)
            rating = round(100+profit,2)

            backOdds = float(home_odds)
            layOdds = float(bf["home_odds"])
            backCommission = 0
            layCommission = 0.05
            backStake = 100
            layStake =  backStake*backOdds/(layOdds-layCommission)
            refund = 25

            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
            netresultlaybackwin  = refund-backStake
            netresultbackbackwin = ((backStake * backOdds) - backStake)
            liability = layStake*layOdds
            netresultbacklaywin =  (-1 * liability)
            netresultlaylaywin = (layStake * (1-layCommission))
            laywinprofit = netresultlaybackwin + netresultlaylaywin
            backwinprofit = netresultbackbackwin + netresultbacklaywin
            snr = laywinprofit
            if(snr>backwinprofit):
                snr = backwinprofit

            snr = round((((float)(snr*100))/refund),2)
            key = exchange_name+"_"+bookie_name+"_"+id+"_home_"+hashlib.md5(bf["id"]).hexdigest()
            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],bf["home"],"1x2",rating,bookie_id,bookie_name,home_odds,bf["home_odds"],bf["home_price"],bf["open_date"],snr,bookie_url,"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],exchange_name,snr,rating,home_odds,bf["home_odds"],bf["home_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])

        if(bf["away_odds"]!="None") and bf["away_odds"]!="" and away_odds!= "":
            bf_stake =  (float(away_odds)*100)/(float(bf["away_odds"])-0.05)
            profit = ((bf_stake*(1-0.05))-100)
            rating = round(100+profit,2)

            backOdds = float(away_odds)
            layOdds = float(bf["away_odds"])
            backCommission = 0
            layCommission = 0.05
            backStake = 100
            layStake =  backStake*backOdds/(layOdds-layCommission)
            refund = 25

            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
            netresultlaybackwin  = refund-backStake
            netresultbackbackwin = ((backStake * backOdds) - backStake)
            liability = layStake*layOdds
            netresultbacklaywin =  (-1 * liability)
            netresultlaylaywin = (layStake * (1-layCommission))
            laywinprofit = netresultlaybackwin + netresultlaylaywin
            backwinprofit = netresultbackbackwin + netresultbacklaywin
            snr = laywinprofit
            if(snr>backwinprofit):
                snr = backwinprofit
            snr = round((((float)(snr*100))/refund),2)
            key = exchange_name+"_"+bookie_name+"_"+id+"_away_"+hashlib.md5(bf["id"]).hexdigest()
            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],bf["away"],"1x2",rating,bookie_id,bookie_name,away_odds,bf["away_odds"],bf["away_price"],bf["open_date"],snr,bookie_url,"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],exchange_name,snr,rating,away_odds,bf["away_odds"],bf["away_price"],bookie_url,"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])

        if(bf["draw_odds"]!="None" and bf["draw_odds"]!="" and draw_odds!= ""):
            bf_stake =  (float(draw_odds)*100)/(float(bf["draw_odds"])-0.05)
            profit = ((bf_stake*(1-0.05))-100)
            rating = round(100+profit,2)

            backOdds = float(draw_odds)
            layOdds = float(bf["draw_odds"])
            backCommission = 0
            layCommission = 0.05
            backStake = 100
            layStake =  backStake*backOdds/(layOdds-layCommission)
            refund = 25

            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
            netresultlaybackwin  = refund-backStake
            netresultbackbackwin = ((backStake * backOdds) - backStake)
            liability = layStake*layOdds
            netresultbacklaywin =  (-1 * liability)
            netresultlaylaywin = (layStake * (1-layCommission))
            laywinprofit = netresultlaybackwin + netresultlaylaywin
            backwinprofit = netresultbackbackwin + netresultbacklaywin
            snr = laywinprofit
            if(snr>backwinprofit):
                snr = backwinprofit

            snr = round((((float)(snr*100))/refund),2)
            key = exchange_name+"_"+bookie_name+"_"+id+"_draw_"+hashlib.md5(bf["id"]).hexdigest()
            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"Pareggio","1x2",rating,bookie_id,bookie_name,draw_odds,bf["draw_odds"],bf["draw_price"],bf["open_date"],snr,bookie_url,"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],exchange_name,snr,rating,draw_odds,bf["draw_odds"],bf["draw_price"],bookie_url,"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])

        try:
            if SQL != "":
                cur = self.db.cursor()
                for result in cur.execute(SQL,multi = True):
                    pass
                self.db.commit()
                cur.close()
                SQL = ""
        except Exception,msg:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
            SQL = ""

    def run(self):
        self.setbookie()
        self.loadmatchedevents()
        self.cj = CookieJar()
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        x = random.randint(0,79)
        all_leagues = ["247:141","158:29","242:50","72:111","69:63","248:153","248:18","242:86","20:7","65:14","65:15","69:4","69:3","112:21","112:22","168:54","199:79","199:137","241:68","242:1","249:132","249:105","65:49","64:17","242:72","242:134","242:109","242:110","0:553","112:47","112:122","154:27","199:60","208:66"]
        self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx),urllib2.HTTPCookieProcessor(self.cj),urllib2.ProxyHandler({'https': 'http://ninjabet:NINJAfrancavilla1987@'+self.all_proxies[x]}))
        self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://www.betflag.it/sport'),('Connection', 'keep-alive'),('Content-Type','application/json; charset=UTF-8'),('Accept-Encoding','gzip, deflate, sdch, br')]
        url = "https://www.betflag.it/networkexchange/default.aspx?cID=62E01EFF-FD52-4D36-84C1-FF1EF0D413DD&cIP=154.122.65.15&F_CB=RefreshToolbar&gTRC=UA-24481188-1"
        req = urllib2.Request(url)
        resp = self.opener.open(req)
        if resp.info().get('Content-Encoding') == 'gzip':
            buf = StringIO(resp.read())
            resp = gzip.GzipFile(fileobj=buf)
        resp = resp.read().replace("\n","").replace("\r","").replace("\t","").replace("&nbsp;"," ")
        soup = BeautifulSoup(resp,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
        all_leagues = [l[2].strip()+":"+l[3].strip() for l in [span["onclick"].split(",") for span in soup.findAll("span",{"title":"CALCIO"})[1].findNext("ul").findAll("span") if span.has_key("title")] if l[3].strip() != "false"]
        self.blacklist=[]
        print all_leagues
        while self._stop.isSet() == False:
            SQL = ""
            try:
                self.cj = CookieJar()
                all_events = []
                for l in all_leagues:
                    try:
                        if(self._stop.isSet()):
                            return

                        x = random.randint(0,79)
                        self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx),urllib2.HTTPCookieProcessor(self.cj),urllib2.ProxyHandler({'https': 'http://ninjabet:NINJAfrancavilla1987@'+self.all_proxies[x]}))
                        self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://www.betflag.it/sport'),('Connection', 'keep-alive'),('Content-Type','application/json; charset=UTF-8')]
                        sleep(random.randint(0,3))
                        postdata = '{"idS":"1", "idN":"'+l.split(":")[0]+'", "idM":"'+l.split(":")[1]+'", "isLive":"false", "menuCall":"0"}'

                        url = "https://exchange-fe.mediasystemtechnologies.it/WSData.asmx/GetHtmlMunuEvents?nxc=018848"
                        req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                        resp = self.opener.open(req)
                        resp = json.loads(resp.read())
                        htmls = resp["d"][0]["HTMLS"]
                        htmls = htmls.encode('utf-8').replace(u'\xa0', u' ')
                        soup = BeautifulSoup(htmls,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
                        soup = soup.find("h3").text.split(",")
                        country_code = soup[0].strip().capitalize()
                        competition = soup[1].strip().capitalize()

                        resp = resp["d"][0]["HTML"]
                        resp = resp.encode('utf-8').replace(u'\xa0', u' ')
                        soup = BeautifulSoup(resp,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
                        for span in soup.findAll("span"):
                            if span.has_key("title") and span.has_key("onclick"):
                                dt = span["title"].split("  ")
                                name = self.db.converter.escape(dt[0])
                                if(len(name.split(" - "))<2):
                                    continue
                                home = name.split(" - ")[0]
                                away = name.split(" - ")[1]
                                dt = dt[1].replace(".",":").strip().split(" ")
                                open_date = dt = "{0}-{1}-{2} {3}".format(dt[0].split("/")[2],dt[0].split("/")[1],dt[0].split("/")[0],dt[1]+":00")
                                open_date = (dateparser.parse(open_date) - datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')
                                ids = span["onclick"].replace("'","").split(",")
                                pal = str(ids[3])
                                avv = str(ids[4])
                                sco = "63"
                                han = "0"

                                postdata = '{"pal":"'+pal+'", "avv":"'+avv+'", "sco":"3", "han":"0", "refresh":"false",caller: "setScoListEvent"}'
                                #postdata = '{"pal":"28071", "avv":"46785", "sco":"3", "han":"0", "refresh":"false", "caller":"setScoListEvent"}'
                                url = "https://exchange-fe.mediasystemtechnologies.it/WSData.asmx/Get_XMLOfferSection?nxc=018848"
                                req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                                resp = self.opener.open(req)
                                resp = json.loads(resp.read())
                                if("d" not in resp or "Off" not in resp["d"][0]):
                                    continue

                                off = resp["d"][0]["Off"].encode('utf-8').replace(u'\xa0', u' ')
                                xmldoc = minidom.parseString(off)
                                banca = xmldoc.getElementsByTagName('banca')[0]
                                punta = xmldoc.getElementsByTagName('punta')[0]

                                back_home_odds = "NULL"
                                back_draw_odds = "NULL"
                                back_away_odds = "NULL"

                                back_home_size = "NULL"
                                back_draw_size = "NULL"
                                back_away_size = "NULL"

                                lay_home_odds = "NULL"
                                lay_draw_odds = "NULL"
                                lay_away_odds = "NULL"

                                lay_home_size = "NULL"
                                lay_draw_size = "NULL"
                                lay_away_size = "NULL"
                                home_odds = ""
                                draw_odds = ""
                                away_odds = ""
                                back_over_odds = "NULL"
                                back_under_odds = "NULL"

                                back_over_size = "NULL"
                                back_under_size = "NULL"

                                lay_over_odds = "NULL"
                                lay_under_odds = "NULL"

                                lay_over_size = "NULL"
                                lay_under_size = "NULL"

                                back_goal_odds = "NULL"
                                back_nogoal_odds = "NULL"

                                back_goal_size = "NULL"
                                back_nogoal_size = "NULL"

                                lay_goal_odds = "NULL"
                                lay_nogoal_odds = "NULL"

                                lay_goal_size = "NULL"
                                lay_nogoal_size = "NULL"

                                for node in banca.getElementsByTagName('esi'):
                                    if(node.attributes["c"].value == "1"):
                                        home_odds = back_home_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                        back_home_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                    elif(node.attributes["c"].value == "2"):
                                        draw_odds = back_draw_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                        back_draw_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                    elif(node.attributes["c"].value == "3"):
                                        away_odds = back_away_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                        back_away_size =  float(node.childNodes[0].attributes["ri"].value)/100;

                                for node in punta.getElementsByTagName('esi'):
                                    if(node.attributes["c"].value == "1"):
                                        lay_home_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                        lay_home_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                    elif(node.attributes["c"].value == "2"):
                                        lay_draw_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                        lay_draw_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                    elif(node.attributes["c"].value == "3"):
                                        lay_away_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                        lay_away_size =  float(node.childNodes[0].attributes["ri"].value)/100;

                                if(lay_home_odds == "NULL" and lay_draw_odds == "NULL" and lay_away_odds == "NULL"):
                                    continue
                                name = name.replace(" - "," v ")
                                id = hashlib.md5(dt+home+away).hexdigest()
                                if(False and home_odds != "" and away_odds != "" and draw_odds != ""):
                                    betfair_data = self.getbetfairmatches()
                                    self.savedata(id,name,home,away,open_date,home_odds,draw_odds,away_odds,betfair_data,self.bookie["id"],self.bookie["name"],self.bookie["coupon_url"],"betfair")
                                print name,open_date,country_code,lay_home_odds,lay_home_size,lay_away_odds,lay_away_size
                                postdata = '{"pal":"'+pal+'", "avv":"'+avv+'", "sco":"80", "han":"0", "refresh":"false",caller: "refSEMM"}'
                                url = "https://exchange-fe.mediasystemtechnologies.it/WSData.asmx/Get_XMLOffer?nxc=749589"
                                req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                                resp = self.opener.open(req)
                                resp = json.loads(resp.read())
                                if("d" in resp and "Off" in resp["d"][0]):
                                    off = resp["d"][0]["Off"].encode('utf-8').replace(u'\xa0', u' ')
                                    xmldoc = minidom.parseString(off)
                                    banca = xmldoc.getElementsByTagName('banca')[0]
                                    punta = xmldoc.getElementsByTagName('punta')[0]
                                    for node in banca.getElementsByTagName('esi'):
                                        if(node.attributes["c"].value == "1"):
                                            back_under_odds = back_home_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            back_under_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                        elif(node.attributes["c"].value == "2"):
                                            back_over_odds = back_draw_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            back_over_size =  float(node.childNodes[0].attributes["ri"].value)/100;

                                    for node in punta.getElementsByTagName('esi'):
                                        if(node.attributes["c"].value == "1"):
                                            lay_under_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            lay_under_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                        elif(node.attributes["c"].value == "2"):
                                            lay_over_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            lay_over_size =  float(node.childNodes[0].attributes["ri"].value)/100;


                                postdata = '{"pal":"'+pal+'", "avv":"'+avv+'", "sco":"583", "han":"0", "refresh":"false",caller: "refSEMM"}'
                                url = "https://exchange-fe.mediasystemtechnologies.it/WSData.asmx/Get_XMLOffer?nxc=749589"
                                req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                                resp = self.opener.open(req)
                                resp = json.loads(resp.read())
                                if("d" in resp and "Off" in resp["d"][0]):
                                    off = resp["d"][0]["Off"].encode('utf-8').replace(u'\xa0', u' ')
                                    xmldoc = minidom.parseString(off)
                                    banca = xmldoc.getElementsByTagName('banca')[0]
                                    punta = xmldoc.getElementsByTagName('punta')[0]

                                    for node in banca.getElementsByTagName('esi'):
                                        if(node.attributes["c"].value == "1"):
                                            back_goal_odds = back_home_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            back_goal_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                        elif(node.attributes["c"].value == "2"):
                                            back_nogoal_odds = back_draw_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            back_nogoal_size =  float(node.childNodes[0].attributes["ri"].value)/100;

                                    for node in punta.getElementsByTagName('esi'):
                                        if(node.attributes["c"].value == "1"):
                                            lay_goal_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            lay_goal_size =  float(node.childNodes[0].attributes["ri"].value)/100;
                                        elif(node.attributes["c"].value == "2"):
                                            lay_nogoal_odds = float(node.childNodes[0].attributes["q"].value)/100;
                                            lay_nogoal_size =  float(node.childNodes[0].attributes["ri"].value)/100;

                                compare_data = self.getcomparedmatches(country_code)
                                br = None
                                changed = False
                                if home.upper()+" v "+away.upper() in self.matched_events:
                                    old_event = home.upper()+" v "+away.upper()
                                    home = self.matched_events[old_event].split(" v ")[0].strip()
                                    away = self.matched_events[old_event].split(" v ")[1].strip()
                                    changed = True
                                for b in compare_data:
                                    if(b["opendate"] != open_date):
                                        continue
                                    if(b["event_name"].split(" v ")[0].strip().lower() == home.strip().lower() or b["event_name"].split(" v ")[1].strip().lower() == away.strip().lower() or home.lower().startswith(b["event_name"].split(" v ")[0].lower()) and away.lower().startswith(b["event_name"].split(" v ")[1].lower())  or b["event_name"].split(" v ")[0].lower().startswith(home.lower()) and b["event_name"].split(" v ")[1].lower().startswith(away.lower())):
                                        br = b
                                        #print name,b['odds_provider'],open_date,br["event_name"],br["opendate"]
                                        if(lay_under_odds != "NULL" and br["bet"] == "UNDER 2.5"):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_under_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_under_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_under_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"OVER/UNDER",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_under_odds,lay_under_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_under_odds,lay_under_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")

                                        elif(lay_over_odds != "NULL" and br["bet"] == "OVER 2.5"):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_over_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_over_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_over_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"OVER/UNDER",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_over_odds,lay_over_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_over_odds,lay_over_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")

                                        elif(lay_goal_odds != "NULL" and br["bet"] == "GOAL"):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_goal_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_goal_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_goal_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"GOAL/NOGOAL",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_goal_odds,lay_goal_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_goal_odds,lay_goal_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")

                                        elif(lay_nogoal_odds != "NULL" and br["bet"] == "NOGOAL"):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_nogoal_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_nogoal_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_no_goal_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"GOAL/NOGOAL",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_nogoal_odds,lay_nogoal_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_nogoal_odds,lay_nogoal_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")

                                        elif(lay_draw_odds != "NULL" and br["bet"] == "Pareggio"):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_draw_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_draw_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_draw_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"1x2",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_draw_odds,lay_draw_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_draw_odds,lay_draw_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")

                                        elif(lay_home_odds != "NULL" and br["event_name"].startswith(br["bet"])):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_home_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_home_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_home_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"1x2",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_home_odds,lay_home_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_home_odds,lay_home_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")

                                        elif(lay_away_odds != "NULL" and br["event_name"].endswith(br["bet"])):
                                            bf_stake =  (float(br["back_odds"])*100)/(float(lay_away_odds)-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(br["back_odds"])
                                            layOdds = float(lay_away_odds)
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = "Betflag"+"_"+br["odds_provider"]+"_"+id+"_away_"+hashlib.md5(br["id"]).hexdigest()
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,self.db.converter.escape(br["event_name"]),self.db.converter.escape(br["bet"]),"1x2",rating,br["odds_provider_fk"],br["odds_provider"],br["back_odds"],lay_away_odds,lay_away_size,br["opendate"],snr,br["bookie_bet_url"],"https://www.betflag.it/exchange",br["competition"],br["country_code"],"betflag",snr,rating,br["back_odds"],lay_away_odds,lay_away_size,br["bookie_bet_url"],"https://www.betflag.it/exchange")


                                        try:
                                            if SQL != "" and len(SQL.split(';'))>2:
                                                cur = self.db.cursor()
                                                for result in cur.execute(SQL,multi = True):
                                                    pass
                                                self.db.commit()
                                                cur.close()
                                                SQL = ""
                                        except Exception,msg:
                                            exc_type, exc_obj, exc_tb = sys.exc_info()
                                            print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                        if SQL != "":
                            cur = self.db.cursor()
                            for result in cur.execute(SQL,multi = True):
                                pass
                            self.db.commit()
                            cur.close()
                            SQL = ""


                    except Exception,msg:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)


            except Exception,msg:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)

            sleep(random.randint(0,5))
settings = {
    'username': 'root',
    'password': 'welSOFT1010',
    'database': 'ninjabet'
}
file = os.path.join(os.path.dirname(os.path.abspath(__file__)),"proxies.txt")
file = open(file,"r")
proxies = [line.strip() for line in file.readlines()]
lock = threading.Lock()
b = BetflagExchange(settings,lock,proxies)
#b = BetflagApi()
#b.login()
#b.GetEvents()
#b.GetOpenOffers()
#b.GetMarketOffers()
#b.PlaceBet()
b.run()
