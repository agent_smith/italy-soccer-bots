import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Goldbet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
            "X-Requested-With":"XMLHttpRequest",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.goldbet.it/getProgram/sport/0"
            # main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
            main_page_request = requests.get(url, headers=self.custom_headers)
            main_page_json = json.loads(main_page_request.content)

            for sport in main_page_json:
                if sport["n"] == "Calcio":
                    for league in sport["lc"]:
                        if "OGGI" not in league["n"] and "Speciali" not in league["n"] and "Fantasy" not in league["n"]:
                            for subleague in league["lts"]:
                                all_leagues.append(subleague["id"])

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://www.goldbet.it/getOverviewEvents/0?idDiscipline=1&idTournament={0}&idSection=0".format(league)
                    # league_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                    league_page_request = requests.get(url, headers=self.custom_headers)
                    league_page_json = json.loads(league_page_request.content)

                    self.init_betfair_events()
                    for event in league_page_json["leo"]:
                        open_date = datetime.datetime.strptime(event["ed"] + ":00", '%d-%m-%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                        home, away = [team.strip() for team in event["en"].split(" - ")]
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()

                        for market in event["mmkW"]:
                            if event["mmkW"][market]["mn"] == "1X2":
                                for outcome in event["mmkW"][market]["spd"]["0.0"]["asl"]:
                                    if outcome["sn"] == "1":
                                        all_book_odds["home"] = float(outcome["ov"])
                                    if outcome["sn"] == "2":
                                        all_book_odds["away"] = float(outcome["ov"])
                                    if outcome["sn"] == "X":
                                        all_book_odds["draw"] = float(outcome["ov"])
                            if event["mmkW"][market]["mn"] == "U/O":
                                for uo_type in event["mmkW"][market]["spd"]:
                                    if uo_type == "0.5":
                                        for outcome in event["mmkW"][market]["spd"][uo_type]["asl"]:
                                            if outcome["sn"] == "U":
                                                all_book_odds["under_05"] = float(outcome["ov"])
                                            elif outcome ["sn"] == "O":
                                                all_book_odds["over_05"] = float(outcome["ov"])
                                    if uo_type == "1.5":
                                        for outcome in event["mmkW"][market]["spd"][uo_type]["asl"]:
                                            if outcome["sn"] == "U":
                                                all_book_odds["under_15"] = float(outcome["ov"])
                                            elif outcome ["sn"] == "O":
                                                all_book_odds["over_15"] = float(outcome["ov"])
                                    if uo_type == "2.5":
                                        for outcome in event["mmkW"][market]["spd"][uo_type]["asl"]:
                                            if outcome["sn"] == "U":
                                                all_book_odds["under_25"] = float(outcome["ov"])
                                            elif outcome ["sn"] == "O":
                                                all_book_odds["over_25"] = float(outcome["ov"])
                                    if uo_type == "3.5":
                                        for outcome in event["mmkW"][market]["spd"][uo_type]["asl"]:
                                            if outcome["sn"] == "U":
                                                all_book_odds["under_35"] = float(outcome["ov"])
                                            elif outcome ["sn"] == "O":
                                                all_book_odds["over_35"] = float(outcome["ov"])
                                    if uo_type == "4.5":
                                        for outcome in event["mmkW"][market]["spd"][uo_type]["asl"]:
                                            if outcome["sn"] == "U":
                                                all_book_odds["under_45"] = float(outcome["ov"])
                                            elif outcome ["sn"] == "O":
                                                all_book_odds["over_45"] = float(outcome["ov"])
                            if event["mmkW"][market]["mn"] == "GG/NG":
                                for outcome in event["mmkW"][market]["spd"]["0.0"]["asl"]:
                                    if outcome["sn"] == "GG":
                                        all_book_odds["goal"] = float(outcome["ov"])
                                    if outcome["sn"] == "NG":
                                        all_book_odds["no_goal"] = float(outcome["ov"])

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "188.166.34.148",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Goldbet(bot_data)
t.run()
