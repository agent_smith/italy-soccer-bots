import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Sisal(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "Host":"www.sisal.it",
            "Referer":"https://www.sisal.it/scommesse-matchpoint",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
            "X-Requested-With":"XMLHttpRequest",
        }

    def get_leagues(self):
        all_leagues = []
        try:
            url = "https://www.sisal.it/scommesse-matchpoint?p_p_id=AlberaturaAntepost_WAR_scommesseportlet&p_p_lifecycle=2&p_p_resource_id=getAlberaturaAntepost&p_p_cacheability=cacheLevelPage&_AlberaturaAntepost_WAR_scommesseportlet_filtro=0"
            # main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
            main_page_request = requests.get(url, headers=self.custom_headers)
            main_page_json = json.loads(main_page_request.content)

            for sport in main_page_json:
                if sport["disciplina"]["sigla"] == "CALCIO":
                    for league in sport["manifestazioneList"]:
                        all_leagues.append(league["codiceManifestazione"])
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()
            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://www.sisal.it/scommesse-matchpoint/palinsesto?p_p_id=ScommesseAntepostPalinsesto_WAR_scommesseportlet&p_p_lifecycle=2&p_p_state=normal&p_p_resource_id=dettagliManifestazione&p_p_cacheability=cacheLevelPage&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codDisc=1&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codMan={0}&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codClusterScomm=&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_filtro=0".format(league)
                    # league_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                    league_page_request = requests.get(url, headers=self.custom_headers)
                    league_page_json = json.loads(league_page_request.content)

                    if "scommessaList" not in league_page_json:
                        continue

                    url = "https://www.sisal.it/scommesse-matchpoint/palinsesto?p_p_id=ScommesseAntepostPalinsesto_WAR_scommesseportlet&p_p_lifecycle=2&p_p_resource_id=dettagliManifestazione&p_p_cacheability=cacheLevelPage&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codDisc=1&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codMan={0}&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codScomm=5546&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codClusterScomm=93&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_filtro=0".format(league)
                    # uo_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                    uo_page_request = requests.get(url, headers=self.custom_headers)
                    uo_page_json = json.loads(uo_page_request.content)

                    url = "https://www.sisal.it/scommesse-matchpoint/palinsesto?p_p_id=ScommesseAntepostPalinsesto_WAR_scommesseportlet&p_p_lifecycle=2&p_p_resource_id=dettagliManifestazione&p_p_cacheability=cacheLevelPage&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codDisc=1&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codMan={0}&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codScomm=603&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_codClusterScomm=93&_ScommesseAntepostPalinsesto_WAR_scommesseportlet_filtro=0".format(league)
                    # ggng_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                    ggng_page_request = requests.get(url, headers=self.custom_headers)
                    ggng_page_json = json.loads(ggng_page_request.content)

                    self.init_betfair_events()
                    for event in league_page_json["scommessaList"]:
                        if event["descrizioneScommessa"] != "ESITO FINALE 1X2":
                            continue
                        event_id = event["idBetradar"]
                        home, away = [team.strip() for team in event["descrizioneAvvenimento"].split(" - ")]
                        open_date = dateparser.parse(event["dataAvvenimento"]).strftime("%Y-%m-%d %H:%M:%S")
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()

                        for esito in event["infoAggList"][0]["esitoList"]:
                            if esito["descrizione"] == "1":
                                all_book_odds["home"] = float(esito["formattedQuota"])
                            elif esito["descrizione"] == "2":
                                all_book_odds["away"] = float(esito["formattedQuota"])
                            elif esito["descrizione"] == "X":
                                all_book_odds["draw"] = float(esito["formattedQuota"])

                        if "scommessaList" in uo_page_json:
                            for uo_event in uo_page_json["scommessaList"]:
                                if uo_event["idBetradar"] == event_id and uo_event["descrizioneScommessa"] == "UNDER/OVER":
                                    for uo_type in uo_event["infoAggList"]:
                                        if uo_type["descrizione"] == "U/O 0.5":
                                            for esito in uo_type["esitoList"]:
                                                if esito["descrizione"] == "UNDER":
                                                    all_book_odds["under_05"] = float(esito["formattedQuota"])
                                                elif esito["descrizione"] == "OVER":
                                                    all_book_odds["over_05"] = float(esito["formattedQuota"])
                                        elif uo_type["descrizione"] == "U/O 1.5":
                                            for esito in uo_type["esitoList"]:
                                                if esito["descrizione"] == "UNDER":
                                                    all_book_odds["under_15"] = float(esito["formattedQuota"])
                                                elif esito["descrizione"] == "OVER":
                                                    all_book_odds["over_15"] = float(esito["formattedQuota"])
                                        elif uo_type["descrizione"] == "U/O 2.5":
                                            for esito in uo_type["esitoList"]:
                                                if esito["descrizione"] == "UNDER":
                                                    all_book_odds["under_25"] = float(esito["formattedQuota"])
                                                elif esito["descrizione"] == "OVER":
                                                    all_book_odds["over_25"] = float(esito["formattedQuota"])
                                        elif uo_type["descrizione"] == "U/O 3.5":
                                            for esito in uo_type["esitoList"]:
                                                if esito["descrizione"] == "UNDER":
                                                    all_book_odds["under_35"] = float(esito["formattedQuota"])
                                                elif esito["descrizione"] == "OVER":
                                                    all_book_odds["over_35"] = float(esito["formattedQuota"])
                                        elif uo_type["descrizione"] == "U/O 4.5":
                                            for esito in uo_type["esitoList"]:
                                                if esito["descrizione"] == "UNDER":
                                                    all_book_odds["under_45"] = float(esito["formattedQuota"])
                                                elif esito["descrizione"] == "OVER":
                                                    all_book_odds["over_45"] = float(esito["formattedQuota"])

                        if "scommessaList" in ggng_page_json:
                            for ggng_event in ggng_page_json["scommessaList"]:
                                if ggng_event["idBetradar"] == event_id and ggng_event["descrizioneScommessa"] == "GOAL/NOGOAL":
                                    for esito in ggng_event["infoAggList"][0]["esitoList"]:
                                        if esito["descrizione"] == "GOAL":
                                            all_book_odds["goal"] = float(esito["formattedQuota"])
                                        elif esito["descrizione"] == "NOGOAL":
                                            all_book_odds["no_goal"] = float(esito["formattedQuota"])

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name + " no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Sisal(bot_data)
t.run()
