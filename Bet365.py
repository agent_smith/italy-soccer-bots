# -*- coding: utf-8 -*-
import cookielib
from cookielib import CookieJar
import copy
import sys
import os
import codecs
import mysql.connector
import json
import datetime
import pytz
import requests
from time import time, sleep
import re
import random
import urllib
import urllib2
from BeautifulSoup import *
import hashlib
import threading
import urlparse
import dateparser
from fractions import Fraction
import ssl
from xml.dom import minidom
from StringIO import StringIO
import gzip
import distance
reload(sys)
sys.setdefaultencoding('utf-8')

class Bet365(threading.Thread):
    def __init__(self,settings,lock):
        threading.Thread.__init__(self)
        self.db = mysql.connector.connect(user=settings['username'], password=settings['password'], database=settings['database'],host = "188.166.34.148")
        self.lock = lock
        cur = self.db.cursor()
        cur.execute("SET NAMES 'utf8'") 
        cur.execute("SET CHARACTER SET utf8") 
        cur.close()
        self._stop = threading.Event()
        self.all_odds = dict()
        self.matched_events = dict()
    
    def stop(self):
        self._stop.set()

    def loadmatchedevents(self):
        sql = "select betfair,bookie from matched_events where bookie_name like '{0}'".format(self.__class__.__name__.lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        if(len(rows)>0):
            for row in rows:
                if(row[1] not in self.matched_events):
                    self.matched_events[row[1]] = row[0]
                    
        return self.matched_events
        
    def setbookie(self):
        sql = "select id,name,full_name,website,coupon_url from providers where name like '{0}'".format(self.__class__.__name__.lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        self.bookie = dict()
        self.bookie["id"] = str(rows[0][0])
        self.bookie["name"] = str(rows[0][1])
        self.bookie["full_name"] = str(rows[0][2])
        self.bookie["website"] = str(rows[0][3])
        self.bookie["coupon_url"] = str(rows[0][4])
        
    def stopped(self):
        return self._stop.isSet()
    
    def getbetfairmatches(self):
        sql = "select o.id,name,home,away,open_date,home_odds,home_price,away_price,draw_price,o_25,o_25_price,u_25,u_25_price,goal,goal_price,no_goal,no_goal_price,country_code,competition,away_odds,draw_odds,market_id,market_id2,market_id3 from bf_match_odds as o natural join bf_events as e"
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        bf_data = []
        for row in rows:
            bf = dict()
            bf["id"] = str(row[0])
            bf["name"] = row[1]
            bf["home"] = row[2]
            bf["away"] = row[3]
            bf["open_date"] = str(row[4])
            bf["home_odds"] = str(row[5])
            bf["home_price"] = str(row[6])
            bf["away_price"] = str(row[7])
            bf["draw_price"] = str(row[8])
            bf["over_odds"] = str(row[9])
            bf["over_price"] = str(row[10])
            bf["under_odds"] = str(row[11])
            bf["under_price"] = str(row[12])
            bf["goal_odds"] = str(row[13])
            bf["goal_price"] = str(row[14])
            bf["no_goal_odds"] = str(row[15])
            bf["no_goal_price"] = str(row[16])
            bf["country_code"] = str(row[17])
            bf["competition"] = str(row[18])
            bf["away_odds"] = str(row[19])
            bf["draw_odds"] = str(row[20])
            bf["market_id"] = str(row[21])
            bf["market_id2"] = str(row[22])
            bf["market_id3"] = str(row[23])
            bf_data.append(bf)
        return bf_data
        
    def run(self):
        self.setbookie()
        self.loadmatchedevents()
        
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        soccer_leagues = ["1-1-13-32123102-2-12-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31949398-2-12-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-32028131-2-12-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-32024919-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-32025538-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29989258-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31831134-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30990260-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29848975-2-7-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-26278644-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29709177-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31108929-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30410052-2-3-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30280124-2-6-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30029332-2-6-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30197733-2-6-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29631468-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30381783-2-3-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30410052-2-3-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29709177-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29705494-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29705456-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31941470-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31941426-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29765451-2-1-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29966071-2-8-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31315808-2-8-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29819640-2-7-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29765035-2-15-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29765040-2-15-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31847526-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-29765266-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30059072-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-30059648-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31831134-2-17-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31822401-2-10-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0","1-1-13-31724509-2-12-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0"]
        self.cj = CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx),urllib2.HTTPCookieProcessor(self.cj))
        self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Host','mobile.bet365.it'),('Accept-Encoding','gzip, deflate, sdch, br'),('Connection', 'keep-alive')]            
        url = "https://mobile.bet365.it/V6/sport/splash/splash.aspx?zone=3&isocode=KE&tzi=4&key=1&gn=0&cid=1&lng=1&ct=97&iscode=EUR&ot=2"
        req = urllib2.Request(url)
        resp = self.opener.open(req)
        if resp.info().get('Content-Encoding') == 'gzip':
            buf = StringIO(resp.read())
            resp = gzip.GzipFile(fileobj=buf)
        resp = resp.read().replace("\n","").replace("\r","").replace("\t","").replace("&nbsp;"," ")    
        soup = BeautifulSoup(resp,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
        all_countries = [h["data-sportskey"] for h in soup.find("div",{"class":"eventWrapper"}).findAll("h3") if h.has_key("data-sportskey")]
        blacklist=[]
        soccer_leagues = []
        for c in all_countries:
            url = "https://mobile.bet365.it/V6/Sport/Splash/SplashPartial.aspx?key={0}&scfv=97&scfm=2&iscode=EUR&isocode=KE&lng=6&zone=3&sst=0&apptype=&appversion=&lvl=1".format(c)
            req = urllib2.Request(url)
            resp = self.opener.open(req)
            if resp.info().get('Content-Encoding') == 'gzip':
                buf = StringIO(resp.read())
                resp = gzip.GzipFile(fileobj=buf)
            resp = resp.read().replace("\n","").replace("\r","").replace("\t","").replace("&nbsp;"," ")    
            soup = BeautifulSoup(resp,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
            soccer_leagues.extend([l["data-sportskey"] for l in soup.findAll("div",{"data-nav":"couponLink"}) if l.has_key("data-sportskey") and l["data-sportskey"] not in soccer_leagues])
        
        while self._stop.isSet() == False:
            SQL = ""
            try:
                self.cj = CookieJar()
                self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=ctx),urllib2.HTTPCookieProcessor(self.cj))
                self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Host','mobile.bet365.it'),('Accept-Encoding','gzip, deflate, sdch, br'),('Connection', 'keep-alive')]            
                url = "https://mobile.bet365.it"
                req = urllib2.Request(url)
                resp = self.opener.open(req)
                if resp.info().get('Content-Encoding') == 'gzip':
                    buf = StringIO(resp.read())
                    resp = gzip.GzipFile(fileobj=buf)
                resp = resp.read().replace("\n","").replace("\r","").replace("\t","").replace("&nbsp;"," ")
                all_events = []
                for l in soccer_leagues:
                    try:
                        if(self._stop.isSet()):
                            return
                        all_league_data = []
                        sleep(random.randint(0,10))
                        url = "https://mobile.bet365.it/V6/sport/coupon/coupon.aspx?zone=3&isocode=KE&tzi=4&key={0}&ip=0&gn=0&cid=1&lng=6&ct=97&clt=9996&ot=2&pk=1".format(l)
                        req = urllib2.Request(url)
                        resp = self.opener.open(req)
                        if resp.info().get('Content-Encoding') == 'gzip':
                            buf = StringIO(resp.read())
                            resp = gzip.GzipFile(fileobj=buf)
                        resp = resp.read().replace("\n","").replace("\r","").replace("\t","").replace("&nbsp;"," ")
                        betfair_data = self.getbetfairmatches()
                        soup = BeautifulSoup(resp,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
                        for row in soup.findAll("div",{"class":"podHeaderRow"}):
                            dt = row.find("div").text.strip()[3:].strip().replace("set","sep")
                            dt = dateparser.parse(dt).strftime("%Y-%m-%d")
                            
                            div = row.findNextSibling()
                            while div is not None and div.has_key("data-fixtureid"):
                                divs = div.findAll("span",{"class":"ippg-Market_Truncator"})
                                home = self.db.converter.escape(divs[0].text.strip())
                                away = self.db.converter.escape(divs[1].text.strip())
                                name = home +" - "+away
                                tm = div.find("div",{"class":"ippg-Market_GameStartTime"}).text.strip().replace(".",":")
                                divs = div.findAll("span",{"class":"ippg-Market_Odds"})
                                id = hashlib.md5(dt+tm+name).hexdigest()
                                home_odds = ""
                                away_odds = ""
                                draw_odds = ""
                                
                                if(len(divs)>2):
                                    open_date = (dateparser.parse(dt+" "+tm+":00") - datetime.timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')
                                    home_odds = divs[0].text.strip()
                                    draw_odds = divs[1].text.strip()
                                    away_odds = divs[2].text.strip()                            
                                    all_events.append(id)
                                    
                                        
                                    if(home_odds != "" and away_odds != "" and draw_odds != ""):
                                        pk = div["data-nav"].split(",")
                                        if(len(pk)>2):
                                            pk = pk[2]                             
                                        else:
                                            pk=""
                                       
                                        #if(id in self.all_odds and self.all_odds[id]["home_odds"] == home_odds and self.all_odds[id]["draw_odds"] == draw_odds and self.all_odds[id]["away_odds"] == away_odds):
                                        #    continue
                                        self.all_odds[id] = dict()
                                        self.all_odds[id]["home_odds"] = home_odds
                                        self.all_odds[id]["draw_odds"] = draw_odds
                                        self.all_odds[id]["away_odds"] = away_odds
                                        changed = False
                                        if home.upper()+" v "+away.upper() in self.matched_events:
                                            old_event = home.upper()+" v "+away.upper()
                                            home = self.matched_events[old_event].split(" v ")[0].strip()
                                            away = self.matched_events[old_event].split(" v ")[1].strip()
                                            changed = True
                                        bf = None
                                        for bf_row in betfair_data:
                                            if(open_date != bf_row["open_date"]):
                                                continue
                                            if(home.lower() == bf_row["home"].lower() and away.lower() == bf_row["away"].lower()):# or home.lower().startswith(bf_row["home"].lower()) and away.lower().startswith(bf_row["away"].lower())  or bf_row["home"].lower().startswith(home.lower()) and bf_row["away"].lower().startswith(away.lower())):
                                                bf = bf_row
                                                #print name,open_date,bf["name"],bf["open_date"],home_odds,bf["home_odds"]
                                                break
                                        if(False and bf==None and changed == False and id not in blacklist):
                                            try:
                                                url = "https://www.betfair.com/exchange/search?&modules=instantsearch-ebs&isAjax=True&container=false&query="+urllib.quote_plus(name.encode('utf8'))+"&alt=json"
                                                req = urllib2.Request(url)
                                                resp = self.opener.open(req)
                                                if resp.info().get('Content-Encoding') == 'gzip':
                                                    buf = StringIO(resp.read())
                                                    resp = gzip.GzipFile(fileobj=buf)
                                                resp = resp.read()
                                                soup = BeautifulSoup(resp)
                                                spans = soup.find("span")
                                                
                                                if(spans == None):
                                                    blacklist.append(id)
                                                    div = div.findNextSibling()
                                                    continue
                                                spans = spans.findAll("span")
                                                
                                                if(spans != None and len(spans)>1):
                                                    evnt = spans[0].text.replace(" v "," - ")
                                                    if(len(evnt.split(" - "))>1):
                                                        h = evnt.split(" - ")[0].strip()
                                                        a = evnt.split(" - ")[1].strip()
                                                        d = spans[1].text.split('|')
                                                        d = d[len(d)-1].strip()+":00"
                                                        
                                                        SQL1=""
                                                        if(open_date == dateparser.parse(d).strftime("%Y-%m-%d %H:%M:%S") and distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())<13):#or  abs((dateparser.parse(str(open_date)) - dateparser.parse(d)).total_seconds())<900 and distance.levenshtein(evnt.upper(),name.upper())<13):
                                                            for bf_row in betfair_data:
                                                                if(h.lower() == bf_row["home"].lower() and a.lower() == bf_row["away"].lower()):
                                                                    bf = bf_row
                                                                    self.matched_events[home.upper()+" v "+away.upper()] = h.upper()+" v "+a.upper()
                                                                    id1 = hashlib.md5(h+a+self.__class__.__name__.upper()).hexdigest()
                                                                    SQL1 = "insert ignore into matched_events (id,betfair,bookie,bookie_name) values ('{0}','{1}','{2}','{3}'); ".format(id1,h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper(),self.__class__.__name__.upper())
                                                                    cur = self.db.cursor() 
                                                                    for result in cur.execute(SQL1,multi = True):
                                                                        pass
                                                                    self.db.commit()            
                                                                    cur.close()
                                                                    break
                                                        elif(open_date == dateparser.parse(d).strftime("%Y-%m-%d %H:%M:%S") and distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())<25 and (h.lower()== home.lower() or a.lower() == away.lower() or (home.lower() in h.lower() or away.lower() in a.lower()))):
                                                            for bf_row in betfair_data:
                                                                if(h.lower() == bf_row["home"].lower() and a.lower() == bf_row["away"].lower()):
                                                                    bf = bf_row
                                                                    bf_stake =  (float(home_odds)*100)/(float(bf["home_odds"])-0.05)
                                                                    profit = ((bf_stake*(1-0.05))-100)
                                                                    rating = round(100+profit,2)
                                                                    if(rating<100):
                                                                        print home+" v "+away,h+" v "+a,distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper()),distance.levenshtein(h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper())
                                                                        self.matched_events[home.upper()+" v "+away.upper()] = h.upper()+" v "+a.upper()
                                                                        id1 = hashlib.md5(h+a+self.__class__.__name__.upper()).hexdigest()
                                                                        SQL1 = "insert ignore into matched_events (id,betfair,bookie,bookie_name) values ('{0}','{1}','{2}','{3}'); ".format(id1,h.upper()+" v "+a.upper(),home.upper()+" v "+away.upper(),self.__class__.__name__.upper())
                                                                        cur = self.db.cursor() 
                                                                        for result in cur.execute(SQL1,multi = True):
                                                                            pass
                                                                        self.db.commit()            
                                                                        cur.close()
                                                                    break
                                                            
                                            except Exception,msg:
                                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                                print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                                                if(True or "MySQL Connection not available" in msg):
                                                    sleep(20)
                                                    self.db = mysql.connector.connect(user=settings['username'], password=settings['password'], database=settings['database'],host = "188.166.34.148")
                                            if bf == None:
                                                blacklist.append(id)
                                                div = div.findNextSibling()
                                                continue   
                                           
                                        if(bf==None):
                                            div = div.findNextSibling()
                                            continue
                                        if(bf["home_odds"]!="None" and bf["home_odds"]!=""):
                                            bf_stake =  (float(home_odds)*100)/(float(bf["home_odds"])-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(home_odds)
                                            layOdds = float(bf["home_odds"])
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = self.__class__.__name__.lower()+"_"+id+"home"+bf["id"]
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],bf["home"],"1x2",rating,self.bookie["id"],self.bookie["full_name"],home_odds,bf["home_odds"],bf["home_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],"betfair",snr,rating,home_odds,bf["home_odds"],bf["home_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])
                                            
                                        if(bf["away_odds"]!="None") and bf["away_odds"]!="":
                                            bf_stake =  (float(away_odds)*100)/(float(bf["away_odds"])-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)
                                            
                                            backOdds = float(away_odds)
                                            layOdds = float(bf["away_odds"])
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit
                                            snr = round((((float)(snr*100))/refund),2)
                                            key = self.__class__.__name__.lower()+"_"+id+"away"+bf["id"]
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],bf["away"],"1x2",rating,self.bookie["id"],self.bookie["full_name"],away_odds,bf["away_odds"],bf["away_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],"betfair",snr,rating,away_odds,bf["away_odds"],bf["away_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])
                                            
                                        if(bf["draw_odds"]!="None" and bf["draw_odds"]!=""):
                                            bf_stake =  (float(draw_odds)*100)/(float(bf["draw_odds"])-0.05)
                                            profit = ((bf_stake*(1-0.05))-100)
                                            rating = round(100+profit,2)

                                            backOdds = float(draw_odds)
                                            layOdds = float(bf["draw_odds"])
                                            backCommission = 0
                                            layCommission = 0.05
                                            backStake = 100
                                            layStake =  backStake*backOdds/(layOdds-layCommission)
                                            refund = 25

                                            layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                            netresultlaybackwin  = refund-backStake
                                            netresultbackbackwin = ((backStake * backOdds) - backStake)
                                            liability = layStake*layOdds
                                            netresultbacklaywin =  (-1 * liability)
                                            netresultlaylaywin = (layStake * (1-layCommission))
                                            laywinprofit = netresultlaybackwin + netresultlaylaywin
                                            backwinprofit = netresultbackbackwin + netresultbacklaywin
                                            snr = laywinprofit
                                            if(snr>backwinprofit):
                                                snr = backwinprofit

                                            snr = round((((float)(snr*100))/refund),2)
                                            key = self.__class__.__name__.lower()+"_"+id+"draw"+bf["id"]
                                            SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange,bf_id) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}',{24}) on duplicate key update update_time=NOW(),snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"Pareggio","1x2",rating,self.bookie["id"],self.bookie["full_name"],draw_odds,bf["draw_odds"],bf["draw_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["competition"],bf["country_code"],"betfair",snr,rating,draw_odds,bf["draw_odds"],bf["draw_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id"],bf["id"])
                                        if(pk != "" and bf!=None and bf["goal_odds"]!="None" or bf["no_goal_odds"]!="None" or bf["over_odds"]!="None" or bf["under_odds"]!="None"):
                                            url = "https://mobile.bet365.it/V6/sport/coupon/coupon.aspx?zone=3&isocode=KE&tzi=4&key={1}&ip=0&gn=0&cid=1&lng=6&ct=97&clt=9996&ot=2&pk={0}".format(l,pk)
                                            req = urllib2.Request(url)
                                            resp = self.opener.open(req)
                                            if resp.info().get('Content-Encoding') == 'gzip':
                                                buf = StringIO(resp.read())
                                                resp = gzip.GzipFile(fileobj=buf)
                                            resp = resp.read().replace("\n","").replace("\r","").replace("\t","").replace("&nbsp;"," ")
                                            soup2 = BeautifulSoup(resp,convertEntities=BeautifulStoneSoup.HTML_ENTITIES)
                                            for row2 in soup2.findAll("div",{"class":re.compile("enhancedPod matchBetting.*")}):
                                                if("Goal: under/over" in row2.find("h1").text):
                                                    for row22 in row2.findAll("div",{"class":re.compile("podEventRow.*")}):
                                                        if(row22.find("em").text.strip() == "2.5"):
                                                            odds = row22.findAll("div",{"class":re.compile("priceColumn.*")})
                                                            over_odds = odds[0].text.strip()
                                                            under_odds = odds[1].text.strip()
                                                            #print name,"OVER/UNDER",under_odds,over_odds
                                                            if(over_odds != "" and bf["over_odds"]!="None"):
                                                                bf_stake =  (float(over_odds)*100)/(float(bf["over_odds"])-0.05)
                                                                profit = ((bf_stake*(1-0.05))-100)
                                                                rating = round(100+profit,2)

                                                                backOdds = float(over_odds)
                                                                layOdds = float(bf["over_odds"])
                                                                backCommission = 0
                                                                layCommission = 0.05
                                                                backStake = 100
                                                                layStake =  backStake*backOdds/(layOdds-layCommission)
                                                                refund = 25

                                                                layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                                                netresultlaybackwin  = refund-backStake
                                                                netresultbackbackwin = ((backStake * backOdds) - backStake)
                                                                liability = layStake*layOdds
                                                                netresultbacklaywin =  (-1 * liability)
                                                                netresultlaylaywin = (layStake * (1-layCommission))
                                                                laywinprofit = netresultlaybackwin + netresultlaylaywin
                                                                backwinprofit = netresultbackbackwin + netresultbacklaywin
                                                                snr = laywinprofit
                                                                if(snr>backwinprofit):
                                                                    snr = backwinprofit

                                                                snr = round((((float)(snr*100))/refund),2)
                                                                if(snr>1000):
                                                                    continue
                                                                key = self.__class__.__name__.lower()+"_"+id+"_over_"+bf["id"]
                                                                SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"OVER 2.5","OVER/UNDER",rating,self.bookie["id"],self.bookie["full_name"],over_odds,bf["over_odds"],bf["over_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id2"],bf["competition"],bf["country_code"],"betfair",snr,rating,over_odds,bf["over_odds"],bf["over_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id2"])
                                                            
                                                            if(under_odds != "" and bf["under_odds"]!="None"):
                                                                bf_stake =  (float(under_odds)*100)/(float(bf["under_odds"])-0.05)
                                                                profit = ((bf_stake*(1-0.05))-100)
                                                                rating = round(100+profit,2)

                                                                backOdds = float(under_odds)
                                                                layOdds = float(bf["under_odds"])
                                                                backCommission = 0
                                                                layCommission = 0.05
                                                                backStake = 100
                                                                layStake =  backStake*backOdds/(layOdds-layCommission)
                                                                refund = 25

                                                                layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                                                netresultlaybackwin  = refund-backStake
                                                                netresultbackbackwin = ((backStake * backOdds) - backStake)
                                                                liability = layStake*layOdds
                                                                netresultbacklaywin =  (-1 * liability)
                                                                netresultlaylaywin = (layStake * (1-layCommission))
                                                                laywinprofit = netresultlaybackwin + netresultlaylaywin
                                                                backwinprofit = netresultbackbackwin + netresultbacklaywin
                                                                snr = laywinprofit
                                                                if(snr>backwinprofit):
                                                                    snr = backwinprofit

                                                                snr = round((((float)(snr*100))/refund),2)
                                                                if(snr>1000):
                                                                    continue
                                                                key = self.__class__.__name__.lower()+"_"+id+"_under_"+bf["id"]
                                                                SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"UNDER 2.5","OVER/UNDER",rating,self.bookie["id"],self.bookie["full_name"],under_odds,bf["under_odds"],bf["under_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id2"],bf["competition"],bf["country_code"],"betfair",snr,rating,under_odds,bf["under_odds"],bf["under_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id2"])
                                                            break
                                                elif("Entrambe le squadre segnano" in row2.find("h1").text):
                                                    odds = row2.find("div",{"class":re.compile("podEventRow")})
                                                    odds = odds.findAll("span",{"class":"odds"})
                                                    goal_odds = odds[0].text.strip()
                                                    no_goal_odds = odds[1].text.strip()
                                                    #print name,"GOAL/NOGOAL",goal_odds,no_goal_odds
                                                    if(goal_odds!= "" and bf["goal_odds"]!="None"):
                                                        bf_stake =  (float(goal_odds)*100)/(float(bf["goal_odds"])-0.05)
                                                        profit = ((bf_stake*(1-0.05))-100)
                                                        rating = round(100+profit,2)

                                                        backOdds = float(goal_odds)
                                                        layOdds = float(bf["goal_odds"])
                                                        backCommission = 0
                                                        layCommission = 0.05
                                                        backStake = 100
                                                        layStake =  backStake*backOdds/(layOdds-layCommission)
                                                        refund = 25

                                                        layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                                        netresultlaybackwin  = refund-backStake
                                                        netresultbackbackwin = ((backStake * backOdds) - backStake)
                                                        liability = layStake*layOdds
                                                        netresultbacklaywin =  (-1 * liability)
                                                        netresultlaylaywin = (layStake * (1-layCommission))
                                                        laywinprofit = netresultlaybackwin + netresultlaylaywin
                                                        backwinprofit = netresultbackbackwin + netresultbacklaywin
                                                        snr = laywinprofit
                                                        if(snr>backwinprofit):
                                                            snr = backwinprofit

                                                        snr = round((((float)(snr*100))/refund),2)
                                                        if(snr>1000):
                                                            continue
                                                        key = self.__class__.__name__.lower()+"_"+id+"_goal_"+bf["id"]
                                                        SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"GOAL","GOAL/NOGOAL",rating,self.bookie["id"],self.bookie["full_name"],goal_odds,bf["goal_odds"],bf["goal_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id3"],bf["competition"],bf["country_code"],"betfair",snr,rating,goal_odds,bf["goal_odds"],bf["goal_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id3"])
                                                        
                                                    if(no_goal_odds != "" and bf["no_goal_odds"]!="None"):
                                                        bf_stake =  (float(no_goal_odds)*100)/(float(bf["no_goal_odds"])-0.05)
                                                        profit = ((bf_stake*(1-0.05))-100)
                                                        rating = round(100+profit,2)
                                                        
                                                        backOdds = float(no_goal_odds)
                                                        layOdds = float(bf["no_goal_odds"])
                                                        backCommission = 0
                                                        layCommission = 0.05
                                                        backStake = 100
                                                        layStake =  backStake*backOdds/(layOdds-layCommission)
                                                        refund = 25

                                                        layStake = ((backStake*backOdds)-refund)/(layOdds-layCommission)
                                                        netresultlaybackwin  = refund-backStake
                                                        netresultbackbackwin = ((backStake * backOdds) - backStake)
                                                        liability = layStake*layOdds
                                                        netresultbacklaywin =  (-1 * liability)
                                                        netresultlaylaywin = (layStake * (1-layCommission))
                                                        laywinprofit = netresultlaybackwin + netresultlaylaywin
                                                        backwinprofit = netresultbackbackwin + netresultbacklaywin
                                                        snr = laywinprofit
                                                        if(snr>backwinprofit):
                                                            snr = backwinprofit
                                                        snr = round((((float)(snr*100))/refund),2)
                                                        if(snr>1000):
                                                            continue
                                                        key = self.__class__.__name__.lower()+"_"+id+"_no_goal_"+bf["id"]
                                                        SQL += "insert into compare_table (id,event_name,bet,market_type,rating,odds_provider_fk,odds_provider,back_odds,lay_odds,availability,opendate,snr_rating,bookie_bet_url,betfair_bet_url,competition,country_code,exchange) values ('{0}','{1}','{2}','{3}',{4},{5},'{6}',{7},{8},{9},DATE_SUB('{10}',INTERVAL 1 HOUR),{11},'{12}','{13}','{14}','{15}','{16}') on duplicate key update snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key,bf["name"],"NO GOAL","GOAL/NOGOAL",rating,self.bookie["id"],self.bookie["full_name"],no_goal_odds,bf["no_goal_odds"],bf["no_goal_price"],bf["open_date"],snr,self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id3"],bf["competition"],bf["country_code"],"betfair",snr,rating,no_goal_odds,bf["no_goal_odds"],bf["no_goal_price"],self.bookie["coupon_url"],"https://www.betfair.it/exchange/football/market?id="+bf["market_id3"])
                                                      
                                            
                                        try:
                                            if SQL != "" and len(SQL.split(';'))>5:
                                                cur = self.db.cursor() 
                                                for result in cur.execute(SQL,multi = True):
                                                    pass
                                                self.db.commit()            
                                                cur.close()
                                                SQL = ""
                                        except Exception,msg:
                                            exc_type, exc_obj, exc_tb = sys.exc_info()
                                            print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                                            SQL = ""
                                    
                                #print name,dt,tm
                                div = div.findNextSibling()
                        
                        
                        if SQL != "":
                            cur = self.db.cursor() 
                            for result in cur.execute(SQL,multi = True):
                                pass
                            self.db.commit()            
                            cur.close()
                            SQL = ""
                        
                    except Exception,msg:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                        if(True or "MySQL Connection not available" in msg):
                            sleep(20)
                            self.db = mysql.connector.connect(user=settings['username'], password=settings['password'], database=settings['database'],host = "188.166.34.148")
            except Exception,msg:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                print(self.__class__.__name__.lower(),msg, exc_tb.tb_lineno)
                if(True or "MySQL Connection not available" in msg):
                    sleep(20)
                    self.db = mysql.connector.connect(user=settings['username'], password=settings['password'], database=settings['database'],host = "188.166.34.148")
            
            sleep(random.randint(0,10))
   
settings = {
    'username': '83fdd02e7115cd2',
    'password': '4e59d10211ad4d477b3cc1c1f7a67e28343185330',
    'database': 'ninjabet'
}
lock = threading.Lock()
b = Bet365(settings,lock)
b.run()