import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Pinterbet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
            "X-Requested-With":"XMLHttpRequest",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.betaland.it/Sport/Groups.aspx?IDSport=19&Antepost=0"
            main_page_request = requests.get(url, headers=self.custom_headers)
            main_page_soup = BeautifulSoup(main_page_request.content, "lxml")

            football_section = main_page_soup.find("div", {"class":"divNSport", "title":"CALCIO"})
            if football_section:
                all_leagues = [re.search("EventID=(\d+)", a["href"]).group(1) for a in football_section.parent.parent.parent.find_all("a", {"id":"lE"})]

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://www.betaland.it/Controls/OddsEvent.aspx?ShowLinkFastBet=0&showDate=1&showGQ=1&rnd=0{0}&EventID={1}&GroupSep=undefined".format(random.randint(10 ** (16 - 1), (10 ** 16) - 1), league)
                    league_page_request = requests.get(url, headers=self.custom_headers)
                    league_page_soup = BeautifulSoup(league_page_request.content, "lxml")

                    self.init_betfair_events()
                    for event in league_page_soup.find_all("tr", {"class":re.compile("dgItem|dgAItem")}):
                        name_section = event.find("td", {"class":"nmInc"})
                        event_id = name_section["idse"]
                        date = event.parent.find("td", {"class":"cqDateTbl"}).text.strip()
                        hour = event.find("td", {"class":"dtInc"}).text.strip()
                        open_date = dateparser.parse(date + " " + hour).strftime('%Y-%m-%d %H:%M:%S')
                        name = name_section.text.strip()
                        home, away = [team.strip() for team in name.split(" - ")]
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()

                        all_odds = event.find_all("div", {"class":"oddsQ"})
                        try:
                            all_book_odds["home"] = float(all_odds[0].text.strip().replace(",", "."))
                        except ValueError:
                            pass
                        try:
                            all_book_odds["away"] = float(all_odds[2].text.strip().replace(",", "."))
                        except ValueError:
                            pass
                        try:
                            all_book_odds["draw"] = float(all_odds[1].text.strip().replace(",", "."))
                        except ValueError:
                            pass

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        if len(all_odds) > 6:
                            try:
                                all_book_odds["under_25"] = float(all_odds[6].text.strip().replace(",", "."))
                            except ValueError:
                                pass
                            try:
                                all_book_odds["over_25"] = float(all_odds[7].text.strip().replace(",", "."))
                            except ValueError:
                                pass
                            try:
                                all_book_odds["goal"] = float(all_odds[8].text.strip().replace(",", "."))
                            except ValueError:
                                pass
                            try:
                                all_book_odds["no_goal"] = float(all_odds[9].text.strip().replace(",", "."))
                            except ValueError:
                                pass

                            if bf_event["under_05"] != "None" or bf_event["over_05"] != "None" or bf_event["under_35"] != "None" or bf_event["over_35"] != "None" or bf_event["under_45"] != "None" or bf_event["over_45"] != "None":
                                url = "https://www.betaland.it/Content/SubEventDetails.aspx?SubEventID={0}".format(event_id)
                                event_page_request = requests.get(url, headers=self.custom_headers)
                                event_page_soup = BeautifulSoup(event_page_request.content, "lxml")

                                under05_section = event_page_soup.find("div", text=re.compile("^\s*U 0\.5\s*$"))
                                if under05_section:
                                    try:
                                        all_book_odds["under_05"] = float(under05_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                over05_section = event_page_soup.find("div", text=re.compile("^\s*O 0\.5\s*$"))
                                if over05_section:
                                    try:
                                        all_book_odds["over_05"] = float(over05_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                under15_section = event_page_soup.find("div", text=re.compile("^\s*U 1\.5\s*$"))
                                if under15_section:
                                    try:
                                        all_book_odds["under_15"] = float(under15_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                over15_section = event_page_soup.find("div", text=re.compile("^\s*O 1\.5\s*$"))
                                if over15_section:
                                    try:
                                        all_book_odds["over_15"] = float(over15_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                under35_section = event_page_soup.find("div", text=re.compile("^\s*U 3\.5\s*$"))
                                if under35_section:
                                    try:
                                        all_book_odds["under_35"] = float(under35_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                over35_section = event_page_soup.find("div", text=re.compile("^\s*O 3\.5\s*$"))
                                if over35_section:
                                    try:
                                        all_book_odds["over_35"] = float(over35_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                under45_section = event_page_soup.find("div", text=re.compile("^\s*U 4\.5\s*$"))
                                if under45_section:
                                    try:
                                        all_book_odds["under_45"] = float(under45_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass
                                over45_section = event_page_soup.find("div", text=re.compile("^\s*O 4\.5\s*$"))
                                if over45_section:
                                    try:
                                        all_book_odds["over_45"] = float(over45_section.parent.a.text.strip().replace(",", "."))
                                    except ValueError:
                                        pass

                        if not self.check_rating(all_book_odds, bf_event):
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Pinterbet(bot_data)
t.run()
