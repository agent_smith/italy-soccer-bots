# -*- coding: utf-8 -*-

import datetime
import json
import re
import sys
import traceback
from contextlib import closing
from time import sleep

import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup
from pathos import multiprocessing

from common_multi import Common

reload(sys)
sys.setdefaultencoding("UTF-8")

class Bwin(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "Host":"sports.bwin.it",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def process_page(self, page):
        import datetime
        import hashlib
        import json
        import re
        import sys
        import traceback
        from fractions import Fraction

        import dateparser
        import requests
        requests.packages.urllib3.disable_warnings()
        from bs4 import BeautifulSoup

        try:
            db = self.get_db()

            url = "https://sports.bwin.it/it/sports/indexmultileague"
            post_data = {"sportId":"4", "page":str(page)}
            page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            page_soup = BeautifulSoup(page_request.content, 'lxml')

            events = page_soup.find_all("div", {"class":"marketboard-event-group__item--event"})
            if not events:
                return

            betfair_events = self.get_betfair_events(db)
            for market in page_soup.find("div", {"id":"markets"}).find_all("div", {"class":"ui-widget-content"}):
                date_string = market.find("span").string
                if not date_string or len(date_string.split(" - ")) < 2:
                    continue
                date_string = date_string.split(' - ')[1].split("/")
                date_string = "{0}-{1}-{2}".format(date_string[2].strip(), date_string[1].strip(), date_string[0].strip())

                events = market.find_all("div", {"class":"marketboard-event-group__item--event"})
                for event in events:
                    time_string = event.find("div", {"class":"marketboard-event-without-header__market-time"}).string.strip() + ":00"
                    tr = event.find("tr")
                    tds = tr.find_all("td")

                    event_id_section = event.find("td", {"data-option":re.compile("\d+/\d+/\d+/.*")})
                    event_id = re.search("\d+/\d+/(\d+)/.*", event_id_section["data-option"]).group(1)
                    home = tds[0].find("button").find_all("div")[0].string
                    away = tds[2].find("button").find_all("div")[0].string
                    name = home + " - " + away
                    open_date = datetime.datetime.strptime(date_string + " " + time_string, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                    event_hash = hashlib.md5(name + event_id).hexdigest()

                    all_book_odds = self.get_empty_odds_dict()
                    try:
                        all_book_odds["home"] = float(tds[0].find("button").find_all("div")[1].string.strip())
                    except ValueError:
                        all_book_odds["home"] = float(Fraction(tds[0].find("button").find_all("div")[1].string.strip()) + 1.00)
                    try:
                        all_book_odds["away"] = float(tds[2].find("button").find_all("div")[1].string.strip())
                    except ValueError:
                        all_book_odds["away"] = float(Fraction(tds[2].find("button").find_all("div")[1].string.strip()) + 1.00)
                    try:
                        all_book_odds["draw"] = float(tds[1].find("button").find_all("div")[1].string.strip())
                    except ValueError:
                        all_book_odds["draw"] = float(Fraction(tds[1].find("button").find_all("div")[1].string.strip()) + 1.00)

                    bf_event = self.pair_match(db, betfair_events, all_book_odds, event_hash, home, away, open_date)
                    if not bf_event:
                        print name, open_date, "no bf_event, skipping"
                        if event_hash not in self.bot_data["blacklist"]:
                            self.bot_data["blacklist"].append(event_hash)
                        continue

                    event_url = event.find("a")["href"]
                    if "betradar" in event_url:
                        event_url = None
                    else:
                        event_url = "https://sports.bwin.it" + event_url.replace("/scommesse", "/-1/scommesse")

                    if event_url and (bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None"):
                        try:
                            event_page_request = requests.get(event_url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
                            event_page_soup = BeautifulSoup(event_page_request.content, 'lxml')

                            for market in event_page_soup.find_all("div", {"class":"event-view-details__subgroup-content"}):
                                if re.search("[^+][^ ]Entrambe le squadre segneranno durante la partita?", market.text):
                                    odds = market.find_all("div", {"class":re.compile("mb-option-button__option-odds.*")})
                                    try:
                                        all_book_odds["goal"] = float(odds[0].text.strip())
                                    except ValueError:
                                        all_book_odds["goal"] = float(Fraction(odds[0].text.strip()) + 1.00)
                                    try:
                                        all_book_odds["no_goal"] = float(odds[1].text.strip())
                                    except ValueError:
                                        all_book_odds["no_goal"] = float(Fraction(odds[1].text.strip())) + 1.00
                                elif "Quanti gol saranno segnati?" in market.text:
                                    for odds in market.find_all("div", {"class":re.compile("mb-option-button__option-odds.*")}):
                                        if "Più di 0,5" in odds.parent.text:
                                            try:
                                                all_book_odds["over_05"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["over_05"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Meno di 0,5" in odds.parent.text:
                                            try:
                                                all_book_odds["under_05"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["under_05"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Più di 1,5" in odds.parent.text:
                                            try:
                                                all_book_odds["over_15"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["over_15"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Meno di 1,5" in odds.parent.text:
                                            try:
                                                all_book_odds["under_15"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["under_15"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Più di 2,5" in odds.parent.text:
                                            try:
                                                all_book_odds["over_25"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["over_25"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Meno di 2,5" in odds.parent.text:
                                            try:
                                                all_book_odds["under_25"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["under_25"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Più di 3,5" in odds.parent.text:
                                            try:
                                                all_book_odds["over_35"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["over_35"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Meno di 3,5" in odds.parent.text:
                                            try:
                                                all_book_odds["under_35"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["under_35"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Più di 4,5" in odds.parent.text:
                                            try:
                                                all_book_odds["over_45"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["over_45"] = float(Fraction(odds.text.strip()) + 1.00)
                                        elif "Meno di 4,5" in odds.parent.text:
                                            try:
                                                all_book_odds["under_45"] = float(odds.text.strip())
                                            except ValueError:
                                                all_book_odds["under_45"] = float(Fraction(odds.text.strip()) + 1.00)

                        except Exception:
                            exc_type, exc_obj, exc_tb = sys.exc_info()
                            traceback.print_exception(exc_type, exc_obj, exc_tb)

                    if not self.check_rating(all_book_odds, bf_event):
                        if event_hash not in self.bot_data["blacklist"]:
                            self.bot_data["blacklist"].append(event_hash)
                        continue

                    print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                    self.save_data(db, event_hash, all_book_odds, bf_event, name, open_date)

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)

        finally:
            if db:
                db.close()
            sys.stdout.flush()
            sys.stderr.flush()

        return {
            "blacklist":self.bot_data["blacklist"],
            "matched_betfair_events":self.matched_betfair_events,
            "matched_events":self.matched_events
        }

    def run(self):
        while True:
            print("round round round round round round round round")
            try:
                url = "https://sports.bwin.it/it/sports/indexmultileague"
                post_data = {"sportId":"4", "page":"0"}
                main_page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
                main_page_soup = BeautifulSoup(main_page_request.content, 'lxml')

                page_count_section = main_page_soup.find("span", {"class":"markets-pager__pages-count"})
                if page_count_section:
                    page_count_search = re.search("risultati \| (\d+)", page_count_section.text)
                    if page_count_search:
                        page_count = int(page_count_search.group(1))
                        with closing(multiprocessing.Pool(processes=page_count + 1)) as p:
                            results = p.map(self.process_page, range(page_count))
                            for result in results:
                                if result:
                                    self.bot_data["blacklist"].extend([event for event in result["blacklist"] if event not in self.bot_data["blacklist"]])
                                    self.matched_betfair_events.update(result["matched_betfair_events"])
                                    self.matched_events.update(result["matched_events"])
                            p.terminate()

            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                traceback.print_exception(exc_type, exc_obj, exc_tb)

            sleep(5)

if __name__ == '__main__':
    bot_data = {
        "blacklist": [],
        "region":"it",
    }

    t = Bwin(bot_data)
    t.run()
