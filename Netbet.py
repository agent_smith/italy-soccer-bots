import datetime
import json
import re
import sys
import traceback
from contextlib import closing
from time import sleep

import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup
from pathos import multiprocessing

from common_multi import Common

reload(sys)
sys.setdefaultencoding("UTF-8")

class Netbet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "it,en-US;q=0.7,en;q=0.3",
            "Connection": "keep-alive",
            "Content-Type": "application/x-www-form-urlencoded",
            "RequestTarget": "AJAXService",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://scommesse.netbet.it/pagemethods.aspx/GetBranchesWithLeagues?"
            main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            main_page_json = json.loads(main_page_request.content.replace(",,", ",0,").replace(",,", ",0,").replace("[,", "[").replace(",]", "]"))

            for league in main_page_json[1]:
                if league[8] == 1:
                    all_leagues.append(league[0])

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def round_odd(self, odd):
        import math

        if abs(odd) < 600:
            rounded = round(odd / 5) * 5
        elif -1000 < odd < 0:
            rounded = math.floor(odd / 5) * 5
        else:
            rounded = math.floor(odd / 25) * 25

        if rounded > 0:
            transformed = 1 + (rounded / 100)
        else:
            transformed = 1 - (100 / rounded)

        final = math.floor(transformed * 100) / 100

        return final

    def process_league(self, league):
        import datetime
        import hashlib
        import json
        import re
        import sys
        import traceback
        from time import time

        import dateparser
        import requests
        requests.packages.urllib3.disable_warnings()
        from bs4 import BeautifulSoup

        if league in self.bot_data["blacklist_leagues"]:
            return

        requests_loaded = False
        event_found = False
        try:
            db = self.get_db()

            url = "https://scommesse.netbet.it/pagemethods.aspx/GetLeaguesContent"
            post_data = {"leaguesCollection":league, "branchId":"1", "showLive":"false"}
            league_page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            league_page_json = json.loads(league_page_request.content.replace(",,", ",0,").replace(",,", ",0,").replace("[,", "[").replace(",]", "]"))

            requests_loaded = True

            if not isinstance(league_page_json[0][0][1], list):
                if league not in self.bot_data["blacklist_leagues"]:
                    print "no results found in league {0}, blacklisting".format(league)
                    self.bot_data["blacklist_leagues"].append(league)
                return

            betfair_events = self.get_betfair_events(db)
            league_events = []
            for event in league_page_json[0][0][1]:
                id_1x2 = None
                id_ou = None
                for odds in event[5]:
                    if odds[1] == 0:
                        id_1x2 = odds[0]
                    if odds[1] == 200:
                        id_ou = odds[0]

                id_ggng = None
                for odds in event[6]:
                    if odds[1] == 158:
                        id_ggng = odds[0]

                event_id = event[0]
                home = event[1].strip()
                away = event[2].strip()
                name = home + " - " + away
                open_date = (datetime.datetime.strptime(event[4][:-8], "%Y-%m-%dT%H:%M:%S") + datetime.timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S")
                event_hash = hashlib.md5(name + str(event_id)).hexdigest()
                league_events.append({
                    "name": name,
                    "home": home,
                    "away": away,
                    "open_date": open_date,
                    "event_hash": event_hash,
                    "id_1x2": id_1x2,
                    "id_ou": id_ou,
                    "id_ggng": id_ggng,
                })

            url = "https://scommesse.netbet.it/pagemethods.aspx/UpdateEvents"
            id_list_1x2 = "@".join(str(event["id_1x2"]) for event in league_events if event["id_1x2"] is not None)
            id_list_ou = "@".join(str(event["id_ou"]) for event in league_events if event["id_ou"] is not None)
            id_list_ggng = "@".join(str(event["id_ggng"]) for event in league_events if event["id_ggng"] is not None)
            post_data = {"requestString": "@".join(id_list for id_list in [id_list_1x2, id_list_ou, id_list_ggng] if id_list)}
            odds_page_request = requests.post(url, data=post_data, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            odds_page_json = json.loads(odds_page_request.content.replace(",,", ",0,").replace(",,", ",0,").replace("[,", "[").replace(",]", "]"))

            for event in league_events:
                home = event["home"]
                away = event["away"]
                name = event["name"]
                open_date = event["open_date"]
                event_hash = event["event_hash"]
                id_1x2 = event["id_1x2"]
                id_ou = event["id_ou"]
                id_ggng = event["id_ggng"]

                all_book_odds = self.get_empty_odds_dict()

                for odds_section in odds_page_json:
                    if id_1x2 and odds_section[0] == id_1x2:
                        home_odds = float(odds_section[2][0][1][1])
                        if home_odds != 0:
                            all_book_odds["home"] = self.round_odd(home_odds)

                        away_odds = float(odds_section[2][0][1][5])
                        if away_odds != 0:
                            all_book_odds["away"] = self.round_odd(away_odds)

                        draw_odds = float(odds_section[2][0][1][3])
                        if draw_odds != 0:
                            all_book_odds["draw"] = self.round_odd(draw_odds)

                    for uo_section in odds_section[2]:
                        if id_ou and odds_section[0] == id_ou and re.match("\d\.\d", str(uo_section[3][1])):
                            uo_type = str(uo_section[3][1]).replace(".", "")

                            under_odds = float(uo_section[3][4])
                            if under_odds != 0:
                                all_book_odds["under_" + uo_type] = self.round_odd(under_odds)

                            over_odds = float(uo_section[3][2])
                            if over_odds != 0:
                                all_book_odds["over_" + uo_type] = self.round_odd(over_odds)

                    if id_ggng and odds_section[0] == id_ggng:
                        if odds_section[2][0][2] == "Si":
                            goal_odds = float(odds_section[2][0][1])
                            no_goal_odds = float(odds_section[2][1][1])
                        else:
                            goal_odds = float(odds_section[2][1][1])
                            no_goal_odds = float(odds_section[2][0][1])

                        if goal_odds != 0:
                            all_book_odds["goal"] = self.round_odd(goal_odds)

                        if no_goal_odds != 0:
                            all_book_odds["no_goal"] = self.round_odd(no_goal_odds)

                bf_event = self.pair_match(db, betfair_events, all_book_odds, event_hash, home, away, open_date)
                if not bf_event:
                    print name, open_date, "no bf_event, skipping"
                    if event_hash not in self.bot_data["blacklist"]:
                        self.bot_data["blacklist"].append(event_hash)
                    continue

                print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                event_found = True
                self.save_data(db, event_hash, all_book_odds, bf_event, name, open_date)

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)

        finally:
            if requests_loaded and not event_found and league not in self.bot_data["blacklist_leagues"]:
                print "no event found in league {0}, blacklisting".format(league)
                self.bot_data["blacklist_leagues"].append(league)
            if db:
                db.close()
            sys.stdout.flush()
            sys.stderr.flush()

        return {
            "blacklist":self.bot_data["blacklist"],
            "blacklist_leagues":self.bot_data["blacklist_leagues"],
            "matched_betfair_events":self.matched_betfair_events,
            "matched_events":self.matched_events
        }

    def run(self):
        while True:
            print("round round round round round round round round")
            all_leagues = self.get_leagues()

            with closing(multiprocessing.Pool(processes=8)) as p:
                results = p.map(self.process_league, all_leagues)
                for result in results:
                    if result:
                        self.bot_data["blacklist"].extend([event for event in result["blacklist"] if event not in self.bot_data["blacklist"]])
                        self.bot_data["blacklist_leagues"].extend([league for league in result["blacklist_leagues"] if league not in self.bot_data["blacklist_leagues"]])
                        self.matched_betfair_events.update(result["matched_betfair_events"])
                        self.matched_events.update(result["matched_events"])
                p.terminate()

        sleep(5)


if __name__ == '__main__':
    bot_data = {
        "blacklist": [],
        "blacklist_leagues": [],
        "region": "it",
    }

    t = Netbet(bot_data)
    t.run()
