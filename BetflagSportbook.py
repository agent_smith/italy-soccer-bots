import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time
import cookielib
from cookielib import CookieJar
import urllib2
import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from xml.dom import minidom
import ssl
from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class BetflagSportbook(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.ctx = ssl.create_default_context()
        self.ctx.check_hostname = False
        self.ctx.verify_mode = ssl.CERT_NONE

    def get_leagues(self):
        all_leagues = []

        try:
            self.cj = CookieJar()
            self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj),urllib2.ProxyHandler(self.get_proxy()))
            self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://www.betflag.it/sport'),('Connection', 'keep-alive'),('Content-Type','application/json; charset=UTF-8'),('Accept-Encoding','gzip, deflate, sdch, br')]            
            url = "https://www.betflag.it/sport"
            req = urllib2.Request(url)
            resp = self.opener.open(req)
            if resp.info().get('Content-Encoding') == 'gzip':
                buf = StringIO(resp.read())
                resp = gzip.GzipFile(fileobj=buf)
            resp = resp.read()  
            soup = BeautifulSoup(resp,"lxml")
            all_leagues = [str(span["onclick"].split(",")[-1].replace("'","").replace(")","")).strip().split("-")[0] for span in soup.find("span",{"title":"Calcio"}).findNext("ul").findAll("span") if span.has_key("title")]
            
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue
                self.init_betfair_events()
                event_found = False
                try:
                    self.cj = CookieJar()
                    self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj),urllib2.ProxyHandler(self.get_proxy()))
                    self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://www.betflag.it/sport'),('Connection', 'keep-alive'),('Content-Type','application/json; charset=UTF-8'),('Accept-Encoding','gzip, deflate, sdch, br')]                   
                        
                    postdata = '{"idS":"1", "idM":"'+league+'", "refreshData":false }'
                    url = "https://www.betflag.it/Services/WSsport.asmx/GetEvents?nxc=4736669"
                    req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                    resp = self.opener.open(req)
                    if resp.info().get('Content-Encoding') == 'gzip':
                        buf = StringIO(resp.read())
                        resp = gzip.GzipFile(fileobj=buf)
                    resp = json.loads(resp.read())
                    resp = resp["d"][0]["XMLE"]
                    resp = resp.encode('utf-8')
                    xmldoc = minidom.parseString(resp)
                    
                    for avv in xmldoc.getElementsByTagName('avv'):
                        dt = avv.attributes["dt"].value.replace("T"," ")
                        name = self.db.converter.escape(avv.attributes["da"].value)
                        if(len(name.split(" - "))<2):
                            continue
                        home = name.split(" - ")[0].strip()
                        away = name.split(" - ")[1].strip()
                        av = avv.attributes["avv"].value
                        pal = avv.attributes["pal"].value
                        
                        postdata = '{"idS":"1", "idM":"'+league+'", "pal":"'+pal+'", "avv":"'+av+'", "con":"0", "sco":"3", "section":""}'
                        url = "https://www.betflag.it/Services/WSsport.asmx/GetOdds?nxc=1939824"
                        req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                        resp = self.opener.open(req)
                        if resp.info().get('Content-Encoding') == 'gzip':
                            buf = StringIO(resp.read())
                            resp = gzip.GzipFile(fileobj=buf)
                        resp = json.loads(resp.read())
                        resp = resp["d"][0]["XMLODDS"]
                        resp = resp.encode('utf-8')
                        xmldoc2 = minidom.parseString(resp)
                        all_book_odds = self.get_empty_odds_dict()
                        
                        event_hash = hashlib.md5(dt+league+name).hexdigest()
                        for q in xmldoc2.getElementsByTagName('q'):
                            if(q.attributes["de"].value == "1"):
                                all_book_odds["home"] = q.attributes["q"].value
                            elif(q.attributes["de"].value == "2"):
                                all_book_odds["away"] = q.attributes["q"].value
                            elif(q.attributes["de"].value == "X"):
                                all_book_odds["draw"] = q.attributes["q"].value
                        if(all_book_odds["home"] == "" or all_book_odds["away"] == "" or all_book_odds["draw"] == ""):
                            continue
                        open_date = dateparser.parse(dt).strftime('%Y-%m-%d %H:%M:%S')
                            
                        if(all_book_odds["home"] == "NULL" or all_book_odds["away"] == "NULL" or all_book_odds["draw"] == "NULL"):
                            continue
                            
                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            #print name, "no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue  
                            
                        if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None":
                            postdata = '{"idS":"1", "idM":"'+league+'", "pal":"'+pal+'", "avv":"'+av+'", "con":"0", "sco":"7989", "section":""}'
                            url = "https://www.betflag.it/Services/WSsport.asmx/GetOdds?nxc=1939826"
                            req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                            resp = self.opener.open(req)
                            if resp.info().get('Content-Encoding') == 'gzip':
                                buf = StringIO(resp.read())
                                resp = gzip.GzipFile(fileobj=buf)
                            resp = json.loads(resp.read())
                            resp = resp["d"][0]["XMLODDS"]
                            resp = resp.encode('utf-8')
                            xmldoc3 = minidom.parseString(resp)
                            for q in xmldoc3.getElementsByTagName('q'):
                                if(q.attributes["de"].value == "UNDER" and q.attributes["iad"].value == "0.5"):
                                    all_book_odds["under_05"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "OVER" and q.attributes["iad"].value == "0.5"):
                                    all_book_odds["over_05"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "UNDER" and q.attributes["iad"].value == "1.5"):
                                    all_book_odds["under_15"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "OVER" and q.attributes["iad"].value == "1.5"):
                                    all_book_odds["over_15"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "UNDER" and q.attributes["iad"].value == "2.5"):
                                    all_book_odds["under_25"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "OVER" and q.attributes["iad"].value == "2.5"):
                                    all_book_odds["over_25"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "UNDER" and q.attributes["iad"].value == "3.5"):
                                    all_book_odds["under_35"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "OVER" and q.attributes["iad"].value == "3.5"):
                                    all_book_odds["over_35"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "UNDER" and q.attributes["iad"].value == "4.5"):
                                    all_book_odds["under_45"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "OVER" and q.attributes["iad"].value == "4.5"):
                                    all_book_odds["over_45"] = q.attributes["q"].value
                        
                        if bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":        
                            postdata = '{"idS":"1", "idM":"'+league+'", "pal":"'+pal+'", "avv":"'+av+'", "con":"0", "sco":"18", "section":""}'
                            url = "https://www.betflag.it/Services/WSsport.asmx/GetOdds?nxc=1939827"
                            req = urllib2.Request(url, data=postdata,headers={'Content-Type': 'application/json'})
                            resp = self.opener.open(req)
                            if resp.info().get('Content-Encoding') == 'gzip':
                                buf = StringIO(resp.read())
                                resp = gzip.GzipFile(fileobj=buf)
                            resp = json.loads(resp.read())
                            resp = resp["d"][0]["XMLODDS"]
                            resp = resp.encode('utf-8')
                            xmldoc3 = minidom.parseString(resp)
                            for q in xmldoc3.getElementsByTagName('q'):
                                if(q.attributes["de"].value == "GOAL"):
                                    all_book_odds["goal"] = q.attributes["q"].value
                                elif(q.attributes["de"].value == "NOGOAL"):
                                    all_book_odds["no_goal"] = q.attributes["q"].value
                                        
                        print name, open_date, all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"]
                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        #print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = BetflagSportbook(bot_data)
t.run()