import datetime
import cookielib
from cookielib import CookieJar
import urllib2
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time
import ssl
import dateparser
import requests
from bs4 import BeautifulSoup
from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Betstars(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.cj = CookieJar()
        self.ctx = ssl.create_default_context()
        self.ctx.check_hostname = False
        self.ctx.verify_mode = ssl.CERT_NONE

    def get_leagues(self):
        all_leagues = []
        try:
            self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
            self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://sports.betstars.it/'),('Connection', 'keep-alive')]

            url = "https://sports.betstars.it/sportsbook/v1/api/getSportTree?sport=SOCCER&includeOutrights=false&includeEvents=false&channelId=3&locale=it-it&siteId=16"
            req = urllib2.Request(url)
            resp = self.opener.open(req)
            main_page_json = json.loads(resp.read())
            all_leagues = [league["id"] for league in main_page_json["categories"]]

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://sports.betstars.it/sportsbook/v1/api/getCompetitionEvents?competitionId={0}&marketTypes=MRES&includeOutrights=false&channelId=3&locale=it-it&siteId=16".format(league)
                    self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
                    self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://sports.betstars.it/'),('Connection', 'keep-alive')]
                    req = urllib2.Request(url)
                    resp = self.opener.open(req)
                    league_page_json = json.loads(resp.read())

                    if not "event" in league_page_json or not league_page_json["event"]:
                        continue

                    url = "https://sports.betstars.it/sportsbook/v1/api/getCompetitionEvents?competitionId={0}&marketTypes=BTSC&includeOutrights=false&channelId=3&locale=it-it&siteId=16".format(league)
                    self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
                    self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://sports.betstars.it/'),('Connection', 'keep-alive')]
                    req = urllib2.Request(url)
                    resp = self.opener.open(req)
                    ggng_page_json = json.loads(resp.read())


                    url = "https://sports.betstars.it/sportsbook/v1/api/getCompetitionEvents?competitionId={0}&marketTypes=OVUN&includeOutrights=false&channelId=3&locale=it-it&siteId=16".format(league)
                    self.opener = urllib2.build_opener(urllib2.HTTPSHandler(context=self.ctx),urllib2.HTTPCookieProcessor(self.cj))
                    self.opener.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'),('Accept-Language', 'en-US,en;q=0.8'),('Referer','https://sports.betstars.it/'),('Connection', 'keep-alive')]
                    req = urllib2.Request(url)
                    resp = self.opener.open(req)
                    uo_page_json = json.loads(resp.read())

                    self.init_betfair_events()
                    for event in league_page_json["event"]:
                        event_id = event["id"]
                        home, away = [team.strip() for team in event["name"].split(" vs ")]
                        open_date = dateparser.parse(str(event["eventTime"])).strftime("%Y-%m-%d %H:%M:%S")
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()
                        for market in event["markets"]:
                            if market["name"] == "Risultato finale":
                                for selection in market["selection"]:
                                    if selection["type"] == "H":
                                        all_book_odds["home"] = float(selection["odds"]["dec"])
                                    if selection["type"] == "A":
                                        all_book_odds["away"] = float(selection["odds"]["dec"])
                                    if selection["type"] == "D":
                                        all_book_odds["draw"] = float(selection["odds"]["dec"])

                        if "event" in ggng_page_json:
                            for ggng_event in ggng_page_json["event"]:
                                if ggng_event["id"] == event_id:
                                    for market in ggng_event["markets"]:
                                        if market["name"] == "Entrambe le squadre segnano":
                                            for selection in market["selection"]:
                                                if selection["type"] == "Y":
                                                    all_book_odds["goal"] = float(selection["odds"]["dec"])
                                                if selection["type"] == "N":
                                                    all_book_odds["no_goal"] = float(selection["odds"]["dec"])

                        if "event" in uo_page_json:
                            for uo_event in uo_page_json["event"]:
                                if uo_event["id"] == event_id:
                                    for market in uo_event["markets"]:
                                        if market["name"] == "Totale goal":
                                            if market["line"] == 0.5:
                                                for selection in market["selection"]:
                                                    if selection["type"] == "U":
                                                        all_book_odds["under_05"] = float(selection["odds"]["dec"])
                                                    if selection["type"] == "O":
                                                        all_book_odds["over_05"] = float(selection["odds"]["dec"])
                                            if market["line"] == 1.5:
                                                for selection in market["selection"]:
                                                    if selection["type"] == "U":
                                                        all_book_odds["under_15"] = float(selection["odds"]["dec"])
                                                    if selection["type"] == "O":
                                                        all_book_odds["over_15"] = float(selection["odds"]["dec"])
                                            if market["line"] == 2.5:
                                                for selection in market["selection"]:
                                                    if selection["type"] == "U":
                                                        all_book_odds["under_25"] = float(selection["odds"]["dec"])
                                                    if selection["type"] == "O":
                                                        all_book_odds["over_25"] = float(selection["odds"]["dec"])
                                            if market["line"] == 3.5:
                                                for selection in market["selection"]:
                                                    if selection["type"] == "U":
                                                        all_book_odds["under_35"] = float(selection["odds"]["dec"])
                                                    if selection["type"] == "O":
                                                        all_book_odds["over_35"] = float(selection["odds"]["dec"])
                                            if market["line"] == 4.5:
                                                for selection in market["selection"]:
                                                    if selection["type"] == "U":
                                                        all_book_odds["under_45"] = float(selection["odds"]["dec"])
                                                    if selection["type"] == "O":
                                                        all_book_odds["over_45"] = float(selection["odds"]["dec"])

                        bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                        if not bf_event:
                            print name + " no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Betstars(bot_data)
t.run()
