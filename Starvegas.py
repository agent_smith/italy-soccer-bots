import datetime
import json
import re
import sys
import traceback
from contextlib import closing
from time import sleep

import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup
from pathos import multiprocessing

from common_multi import Common

reload(sys)
sys.setdefaultencoding("UTF-8")

class Starvegas(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "it,en-US;q=0.7,en;q=0.3",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://e1-api.aws.kambicdn.com/offering/api/v2/gtit/group.json?lang=it_IT&market=IT&channel_id=1&client_id=2&ncid=760722"
            main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            main_page_json = json.loads(main_page_request.content)

            for group in main_page_json["group"]["groups"]:
                if group["englishName"] == "Football":
                    for league in group["groups"]:
                        if league.has_key("groups"):
                            for subleague in league["groups"]:
                                all_leagues.append({"league":league["termKey"], "subleague":subleague["termKey"]})
                        else:
                            all_leagues.append({"league":league["termKey"]})

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def process_league(self, league):
        import datetime
        import hashlib
        import json
        import re
        import sys
        import traceback
        from time import time

        import dateparser
        import requests
        requests.packages.urllib3.disable_warnings()
        from bs4 import BeautifulSoup

        if league in self.bot_data["blacklist_leagues"]:
            return

        requests_loaded = False
        event_found = False
        try:
            db = self.get_db()

            league_name = league["league"]
            subleague_name = league.get("subleague", None)

            url = "https://e1-api.aws.kambicdn.com/offering/api/v3/gtit/listView/football/{0}{1}.json?lang=it_IT&market=IT&client_id=2&channel_id=1&ncid={2}&categoryGroup=COMBINED&displayDefault=true&category=match".format(league_name, ("/" + subleague_name) if subleague_name else "", (int(time())) * 1000)
            league_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            league_page_json = json.loads(league_page_request.content)

            requests_loaded = True

            betfair_events = self.get_betfair_events(db)
            for event in league_page_json["events"]:
                if event["event"]["state"] != "NOT_STARTED":
                    continue
                event_id = event["event"]["id"]
                open_date = (dateparser.parse(str(event["event"]["start"])) + datetime.timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S')
                home = event["event"]["homeName"]
                away = event["event"].get("awayName", None)
                if not away:
                    continue
                name = home + " - " + away
                event_hash = hashlib.md5(name + str(event_id)).hexdigest()

                all_book_odds = self.get_empty_odds_dict()

                for offer in event["betOffers"]:
                    if offer["betOfferType"]["englishName"] == "Match" and offer["criterion"]["englishLabel"] == "Full Time" and offer["outcomes"]:
                        all_book_odds["home"] = float(offer["outcomes"][0]["odds"]) / 1000
                        all_book_odds["away"] = float(offer["outcomes"][2]["odds"]) / 1000
                        all_book_odds["draw"] = float(offer["outcomes"][1]["odds"]) / 1000

                bf_event = self.pair_match(db, betfair_events, all_book_odds, event_hash, home, away, open_date)
                if not bf_event:
                    print name, open_date, "no bf_event, skipping"
                    if event_hash not in self.bot_data["blacklist"]:
                        self.bot_data["blacklist"].append(event_hash)
                    continue

                if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":
                    try:
                        url = "https://e1-api.aws.kambicdn.com/offering/api/v2/gtit/betoffer/event/{0}.json?lang=it_IT&market=IT&client_id=2&channel_id=1&ncid={1}".format(event_id, (int(time())) * 1000)
                        event_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
                        event_page_json = json.loads(event_page_request.content)

                        if "betoffers" in event_page_json:
                            for offer in event_page_json["betoffers"]:
                                if offer["betOfferType"]["englishName"] == "Over/Under" and offer["criterion"]["englishLabel"] == "Total Goals" and offer["outcomes"]:
                                    if offer["outcomes"][1]["line"] == 500:
                                        all_book_odds["under_05"] = float(offer["outcomes"][1]["odds"]) / 1000
                                    if offer["outcomes"][0]["line"] == 500:
                                        all_book_odds["over_05"] = float(offer["outcomes"][0]["odds"]) / 1000
                                    if offer["outcomes"][1]["line"] == 1500:
                                        all_book_odds["under_15"] = float(offer["outcomes"][1]["odds"]) / 1000
                                    if offer["outcomes"][0]["line"] == 1500:
                                        all_book_odds["over_15"] = float(offer["outcomes"][0]["odds"]) / 1000
                                    if offer["outcomes"][1]["line"] == 2500:
                                        all_book_odds["under_25"] = float(offer["outcomes"][1]["odds"]) / 1000
                                    if offer["outcomes"][0]["line"] == 2500:
                                        all_book_odds["over_25"] = float(offer["outcomes"][0]["odds"]) / 1000
                                    if offer["outcomes"][1]["line"] == 3500:
                                        all_book_odds["under_35"] = float(offer["outcomes"][1]["odds"]) / 1000
                                    if offer["outcomes"][0]["line"] == 3500:
                                        all_book_odds["over_35"] = float(offer["outcomes"][0]["odds"]) / 1000
                                    if offer["outcomes"][1]["line"] == 4500:
                                        all_book_odds["under_45"] = float(offer["outcomes"][1]["odds"]) / 1000
                                    if offer["outcomes"][0]["line"] == 4500:
                                        all_book_odds["over_45"] = float(offer["outcomes"][0]["odds"]) / 1000
                                elif offer["betOfferType"]["englishName"] == "Yes/No" and offer["criterion"]["englishLabel"] == "Both Teams To Score" and offer["outcomes"]:
                                    all_book_odds["goal"] = float(offer["outcomes"][0]["odds"]) / 1000
                                    all_book_odds["no_goal"] = float(offer["outcomes"][1]["odds"]) / 1000

                    except Exception:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        traceback.print_exception(exc_type, exc_obj, exc_tb)

                if not self.check_rating(all_book_odds, bf_event):
                    if event_hash not in self.bot_data["blacklist"]:
                        self.bot_data["blacklist"].append(event_hash)
                    continue

                print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                event_found = True
                self.save_data(db, event_hash, all_book_odds, bf_event, name, open_date)

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)

        finally:
            if requests_loaded and not event_found and league not in self.bot_data["blacklist_leagues"]:
                print "no event found in league {0}, blacklisting".format(league)
                self.bot_data["blacklist_leagues"].append(league)
            if db:
                db.close()
            sys.stdout.flush()
            sys.stderr.flush()

        return {
            "blacklist":self.bot_data["blacklist"],
            "blacklist_leagues":self.bot_data["blacklist_leagues"],
            "matched_betfair_events":self.matched_betfair_events,
            "matched_events":self.matched_events
        }

    def run(self):
        while True:
            print("round round round round round round round round")
            all_leagues = self.get_leagues()

            with closing(multiprocessing.Pool(processes=8)) as p:
                results = p.map(self.process_league, all_leagues)
                for result in results:
                    if result:
                        self.bot_data["blacklist"].extend([event for event in result["blacklist"] if event not in self.bot_data["blacklist"]])
                        self.bot_data["blacklist_leagues"].extend([league for league in result["blacklist_leagues"] if league not in self.bot_data["blacklist_leagues"]])
                        self.matched_betfair_events.update(result["matched_betfair_events"])
                        self.matched_events.update(result["matched_events"])
                p.terminate()

            sleep(5)


if __name__ == '__main__':
    bot_data = {
        "blacklist": [],
        "blacklist_leagues": [],
        "region": "it",
    }

    t = Starvegas(bot_data)
    t.run()
