import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from xml.dom import minidom
from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Totosi(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "User-Agent": "Mozilla/5.0 (Linux; U; Android 4.2.2; en-us; sdk Build/JB_MR1.1) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30",
            "Accept": "application/json",
            "Accept-Language":"en-US,en;q=0.8",
            "Accept-Charset":"utf-8, iso-8859-1, utf-16, *;q=0.1",
            "Connection":"keep-alive",
            "context":"5:0:en_US",
            "Referer":"https://www.totosi.it/",
            "Accept-Encoding":"gzip, deflate, sdch, br"
        }
            
    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.totosi.it/ww-adapter/api/v1/c2/cnom/navigation/depth/5206"
            main_page_json = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False).json()
            for s in main_page_json["extNode"]["nodes"]:
                if s["id"] == 6238:
                    for l in s["nodes"]:
                        if(l.has_key("childNodesNum")):
                            for ls in l["nodes"]:
                                all_leagues.append(ls["id"])

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    proxy = self.get_proxy()

                    url = "https://scommesse.totosi.it/ww-adapter/api/v1/c2/cnom/node/{0}/categorymarkettype".format(league)
                    league_page_json = requests.get(url, headers=self.custom_headers, proxies=proxy, verify=False).json()

                    dt = ""
                    self.init_betfair_events()
                        
                    if("events" in league_page_json and "boGroup" in league_page_json["events"] and "events" in league_page_json["events"]["boGroup"]):
                        for e in league_page_json["events"]["boGroup"]["events"]:
                            name = e["name"]
                            if(len(name.split('-'))<2):
                                continue
                            home = name.split('-')[0].strip()
                            away = name.split('-')[1].strip()
                            all_book_odds = self.get_empty_odds_dict()
                            for m in e["eventMarketGroups"]:
                                for g in m["detailedMarkets"]:
                                    if(g is not None and "name" in g and g["name"] == "ESITO FINALE 1X2" and "selections" in g):
                                        for s in g["selections"]:
                                            if(s["name"]=="X"):
                                                all_book_odds["draw"] = s["priceValues"][0]["formatValue"]
                                            elif(s["name"]=="1"):
                                                all_book_odds["home"] = s["priceValues"][0]["formatValue"]
                                            elif(s["name"]=="2"):
                                                all_book_odds["away"] = s["priceValues"][0]["formatValue"]
                            open_date = datetime.datetime.utcfromtimestamp((int(e["startTime"])/1000)).strftime('%Y-%m-%d %H:%M:%S')

                            event_hash = hashlib.md5(open_date+name).hexdigest()
                            if all_book_odds["home"] == "NULL" or all_book_odds["away"] == "NULL" or all_book_odds["draw"] == "NULL":
                                continue

                            bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                            if not bf_event:
                                print name, "no bf_event, skipping"
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue

                            if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":
                                url = "https://scommesse.totosi.it/ww-adapter/api/v1/c2/tsm/event/"+str(e["id"])
                                event_page_json = requests.get(url, headers=self.custom_headers, proxies=proxy, verify=False).json()
                                
                                if("event" in event_page_json):
                                    for m1 in event_page_json["event"]["eventMarketGroups"]:
                                        if(m1["name"] == "PIU' GIOCATE"):
                                            for m2 in m1["detailedMarkets"]:
                                                if(m2["name"] == "UNDER / OVER 0,5"):
                                                    for s1 in m2["selections"]:
                                                        if(s1["name"]=="UNDER"):
                                                            all_book_odds["under_05"] = s1["priceValues"][0]["formatValue"]
                                                        elif(s1["name"]=="OVER"):
                                                            all_book_odds["over_05"] = s1["priceValues"][0]["formatValue"]
                                                elif(m2["name"] == "UNDER / OVER 1,5"):
                                                    for s1 in m2["selections"]:
                                                        if(s1["name"]=="UNDER"):
                                                            all_book_odds["under_15"] = s1["priceValues"][0]["formatValue"]
                                                        elif(s1["name"]=="OVER"):
                                                            all_book_odds["over_15"] = s1["priceValues"][0]["formatValue"]
                                                elif(m2["name"] == "UNDER / OVER 2,5"):
                                                    for s1 in m2["selections"]:
                                                        if(s1["name"]=="UNDER"):
                                                            all_book_odds["under_25"] = s1["priceValues"][0]["formatValue"]
                                                        elif(s1["name"]=="OVER"):
                                                            all_book_odds["over_25"] = s1["priceValues"][0]["formatValue"]
                                                elif(m2["name"] == "UNDER / OVER 3,5"):
                                                    for s1 in m2["selections"]:
                                                        if(s1["name"]=="UNDER"):
                                                            all_book_odds["under_35"] = s1["priceValues"][0]["formatValue"]
                                                        elif(s1["name"]=="OVER"):
                                                            all_book_odds["over_35"] = s1["priceValues"][0]["formatValue"]
                                                elif(m2["name"] == "UNDER / OVER 4,5"):
                                                    for s1 in m2["selections"]:
                                                        if(s1["name"]=="UNDER"):
                                                            all_book_odds["under_45"] = s1["priceValues"][0]["formatValue"]
                                                        elif(s1["name"]=="OVER"):
                                                            all_book_odds["over_45"] = s1["priceValues"][0]["formatValue"]
                                                elif(m2["name"] == "GOAL/NO GOAL"):
                                                    for s1 in m2["selections"]:
                                                        if(s1["name"]=="GOAL"):
                                                            all_book_odds["goal"] = s1["priceValues"][0]["formatValue"]
                                                        elif(s1["name"]=="NOGOAL"):
                                                            all_book_odds["no_goal"] = s1["priceValues"][0]["formatValue"]

                            if not self.check_rating(all_book_odds, bf_event):
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue

                            print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                            event_found = True
                            self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(1, 4))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Totosi(bot_data)
t.run()
