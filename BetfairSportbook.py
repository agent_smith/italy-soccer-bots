import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class BetfairSportbook(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "Host":"www.betfair.it",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }
        self.proxy_data = {}
        self.all_countries = []

    def get_initialized_proxy(self):
        proxy = self.get_proxy()

        initialized_proxy = self.proxy_data.get(frozenset(proxy), None)
        if not initialized_proxy:
            url = "https://www.betfair.it/sport/football"
            xsrf_token_page_request = requests.get(url, headers=self.custom_headers, proxies=proxy)
            xsrf_token_page_soup = BeautifulSoup(xsrf_token_page_request.content, "lxml")

            xsrf_section = xsrf_token_page_soup.find("a", href=re.compile(".*xsrftoken=.*"))
            if xsrf_section:
                xsrf_token = re.search(".*xsrftoken=(.*)&?", xsrf_section["href"]).group(1)

            initialized_proxy = {"proxy":proxy, "xsrf_token":xsrf_token, "cookies":xsrf_token_page_request.cookies}
            self.proxy_data[frozenset(proxy)] = initialized_proxy

        return initialized_proxy

    def save_cookies(self, cookies, proxy):
        self.proxy_data[frozenset(proxy["proxy"])]["cookies"] = cookies

    def get_next_country_leagues(self):
        country_leagues = []
        try:
            if not self.all_countries:
                proxy = self.get_initialized_proxy()
                url = "https://www.betfair.it/sport/football?id=&action=browseAll&selectedTabType=FEATURED&modules=multipickavbId%401006&d18=Main&d31=Middle&lastId=1057&isAjax=true&ts={0}&alt=json&xsrftoken={1}".format(int(time() * 1000), proxy["xsrf_token"])
                main_page_request = requests.get(url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                self.save_cookies(main_page_request.cookies, proxy)
                main_page_json = json.loads(main_page_request.content)

                for section in main_page_json["page"]["config"]["instructions"]:
                    if "container" in section["arguments"] and "browse-all-container" in section["arguments"]["container"]:
                        html = section["arguments"]["html"]
                        soup = BeautifulSoup(html, "lxml")
                        self.all_countries = ["https://www.betfair.it" + str(a["href"]) + "&d18=Main&d31=Middle&lastId=1057&isAjax=true&ts={0}&alt=json&xsrftoken={1}".format(int(time() * 1000), proxy["xsrf_token"]) for a in soup.find_all("a", {"class":"outrights-list-item"})]
                        break

            country_url = self.all_countries.pop(0)

            proxy = self.get_initialized_proxy()
            country_page_request = requests.get(country_url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
            self.save_cookies(country_page_request.cookies, proxy)
            country_page_json = json.loads(country_page_request.content)

            for section in country_page_json["page"]["config"]["instructions"]:
                if "container" in section["arguments"] and "browse-all-container" in section["arguments"]["container"]:
                    html = section["arguments"]["html"]
                    soup = BeautifulSoup(html, "lxml")
                    country_leagues = ["https://www.betfair.it" + str(a["href"]) + "&d18=Main&d31=Middle&lastId=1057&isAjax=true&ts={0}&alt=json&xsrftoken={1}".format(int(time() * 1000), proxy["xsrf_token"]) for a in soup.find_all("a")]
                    break

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return country_leagues

    def run(self):
        while True:
            country_leagues = self.get_next_country_leagues()

            for league in country_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue
                
                event_found = False
                try:
                    proxy = self.get_initialized_proxy()
                    league_page_request = requests.get(league, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(league_page_request.cookies, proxy)
                    league_page_json = json.loads(league_page_request.content)
                    soup = None
                    for section in league_page_json["page"]["config"]["instructions"]:
                        if "container" in section["arguments"] and "updated-markets" in section["arguments"]["container"]:
                            html = section["arguments"]["html"]
                            soup = BeautifulSoup(html, "lxml")
                            break
                    if not soup:
                        continue

                    url_id = re.search("\?id=(\d+)", league).group(1)
                    url_competition_event_id = re.search("&competitionEventId=(\d+)", league).group(1)

                    ggng_url = "https://www.betfair.it/sport/football?id={0}&marketType=BOTH_TEAMS_TO_SCORE&competitionEventId={1}&action=changeMarketType&selectedTabType=COMPETITION&competitionId=&modules=multipickavbId@1126&d18=Main&d31=Middle&lastId=1125&isAjax=true&ts={2}&alt=json&xsrftoken={3}".format(url_id, url_competition_event_id, int(time() * 1000), proxy["xsrf_token"])
                    proxy = self.get_initialized_proxy()
                    ggng_page_request = requests.get(ggng_url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(ggng_page_request.cookies, proxy)
                    ggng_page_json = json.loads(ggng_page_request.content)
                    ggng_soup = None
                    for section in ggng_page_json["page"]["config"]["instructions"]:
                        if "container" in section["arguments"] and "updated-markets" in section["arguments"]["container"]:
                            html = section["arguments"]["html"]
                            ggng_soup = BeautifulSoup(html, "lxml")
                            break

                    uo05_url = "https://www.betfair.it/sport/football?id={0}&marketType=OVER_UNDER_05&competitionEventId={1}&action=changeMarketType&selectedTabType=COMPETITION&competitionId=&modules=multipickavbId@1126&d18=Main&d31=Middle&lastId=1125&isAjax=true&ts={2}&alt=json&xsrftoken={3}".format(url_id, url_competition_event_id, int(time() * 1000), proxy["xsrf_token"])
                    proxy = self.get_initialized_proxy()
                    uo05_page_request = requests.get(uo05_url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(uo05_page_request.cookies, proxy)
                    uo05_page_json = json.loads(uo05_page_request.content)
                    uo05_soup = None
                    for section in uo05_page_json["page"]["config"]["instructions"]:
                        if "container" in section["arguments"] and "updated-markets" in section["arguments"]["container"]:
                            html = section["arguments"]["html"]
                            uo05_soup = BeautifulSoup(html, "lxml")
                            break

                    uo15_url = "https://www.betfair.it/sport/football?id={0}&marketType=OVER_UNDER_15&competitionEventId={1}&action=changeMarketType&selectedTabType=COMPETITION&competitionId=&modules=multipickavbId@1126&d18=Main&d31=Middle&lastId=1125&isAjax=true&ts={2}&alt=json&xsrftoken={3}".format(url_id, url_competition_event_id, int(time() * 1000), proxy["xsrf_token"])
                    proxy = self.get_initialized_proxy()
                    uo15_page_request = requests.get(uo15_url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(uo15_page_request.cookies, proxy)
                    uo15_page_json = json.loads(uo15_page_request.content)
                    uo15_soup = None
                    for section in uo15_page_json["page"]["config"]["instructions"]:
                        if "container" in section["arguments"] and "updated-markets" in section["arguments"]["container"]:
                            html = section["arguments"]["html"]
                            uo15_soup = BeautifulSoup(html, "lxml")
                            break

                    uo35_url = "https://www.betfair.it/sport/football?id={0}&marketType=OVER_UNDER_35&competitionEventId={1}&action=changeMarketType&selectedTabType=COMPETITION&competitionId=&modules=multipickavbId@1126&d18=Main&d31=Middle&lastId=1125&isAjax=true&ts={2}&alt=json&xsrftoken={3}".format(url_id, url_competition_event_id, int(time() * 1000), proxy["xsrf_token"])
                    proxy = self.get_initialized_proxy()
                    uo35_page_request = requests.get(uo35_url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(uo35_page_request.cookies, proxy)
                    uo35_page_json = json.loads(uo35_page_request.content)
                    uo35_soup = None
                    for section in uo35_page_json["page"]["config"]["instructions"]:
                        if "container" in section["arguments"] and "updated-markets" in section["arguments"]["container"]:
                            html = section["arguments"]["html"]
                            uo35_soup = BeautifulSoup(html, "lxml")
                            break

                    uo45_url = "https://www.betfair.it/sport/football?id={0}&marketType=OVER_UNDER_45&competitionEventId={1}&action=changeMarketType&selectedTabType=COMPETITION&competitionId=&modules=multipickavbId@1126&d18=Main&d31=Middle&lastId=1125&isAjax=true&ts={2}&alt=json&xsrftoken={3}".format(url_id, url_competition_event_id, int(time() * 1000), proxy["xsrf_token"])
                    proxy = self.get_initialized_proxy()
                    uo45_page_request = requests.get(uo45_url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(uo45_page_request.cookies, proxy)
                    uo45_page_json = json.loads(uo45_page_request.content)
                    uo45_soup = None
                    for section in uo45_page_json["page"]["config"]["instructions"]:
                        if "container" in section["arguments"] and "updated-markets" in section["arguments"]["container"]:
                            html = section["arguments"]["html"]
                            uo45_soup = BeautifulSoup(html, "lxml")
                            break

                    self.init_betfair_events()

                    events = soup.find_all("li", {"class":"market-2-columns"})
                    for event in events:
                        event_id = event.find("div", {"data-eventid":True})["data-eventid"]
                        home, away = [team.strip() for team in event.find("a", {"data-event":True})["data-event"].replace(" - ", " v ").split(" v ")]
                        date_section = event.find("span", {"class":"date ui-countdown"})
                        
                        if not date_section:
                            continue
                        date_text = date_section.text.strip().replace("Domani", "Tomorrow")
                        if "Inizio tra" in date_text:
                            minutes_cd = re.search("Inizio tra (\d+)", date_text).group(1)
                            open_date = datetime.datetime.now() + datetime.timedelta(minutes=int(minutes_cd))
                            open_date += datetime.timedelta(minutes=2, seconds=30)
                            open_date -= datetime.timedelta(minutes=open_date.minute % 5, seconds=open_date.second, microseconds=open_date.microsecond)
                        else:
                            open_date = dateparser.parse(date_text)
                        open_date = open_date.strftime("%Y-%m-%d %H:%M:%S")
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()

                        section_1x2 = event.find("div", {"class":"details-market market-3-runners"})
                        if not section_1x2:
                            continue
                        section_1 = section_1x2.find("li", {"class":"sel-0"})
                        if section_1:
                            all_book_odds["home"] = float(section_1.a.span.text.strip())
                        section_2 = section_1x2.find("li", {"class":"sel-2"})
                        if section_2:
                            all_book_odds["away"] = float(section_2.a.span.text.strip())
                        section_x = section_1x2.find("li", {"class":"sel-1"})
                        if section_x:
                            all_book_odds["draw"] = float(section_x.a.span.text.strip())

                        section_uo25 = event.find("div", {"class":"details-market market-2-runners"})
                        if section_uo25:
                            section_under = section_uo25.find("li", {"class":"sel-1"})
                            if section_under:
                                all_book_odds["under_25"] = float(section_under.a.span.text.strip())
                            section_over = section_uo25.find("li", {"class":"sel-0"})
                            if section_over:
                                all_book_odds["over_25"] = float(section_over.a.span.text.strip())

                        if ggng_soup:
                            try:
                                ggng_events = ggng_soup.find_all("li", {"class":"market-2-columns"})
                                for ggng_event in ggng_events:
                                    ggng_event_id = ggng_event.find("div", {"data-eventid":True})["data-eventid"]
                                    if ggng_event_id == event_id:
                                        section_ggng = ggng_event.find("div", {"class":"details-market market-2-runners"})
                                        if section_ggng:
                                            section_goal = section_ggng.find("li", {"class":"sel-0"})
                                            if section_goal:
                                                all_book_odds["goal"] = float(section_goal.a.span.text.strip())
                                            section_no_goal = section_ggng.find("li", {"class":"sel-1"})
                                            if section_no_goal:
                                                all_book_odds["no_goal"] = float(section_no_goal.a.span.text.strip())
                            except Exception:
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                traceback.print_exception(exc_type, exc_obj, exc_tb)

                        if uo05_soup:
                            try:
                                uo05_events = uo05_soup.find_all("li", {"class":"market-2-columns"})
                                for uo05_event in uo05_events:
                                    uo05_event_id = uo05_event.find("div", {"data-eventid":True})["data-eventid"]
                                    if uo05_event_id == event_id:
                                        section_uo05 = uo05_event.find("div", {"class":"details-market market-2-runners"})
                                        if section_uo05:
                                            section_under05 = section_uo05.find("li", {"class":"sel-1"})
                                            if section_under05:
                                                all_book_odds["under_05"] = float(section_under05.a.span.text.strip())
                                            section_over05 = section_uo05.find("li", {"class":"sel-0"})
                                            if section_over05:
                                                all_book_odds["over_05"] = float(section_over05.a.span.text.strip())
                            except Exception:
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                traceback.print_exception(exc_type, exc_obj, exc_tb)

                        if uo15_soup:
                            try:
                                uo15_events = uo15_soup.find_all("li", {"class":"market-2-columns"})
                                for uo15_event in uo15_events:
                                    uo15_event_id = uo15_event.find("div", {"data-eventid":True})["data-eventid"]
                                    if uo15_event_id == event_id:
                                        section_uo15 = uo15_event.find("div", {"class":"details-market market-2-runners"})
                                        if section_uo15:
                                            section_under15 = section_uo15.find("li", {"class":"sel-1"})
                                            if section_under15:
                                                all_book_odds["under_15"] = float(section_under15.a.span.text.strip())
                                            section_over15 = section_uo15.find("li", {"class":"sel-0"})
                                            if section_over15:
                                                all_book_odds["over_15"] = float(section_over15.a.span.text.strip())
                            except Exception:
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                traceback.print_exception(exc_type, exc_obj, exc_tb)

                        if uo35_soup:
                            try:
                                uo35_events = uo35_soup.find_all("li", {"class":"market-2-columns"})
                                for uo35_event in uo35_events:
                                    uo35_event_id = uo35_event.find("div", {"data-eventid":True})["data-eventid"]
                                    if uo35_event_id == event_id:
                                        section_uo35 = uo35_event.find("div", {"class":"details-market market-2-runners"})
                                        if section_uo35:
                                            section_under35 = section_uo35.find("li", {"class":"sel-1"})
                                            if section_under35:
                                                all_book_odds["under_35"] = float(section_under35.a.span.text.strip())
                                            section_over35 = section_uo35.find("li", {"class":"sel-0"})
                                            if section_over35:
                                                all_book_odds["over_35"] = float(section_over35.a.span.text.strip())
                            except Exception:
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                traceback.print_exception(exc_type, exc_obj, exc_tb)

                        if uo45_soup:
                            try:
                                uo45_events = uo45_soup.find_all("li", {"class":"market-2-columns"})
                                for uo45_event in uo45_events:
                                    uo45_event_id = uo45_event.find("div", {"data-eventid":True})["data-eventid"]
                                    if uo45_event_id == event_id:
                                        section_uo45 = uo45_event.find("div", {"class":"details-market market-2-runners"})
                                        if section_uo45:
                                            section_under45 = section_uo45.find("li", {"class":"sel-1"})
                                            if section_under45:
                                                all_book_odds["under_45"] = float(section_under45.a.span.text.strip())
                                            section_over45 = section_uo45.find("li", {"class":"sel-0"})
                                            if section_over45:
                                                all_book_odds["over_45"] = float(section_over45.a.span.text.strip())
                            except Exception:
                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                traceback.print_exception(exc_type, exc_obj, exc_tb)

                        bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                        if not bf_event:
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = BetfairSportbook(bot_data)
t.run()
