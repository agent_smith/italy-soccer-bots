import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Williamhill(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "Host":"sports.williamhill.it",
            "Referer":"http://sports.williamhill.it/bet_ita/it",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def get_leagues(self):
        all_leagues = []
        try:
            url = "http://sports.williamhill.it/bet_ita/it/betting/y/5/Calcio.html"
            main_page_request = requests.get(url, headers=self.custom_headers, verify=False)
            main_page_soup = BeautifulSoup(main_page_request.content, "lxml")

            leagues_container = main_page_soup.find("div", {"class":"listContainer"})
            if leagues_container:
                all_leagues = [a["href"] for a in leagues_container.find_all("a")]

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()
            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    league_page_request = requests.get(league, headers=self.custom_headers, verify=False)
                    league_page_soup = BeautifulSoup(league_page_request.content, "lxml")

                    event_rows = []
                    next_matches_sections = league_page_soup.find_all("span", string=re.compile(".*Esito Finale 1X2.*"))
                    for next_matches_section in next_matches_sections:
                        event_rows = next_matches_section.parent.parent.parent.parent.find_all("tr", {"class":"rowOdd"})
                        if event_rows:
                            break

                    if not event_rows:
                        continue

                    all_jsons = [json.loads(js[0]) for js in re.findall("document\.uc_list\.create_prebuilt_event\(\n((.*\n)+?)\);", str(league_page_soup))]

                    uo05_page_soup = None
                    uo05_link = league_page_soup.find("a", string=re.compile(".*Over/Under 0.5 Goal.*"))
                    if uo05_link:
                        uo05_page_request = requests.get(uo05_link["href"], headers=self.custom_headers, verify=False)
                        uo05_page_soup = BeautifulSoup(uo05_page_request.content, "lxml")
                    uo15_page_soup = None
                    uo15_link = league_page_soup.find("a", string=re.compile(".*Over/Under 1.5 Goal.*"))
                    if uo15_link:
                        uo15_page_request = requests.get(uo15_link["href"], headers=self.custom_headers, verify=False)
                        uo15_page_soup = BeautifulSoup(uo15_page_request.content, "lxml")
                    uo25_page_soup = None
                    uo25_link = league_page_soup.find("a", string=re.compile(".*Over/Under 2.5 Goal.*"))
                    if uo25_link:
                        uo25_page_request = requests.get(uo25_link["href"], headers=self.custom_headers, verify=False)
                        uo25_page_soup = BeautifulSoup(uo25_page_request.content, "lxml")
                    uo35_page_soup = None
                    uo35_link = league_page_soup.find("a", string=re.compile(".*Over/Under 3.5 Goal.*"))
                    if uo35_link:
                        uo35_page_request = requests.get(uo35_link["href"], headers=self.custom_headers, verify=False)
                        uo35_page_soup = BeautifulSoup(uo35_page_request.content, "lxml")
                    uo45_page_soup = None
                    uo45_link = league_page_soup.find("a", string=re.compile(".*Over/Under 4.5 Goal.*"))
                    if uo45_link:
                        uo45_page_request = requests.get(uo45_link["href"], headers=self.custom_headers, verify=False)
                        uo45_page_soup = BeautifulSoup(uo45_page_request.content, "lxml")
                    ggng_page_soup = None
                    ggng_link = league_page_soup.find("a", string=re.compile(".*Goal/No Goal.*"))
                    if ggng_link:
                        ggng_page_request = requests.get(ggng_link["href"], headers=self.custom_headers, verify=False)
                        ggng_page_soup = BeautifulSoup(ggng_page_request.content, "lxml")
                    self.init_betfair_events()
                    for event_row in event_rows:
                        event_id = re.search(".*?(\d+)", event_row["id"]).group(1)
                        home, away = [team.strip() for team in event_row.find("span", id=re.compile(".*_mkt_namespace")).text.replace("\xc2\xa0", " ").split(" - ")]
                        open_date = None
                        for js in all_jsons:
                            if js["event"] == event_id:
                                open_date = (datetime.datetime.strptime(js["start_time"], "%Y-%m-%d %H:%M:%S") + datetime.timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
                        if not open_date:
                            continue
                        name = home + " - " + away
                        event_hash = hashlib.md5(open_date + name).hexdigest()
                        all_book_odds = self.get_empty_odds_dict()

                        try:
                            all_book_odds["home"], all_book_odds["draw"], all_book_odds["away"] = [float(odd.text.strip()) for odd in event_row.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass


                        try:
                            if uo05_page_soup:
                                uo05_section = uo05_page_soup.find("div", {"class":"marketHolderExpanded"}, id=re.compile(".*{0}$".format(event_id)))
                                if uo05_section:
                                    all_book_odds["under_05"], all_book_odds["over_05"] = [float(odd.text.strip()) for odd in uo05_section.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass
                        try:
                            if uo15_page_soup:
                                uo15_section = uo15_page_soup.find("div", {"class":"marketHolderExpanded"}, id=re.compile(".*{0}$".format(event_id)))
                                if uo15_section:
                                    all_book_odds["under_15"], all_book_odds["over_15"] = [float(odd.text.strip()) for odd in uo15_section.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass
                        try:
                            if uo25_page_soup:
                                uo25_section = uo25_page_soup.find("div", {"class":"marketHolderExpanded"}, id=re.compile(".*{0}$".format(event_id)))
                                if uo25_section:
                                    all_book_odds["under_25"], all_book_odds["over_25"] = [float(odd.text.strip()) for odd in uo25_section.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass
                        try:
                            if uo35_page_soup:
                                uo35_section = uo35_page_soup.find("div", {"class":"marketHolderExpanded"}, id=re.compile(".*{0}$".format(event_id)))
                                if uo35_section:
                                    all_book_odds["under_35"], all_book_odds["over_35"] = [float(odd.text.strip()) for odd in uo35_section.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass
                        try:
                            if uo45_page_soup:
                                uo45_section = uo45_page_soup.find("div", {"class":"marketHolderExpanded"}, id=re.compile(".*{0}$".format(event_id)))
                                if uo45_section:
                                    all_book_odds["under_45"], all_book_odds["over_45"] = [float(odd.text.strip()) for odd in uo45_section.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass
                        try:
                            if ggng_page_soup:
                                ggng_section = ggng_page_soup.find("tr", {"class":"rowOdd"}, id=re.compile(".*{0}$".format(event_id)))
                                if ggng_section:
                                    all_book_odds["goal"], all_book_odds["no_goal"] = [float(odd.text.strip()) for odd in ggng_section.find_all("div", {"class":"eventprice"})]
                        except ValueError:
                            pass

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name + " no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name, open_date, all_book_odds["home"], all_book_odds["draw"], all_book_odds["away"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Williamhill(bot_data)
t.run()
