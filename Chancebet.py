import datetime
import json
import re
import sys
import traceback
from contextlib import closing
from time import sleep

import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup
from pathos import multiprocessing

from common_multi import Common

reload(sys)
sys.setdefaultencoding("UTF-8")

class Chancebet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"*/*",
            "Accept-Encoding":"gzip, deflate",
            "Accept-Language":"en-US,en;q=0.8",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
            "X-Requested-With":"XMLHttpRequest",
        }

    def get_leagues(self):
        all_leagues = []
        try:
            url = "https://www.chancebet.it/SportsBookAPI.svc/PrematchSchedule/"
            main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            main_page_json = json.loads(main_page_request.content)

            for category in main_page_json:
                if category["Name"] != "Calcio":
                    continue
                for league in category["Children"]:
                    for subleague in league["Children"]:
                        if not subleague["Special"]:
                            all_leagues.append(subleague["Id"])
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def process_league(self, league):
        import hashlib
        import json
        import re
        import sys
        import traceback

        import dateparser
        import requests
        requests.packages.urllib3.disable_warnings()

        if league in self.bot_data["blacklist_leagues"]:
            return

        requests_loaded = False
        event_found = False
        try:
            db = self.get_db()

            url = "https://www.chancebet.it/SportsBookAPI.svc/Odds/{0}/".format(league)
            league_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy(), verify=False, timeout=4)
            league_page_json = json.loads(league_page_request.content)

            requests_loaded = True

            betfair_events = self.get_betfair_events(db)
            for event in league_page_json["events"]:
                event_id = event["Id"]
                home, away = re.sub("\([^()]*\)", "", event["Name"]).strip().split("-")
                open_date = (dateparser.parse(re.search("(\d+)", event["StartDate"]).group(1)) + datetime.timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S")
                name = home + " - " + away
                event_hash = hashlib.md5(name + str(event_id)).hexdigest()

                all_book_odds = self.get_empty_odds_dict()

                for event_odds in league_page_json["eventsOutcome"]:
                    if event_odds["EventId"] == event_id:
                        if event_odds["Kind"]["Name"] == "1X2 Finale":
                            if event_odds["Name"] == "1":
                                all_book_odds["home"] = event_odds["Odds"]
                            elif event_odds["Name"] == "2":
                                all_book_odds["away"] = event_odds["Odds"]
                            elif event_odds["Name"] == "X":
                                all_book_odds["draw"] = event_odds["Odds"]
                        elif event_odds["Kind"]["Name"] == "Un/Ov 0,5":
                            if event_odds["Name"] == "Un.0,5":
                                all_book_odds["under_05"] = event_odds["Odds"]
                            elif event_odds["Name"] == "Ov.0,5":
                                all_book_odds["over_05"] = event_odds["Odds"]
                        elif event_odds["Kind"]["Name"] == "Un/Ov 1,5":
                            if event_odds["Name"] == "Un.1,5":
                                all_book_odds["under_15"] = event_odds["Odds"]
                            elif event_odds["Name"] == "Ov.1,5":
                                all_book_odds["over_15"] = event_odds["Odds"]
                        elif event_odds["Kind"]["Name"] == "Un/Ov 2,5":
                            if event_odds["Name"] == "Un.2,5":
                                all_book_odds["under_25"] = event_odds["Odds"]
                            elif event_odds["Name"] == "Ov.2,5":
                                all_book_odds["over_25"] = event_odds["Odds"]
                        elif event_odds["Kind"]["Name"] == "Un/Ov 3,5":
                            if event_odds["Name"] == "Un.3,5":
                                all_book_odds["under_35"] = event_odds["Odds"]
                            elif event_odds["Name"] == "Ov.3,5":
                                all_book_odds["over_35"] = event_odds["Odds"]
                        elif event_odds["Kind"]["Name"] == "Un/Ov 4,5":
                            if event_odds["Name"] == "Un.4,5":
                                all_book_odds["under_45"] = event_odds["Odds"]
                            elif event_odds["Name"] == "Ov.4,5":
                                all_book_odds["over_45"] = event_odds["Odds"]
                        elif event_odds["Kind"]["Name"] == "Goal/No goal":
                            if event_odds["Name"] == "Goal":
                                all_book_odds["goal"] = event_odds["Odds"]
                            elif event_odds["Name"] == "No goal ":
                                all_book_odds["no_goal"] = event_odds["Odds"]

                bf_event = self.pair_match(db, betfair_events, all_book_odds, event_hash, home, away, open_date)
                if not bf_event:
                    print name, open_date, "no bf_event, skipping"
                    if event_hash not in self.bot_data["blacklist"]:
                        self.bot_data["blacklist"].append(event_hash)
                    continue

                print name, open_date, all_book_odds["home"], all_book_odds["draw"], all_book_odds["away"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                event_found = True
                self.save_data(db, event_hash, all_book_odds, bf_event, name, open_date)

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)

        finally:
            if requests_loaded and not event_found and league not in self.bot_data["blacklist_leagues"]:
                print "no event found in league {0}, blacklisting".format(league)
                self.bot_data["blacklist_leagues"].append(league)
            if db:
                db.close()
            sys.stdout.flush()
            sys.stderr.flush()

        return {
            "blacklist":self.bot_data["blacklist"],
            "blacklist_leagues":self.bot_data["blacklist_leagues"],
            "matched_betfair_events":self.matched_betfair_events,
            "matched_events":self.matched_events
        }

    def run(self):
        while True:
            print("round round round round round round round round")
            all_leagues = self.get_leagues()

            with closing(multiprocessing.Pool(processes=8)) as p:
                results = p.map(self.process_league, all_leagues)
                for result in results:
                    if result:
                        self.bot_data["blacklist"].extend([event for event in result["blacklist"] if event not in self.bot_data["blacklist"]])
                        self.bot_data["blacklist_leagues"].extend([league for league in result["blacklist_leagues"] if league not in self.bot_data["blacklist_leagues"]])
                        self.matched_betfair_events.update(result["matched_betfair_events"])
                        self.matched_events.update(result["matched_events"])
                p.terminate()

            sleep(5)

if __name__ == '__main__':
    bot_data = {
        "blacklist": [],
        "blacklist_leagues": [],
        "region":"it",
    }

    t = Chancebet(bot_data)
    t.run()
