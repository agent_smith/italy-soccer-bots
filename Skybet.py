import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
requests.packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Skybet(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "it,en-US;q=0.7,en;q=0.3",
            "Connection": "keep-alive",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
        }

    def get_leagues(self):
        all_leagues = []

        try:
            url = "https://www.skybet.it/catalog/primary/calcio/top/events"
            # main_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
            main_page_request = requests.get(url, headers=self.custom_headers)
            main_page_json = json.loads(main_page_request.content)

            if "data" in main_page_json and "types" in main_page_json["data"] and main_page_json["data"]["types"]:
                for league in main_page_json["data"]["types"]:
                    all_leagues.append(league["id"])

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    url = "https://www.skybet.it/catalog/primary/calcio/{0}".format(league)
                    # league_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                    league_page_request = requests.get(url, headers=self.custom_headers)
                    league_page_json = json.loads(league_page_request.content)

                    if not "data" in league_page_json or not "types" in league_page_json["data"] or not league_page_json["data"]["types"]:
                        if league not in self.bot_data["blacklist_leagues"]:
                            self.bot_data["blacklist_leagues"].append(league)
                        continue

                    self.init_betfair_events()
                    for subleague in league_page_json["data"]["types"]:
                        if "preLiveEvents" not in subleague:
                            continue
                        for event in subleague["preLiveEvents"]:
                            if event["live"]:
                                continue
                            event_id = event["id"]
                            home, away = [team.strip() for team in event["name"].split(" - ")]
                            open_date = (datetime.datetime.strptime(event["startTime"], "%Y-%m-%dT%H:%M:%SZ") + datetime.timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S")
                            name = home + " - " + away
                            event_hash = hashlib.md5(open_date + name).hexdigest()

                            all_book_odds = self.get_empty_odds_dict()

                            if "markets" in event:
                                for market in event["markets"]:
                                    if market["name"] == "1X2":
                                        for outcome in market["outcomes"]:
                                            if outcome["type"] == "H":
                                                all_book_odds["home"] = float(outcome["price"])
                                            if outcome["type"] == "A":
                                                all_book_odds["away"] = float(outcome["price"])
                                            if outcome["type"] == "D":
                                                all_book_odds["draw"] = float(outcome["price"])

                            bf_event = self.pair_match(all_book_odds, event_hash, home, away, open_date)
                            if not bf_event:
                                print name + " no bf_event, skipping"
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue

                            if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":
                                url = "https://www.skybet.it/catalog/event/{0}".format(event_id)
                                # event_page_request = requests.get(url, headers=self.custom_headers, proxies=self.get_proxy())
                                event_page_request = requests.get(url, headers=self.custom_headers)
                                event_page_json = json.loads(event_page_request.content)

                                if "data" in event_page_json and "markets" in event_page_json["data"]:
                                    for market in event_page_json["data"]["markets"]:
                                        if re.search("^Under / Over \d\.\d$", market["name"]):
                                            for outcome in market["outcomes"]:
                                                if outcome["name"] == "Under 0.5":
                                                    all_book_odds["under_05"] = float(outcome["price"])
                                                if outcome["name"] == "Over 0.5":
                                                    all_book_odds["over_05"] = float(outcome["price"])
                                                if outcome["name"] == "Under 1.5":
                                                    all_book_odds["under_15"] = float(outcome["price"])
                                                if outcome["name"] == "Over 1.5":
                                                    all_book_odds["over_15"] = float(outcome["price"])
                                                if outcome["name"] == "Under 2.5":
                                                    all_book_odds["under_25"] = float(outcome["price"])
                                                if outcome["name"] == "Over 2.5":
                                                    all_book_odds["over_25"] = float(outcome["price"])
                                                if outcome["name"] == "Under 3.5":
                                                    all_book_odds["under_35"] = float(outcome["price"])
                                                if outcome["name"] == "Over 3.5":
                                                    all_book_odds["over_35"] = float(outcome["price"])
                                                if outcome["name"] == "Under 4.5":
                                                    all_book_odds["under_45"] = float(outcome["price"])
                                                if outcome["name"] == "Over 4.5":
                                                    all_book_odds["over_45"] = float(outcome["price"])
                                        if market["name"] == "Goal/No Goal":
                                            for outcome in market["outcomes"]:
                                                if outcome["name"] == "Goal":
                                                    all_book_odds["goal"] = float(outcome["price"])
                                                if outcome["name"] == "Nessun Goal":
                                                    all_book_odds["no_goal"] = float(outcome["price"])

                            if not self.check_rating(all_book_odds, bf_event):
                                if event_hash not in self.bot_data["blacklist"]:
                                    self.bot_data["blacklist"].append(event_hash)
                                continue

                            print name, open_date, all_book_odds["home"], all_book_odds["away"], all_book_odds["draw"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                            event_found = True
                            self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "127.0.0.1",
    "blacklist": [],
    "blacklist_leagues": [],
    "region": "it",
}

t = Skybet(bot_data)
t.run()
