import datetime
import hashlib
import os
import random
import re
import sys
import traceback
import urllib

import dateparser
import mysql.connector
import requests
from bs4 import BeautifulSoup
from fuzzywuzzy import fuzz
from unidecode import unidecode

class Common(object):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        if os.uname()[1] in ["oddsmatcher.localdomain", "spain-odds"]:
            self.bot_data.update({"username":"root", "password":"welSOFT1010", "database":"ninjabet", "host":"127.0.0.1"})
        else:
            self.bot_data.update({"username":"83fdd02e7115cd2", "password":"4e59d10211ad4d477b3cc1c1f7a67e28343185330", "database":"ninjabet"})
            if self.bot_data["region"] == "it":
                self.bot_data.update({"host":"188.166.34.148"})
            elif self.bot_data["region"] == "es":
                self.bot_data.update({"host":"174.138.4.2"})
        self.init_proxies()
        self.init_db()
        self.init_book_data()
        self.init_matched_events()
        self.matched_betfair_events = {}

    def __del__(self):
        if self.db:
            self.db.close()

    def init_proxies(self):
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "proxies.txt"), "r") as f:
            self.proxies = [line.strip() for line in f.readlines()]

    def init_db(self):
        self.db = mysql.connector.connect(user=self.bot_data["username"], password=self.bot_data["password"], database=self.bot_data["database"], host=self.bot_data["host"])
        cur = self.db.cursor()
        cur.execute("SET NAMES 'utf8'")
        cur.execute("SET CHARACTER SET utf8")
        cur.close()

    def get_db(self):
        db = mysql.connector.connect(user=self.bot_data["username"], password=self.bot_data["password"], database=self.bot_data["database"], host=self.bot_data["host"])
        cur = db.cursor()
        cur.execute("SET NAMES 'utf8'")
        cur.execute("SET CHARACTER SET utf8")
        cur.close()

        return db

    def init_book_data(self):
        sql = "SELECT id, name, full_name, website, coupon_url FROM providers WHERE name LIKE '{0}'".format(self.bot_data["book_name"].lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()

        self.bot_data["book_data"] = {
            "id": str(rows[0][0]),
            "name": str(rows[0][1]),
            "full_name": str(rows[0][2]),
            "website": str(rows[0][3]),
            "coupon_url": str(rows[0][4]),
        }

    def init_matched_events(self):
        sql = "SELECT betfair, bookie FROM matched_events WHERE bookie_name LIKE '{0}'".format(self.bot_data["book_name"].lower())
        cur = self.db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()

        self.matched_events = {}
        for row in rows:
            self.matched_events[row[1]] = row[0]

    def get_betfair_events(self, db):
        sql = "SELECT o.id, name, home, away, open_date, home_odds, home_price, away_price, draw_price, o_25, o_25_price, u_25, u_25_price, goal, goal_price, no_goal, no_goal_price, country_code, competition, away_odds, draw_odds, market_id, market_id2, market_id3, o_05, o_05_price, u_05, u_05_price, o_15, o_15_price, u_15, u_15_price, o_35, o_35_price, u_35, u_35_price, o_45, o_45_price, u_45, u_45_price, mid_ou_05, mid_ou_15, mid_ou_35, mid_ou_45 FROM bf_match_odds AS o NATURAL JOIN bf_events AS e"
        cur = db.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()

        betfair_events = {}
        for row in rows:
            event = {
                "id": str(row[0]),
                "name": row[1],
                "home": row[2],
                "away": row[3],
                "open_date": str(row[4]),
                "home_odds": str(row[5]),
                "home_price": str(row[6]),
                "away_price": str(row[7]),
                "draw_price": str(row[8]),
                "goal_odds": str(row[13]),
                "goal_price": str(row[14]),
                "no_goal_odds": str(row[15]),
                "no_goal_price": str(row[16]),
                "country_code": str(row[17]),
                "competition": str(row[18]),
                "away_odds": str(row[19]),
                "draw_odds": str(row[20]),
                "over_05": str(row[24]),
                "over_05_price": str(row[25]),
                "under_05": str(row[26]),
                "under_05_price": str(row[27]),
                "over_15": str(row[28]),
                "over_15_price": str(row[29]),
                "under_15": str(row[30]),
                "under_15_price": str(row[31]),
                "over_25": str(row[9]),
                "over_25_price": str(row[10]),
                "under_25": str(row[11]),
                "under_25_price": str(row[12]),
                "over_35": str(row[32]),
                "over_35_price": str(row[33]),
                "under_35": str(row[34]),
                "under_35_price": str(row[35]),
                "over_45": str(row[36]),
                "over_45_price": str(row[37]),
                "under_45": str(row[38]),
                "under_45_price": str(row[39]),
                "mid_1x2":str(row[21]),
                "mid_gng":str(row[23]),
                "mid_ou_05": str(row[40]),
                "mid_ou_15": str(row[41]),
                "mid_ou_25": str(row[22]),
                "mid_ou_35": str(row[42]),
                "mid_ou_45": str(row[43]),
            }
            betfair_events[row[0]] = event

        return betfair_events

    def get_empty_odds_dict(self):
        return {
            "home": "",
            "draw": "",
            "away": "",
            "under_05": "",
            "over_05": "",
            "under_15": "",
            "over_15": "",
            "under_25": "",
            "over_25": "",
            "under_35": "",
            "over_35": "",
            "under_45": "",
            "over_45": "",
            "goal": "",
            "no_goal": "",
        }

    def save_data(self, db, book_event_id, all_book_odds, bf_event, book_event_name, book_open_date):
        book_name = self.bot_data["book_name"]
        book_data = self.bot_data["book_data"]
        region = self.bot_data.get("region")

        sql = ""
        for bet_type, book_odds in all_book_odds.items():
            if not book_odds:
                continue
            if bet_type == "home":
                market = "1x2"
                market_id_string = "mid_1x2"
                bf_string = "home_odds"
                bf_string_price = "home_price"
                bet = bf_event[bet_type]
            elif bet_type == "away":
                market = "1x2"
                market_id_string = "mid_1x2"
                bf_string = "away_odds"
                bf_string_price = "away_price"
                bet = bf_event[bet_type]
            elif bet_type == "draw":
                market = "1x2"
                market_id_string = "mid_1x2"
                bf_string = "draw_odds"
                bf_string_price = "draw_price"
                if region == "it":
                    bet = "Pareggio"
                if region == "es":
                    bet = "Empate"
            elif bet_type == "goal":
                market = "GOAL/NOGOAL"
                market_id_string = "mid_gng"
                bf_string = "goal_odds"
                bf_string_price = "goal_price"
                bet = "GOAL"
            elif bet_type == "no_goal":
                market = "GOAL/NOGOAL"
                market_id_string = "mid_gng"
                bf_string = "no_goal_odds"
                bf_string_price = "no_goal_price"
                bet = "NO GOAL"
            elif bet_type.lower().startswith("under_") or bet_type.lower().startswith("over_"):
                market = "OVER/UNDER"
                goal_num = bet_type.split("_")[1]
                market_id_string = "mid_ou_" + goal_num
                bf_string = bet_type.split("_")[0] + "_" + goal_num
                bf_string_price = "{0}_{1}_price".format(bet_type.split("_")[0], goal_num)
                bet = bet_type.split("_")[0].upper() + " {0}.{1}".format(goal_num[0], goal_num[1])
            else:
                raise Exception("Invalid bet_type: " + bet_type)

            if region == "es" and market in ["OVER/UNDER", "GOAL/NOGOAL"]:
                key = book_name.lower() + "_" + book_event_id + "_" + bet_type.lower()
                sql += "INSERT into bookie_odds (id, event_name, bet, market_type, odds_provider_id, odds_provider, odds, open_date, competition, country_code, bookie_bet_url) values ('{0}', '{1}', '{2}', '{3}', {4}, '{5}', {6}, '{7}', '{8}', '{9}', '{10}') ON DUPLICATE KEY UPDATE update_time=NOW(), odds={6}; ".format(key, bf_event["name"], bet, market, book_data["id"], book_data["full_name"], book_odds, bf_event["open_date"], bf_event["competition"], bf_event["country_code"], book_data["coupon_url"])

            bf_odds = bf_event.get(bf_string, None)
            if not bf_odds or bf_odds == "None":
                continue

            bf_stake = (float(book_odds) * 100) / (float(bf_odds) - 0.05)
            profit = (bf_stake * (1 - 0.05)) - 100
            rating = round(100 + profit, 2)

            back_odds = float(book_odds)
            lay_odds = float(bf_odds)
            lay_commission = 0.05
            back_stake = 100
            refund = 25

            lay_stake = ((back_stake * back_odds) - refund) / (lay_odds - lay_commission)
            net_result_lay_back_win = refund - back_stake
            net_result_back_back_win = ((back_stake * back_odds) - back_stake)
            liability = lay_stake * lay_odds
            net_result_back_lay_win = (-1 * liability)
            net_result_lay_lay_win = (lay_stake * (1 - lay_commission))
            lay_win_profit = net_result_lay_back_win + net_result_lay_lay_win
            back_win_profit = net_result_back_back_win + net_result_back_lay_win
            snr = lay_win_profit
            if snr > back_win_profit:
                snr = back_win_profit
            snr = round((float(snr * 100) / refund), 2)

            key = book_name.lower() + "_" + book_event_id + "_" + bet_type + "_" + bf_event["id"]

            sql += "INSERT INTO compare_table (id, event_name, bet, market_type, rating, odds_provider_fk, odds_provider, back_odds, lay_odds, availability, opendate, snr_rating, bookie_bet_url, betfair_bet_url, competition, country_code, exchange, bf_id, bookie_event_name, opendate_bookie) VALUES ('{0}', '{1}', '{2}', '{3}', {4}, {5}, '{6}', {7}, {8}, {9}, DATE_SUB('{10}', INTERVAL 1 HOUR), {11}, '{12}', '{13}', '{14}', '{15}', '{16}', {24}, '{25}', '{26}') ON DUPLICATE KEY UPDATE update_time=NOW(), snr_rating={17}, rating={18}, back_odds={19}, lay_odds={20}, availability={21}, bookie_bet_url='{22}', betfair_bet_url='{23}'; ".format(key, db.converter.escape(bf_event["name"]), db.converter.escape(bet), market, rating, book_data["id"], book_data["full_name"], book_odds, bf_odds, bf_event[bf_string_price], bf_event["open_date"], snr, book_data["coupon_url"], "https://www.betfair." + region + "/exchange/football/market?id=" + bf_event[market_id_string], bf_event["competition"], bf_event["country_code"], "betfair", snr, rating, book_odds, bf_odds, bf_event[bf_string_price], book_data["coupon_url"], "https://www.betfair." + region + "/exchange/football/market?id=" + bf_event[market_id_string], bf_event["id"], book_event_name, book_open_date)

        if sql:
            self.commit_sql(db, sql)

    def commit_sql(self, db, sql):
        cur = db.cursor()
        for result in cur.execute(sql, multi=True):
            pass
        db.commit()
        cur.close()

    def get_proxy(self):
        random_proxy = random.choice(self.proxies)
        return {"http":"http://ninjabet:NINJAfrancavilla1987@" + random_proxy, "https":"http://ninjabet:NINJAfrancavilla1987@" + random_proxy}

    def check_rating(self, all_book_odds, bf_event):
        book_to_bf_bet = {
            "home": "home_odds",
            "away": "away_odds",
            "draw": "draw_odds",
            "under_05": "under_05",
            "over_05": "over_05",
            "under_15": "under_15",
            "over_15": "over_15",
            "under_25": "under_25",
            "over_25": "over_25",
            "under_35": "under_35",
            "over_35": "over_35",
            "under_45": "under_45",
            "over_45": "over_45",
            "goal": "goal_odds",
            "no_goal": "no_goal_odds",
        }

        try:
            for bet in self.get_empty_odds_dict().keys():
                book_odd = all_book_odds[bet]
                bf_odd = bf_event[book_to_bf_bet[bet]]
                if book_odd and bf_odd and bf_odd != "None":
                    bf_stake = (float(book_odd) * 100) / (float(bf_odd) - 0.05)
                    profit = (bf_stake * (1 - 0.05)) - 100
                    rating = round(100 + profit, 2)
                    if rating > 105:
                        return False
        except Exception:
            return False

        return True

    def pair_match(self, db, betfair_events, all_book_odds, book_event_id, book_home, book_away, book_open_date):
        if book_event_id in self.bot_data["blacklist"]:
            return None
        region = self.bot_data.get("region")
        book_name = self.bot_data["book_name"]

        updating = False
        bf_event = None

        if book_event_id in self.matched_betfair_events and self.matched_betfair_events[book_event_id] in betfair_events:
            bf_event =  betfair_events[self.matched_betfair_events[book_event_id]]
        else:
            book_n = book_home.strip().upper() + " v " + book_away.strip().upper()
            if book_n in self.matched_events:
                book_home, book_away = self.matched_events[book_n].split(" v ")
                updating = True
            for bf_id, bf_row in betfair_events.items():
                if abs(datetime.datetime.strptime(book_open_date, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(bf_row["open_date"], "%Y-%m-%d %H:%M:%S")) > datetime.timedelta(hours=1):
                    continue
                if book_home.lower().strip() == bf_row["home"].lower().strip() and book_away.lower().strip() == bf_row["away"].lower().strip() and self.check_rating(all_book_odds, bf_row):
                    bf_event = bf_row
                    self.matched_betfair_events[book_event_id] = bf_id
                    break

        if not bf_event and not updating:
            try:
                url = "https://www.betfair.{0}/exchange/search?query={1}".format(region, urllib.quote_plus("{0} {1}".format(unidecode(book_home), unidecode(book_away)).encode("UTF-8")))
                bf_search_page = requests.get(url, proxies=self.get_proxy(), verify=False, timeout=4)
                bf_search_soup = BeautifulSoup(bf_search_page.content, "lxml")
                exchange_results = bf_search_soup.find_all("a", {"class":"mod-searchresults-ebs-link"})
                if not exchange_results:
                    return None
                exchange_events = []
                for exchange_event in exchange_results:
                    bf_date_text = re.search("\| (\d+ \w{3} \d{4}, \d{2}:\d{2})", exchange_event.parent.text)
                    if bf_date_text:
                        bf_open_date = dateparser.parse(bf_date_text.group(1)).strftime('%Y-%m-%d %H:%M:%S')
                    else:
                        continue
                    if abs(datetime.datetime.strptime(book_open_date, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(bf_open_date, "%Y-%m-%d %H:%M:%S")) > datetime.timedelta(hours=1):
                        continue
                    bf_name = exchange_event.text.strip().replace(" v ", " - ").split(" - ")
                    if len(bf_name) != 2:
                        continue
                    bf_home = bf_name[0].strip()
                    bf_away = bf_name[1].strip()
                    comparison_rating = fuzz.partial_ratio(book_home.lower() + " " + book_away.lower(), bf_home.lower() + " " + bf_away.lower())
                    exchange_events.append({"comparison_rating":comparison_rating, "bf_open_date":bf_open_date, "bf_home":bf_home, "bf_away":bf_away})

                if exchange_events:
                    best_match = sorted(exchange_events, key=lambda k: k["comparison_rating"], reverse=True)[0]
                    if best_match and best_match["comparison_rating"] >= 55:
                        bf_home = best_match["bf_home"]
                        bf_away = best_match["bf_away"]
                        bf_open_date = best_match["bf_open_date"]
                        for bf_id, bf_row in betfair_events.items():
                            if bf_home.lower() == bf_row["home"].lower() and bf_away.lower() == bf_row["away"].lower() and abs(datetime.datetime.strptime(bf_open_date, "%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(bf_row["open_date"], "%Y-%m-%d %H:%M:%S")) <= datetime.timedelta(hours=1) and self.check_rating(all_book_odds, bf_row):
                                bf_event = bf_row
                                self.matched_events[book_home.upper() + " v " + book_away.upper()] = bf_home.upper() + " v " + bf_away.upper()
                                self.matched_betfair_events[book_event_id] = bf_id
                                bf_event_id = hashlib.md5(bf_home + bf_away + book_name.upper()).hexdigest()
                                sql = "INSERT IGNORE INTO matched_events (id, betfair, bookie, bookie_name) VALUES ('{0}', '{1}', '{2}', '{3}'); ".format(bf_event_id, db.converter.escape(bf_home.upper() + " v " + bf_away.upper()), db.converter.escape(book_home.upper() + " v " + book_away.upper()), book_name.upper())
                                self.commit_sql(db, sql)
                                break
            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                traceback.print_exception(exc_type, exc_obj, exc_tb)

        return bf_event
