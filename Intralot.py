import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Intralot(Common):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        Common.__init__(self, bot_data)
        self.custom_headers = {
            "Accept":"*/*",
            "Accept-Encoding":"gzip, deflate",
            "Accept-Language":"en-US,en;q=0.8",
            "Connection":"keep-alive",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
            "X-Requested-With":"XMLHttpRequest",
        }
        self.proxy_data = {}

    def get_initialized_proxy(self):
        proxy = self.get_proxy()

        initialized_proxy = self.proxy_data.get(frozenset(proxy), None)
        if not initialized_proxy:
            url = "https://www.intralot.it/"
            csrf_token_page_request = requests.get(url, headers=self.custom_headers, proxies=proxy)
            csrf_token_page_soup = BeautifulSoup(csrf_token_page_request.content, "lxml")

            csrf_section = csrf_token_page_soup.find("input", id=re.compile(".*_Csrftkn"))
            if csrf_section:
                csrf_token = csrf_section["value"]

            initialized_proxy = {"proxy":proxy, "csrf_token":csrf_token, "cookies":csrf_token_page_request.cookies}
            self.proxy_data[frozenset(proxy)] = initialized_proxy

        return initialized_proxy

    def save_cookies(self, cookies, proxy):
        self.proxy_data[frozenset(proxy["proxy"])]["cookies"] = cookies

    def get_leagues(self):
        all_leagues = []

        try:
            proxy = self.get_initialized_proxy()
            url = "https://www.intralot.it/AJAX/menu.aspx?Ctkn={0}".format(proxy["csrf_token"])
            main_page_request = requests.get(url, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
            self.save_cookies(main_page_request.cookies, proxy)
            main_page_json = json.loads(main_page_request.content)

            for item in main_page_json["MenuItem"]:
                if item["ItemId"] == 1:
                    for itemSub in item["ItemMenuSub"]:
                        if "ItemMenuSub" in itemSub and itemSub["ItemMenuSub"] != None and "special" not in itemSub["ItemDescription"].lower():
                            for itemSub2 in itemSub["ItemMenuSub"]:
                                all_leagues.append(str(itemSub2["ItemId"]))

        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            traceback.print_exception(exc_type, exc_obj, exc_tb)
            sleep(10)

        return all_leagues

    def run(self):
        while True:
            all_leagues = self.get_leagues()

            for league in all_leagues:
                if league in self.bot_data["blacklist_leagues"]:
                    continue

                event_found = False
                try:
                    proxy = self.get_initialized_proxy()
                    postdata = {'paMetodo':'getAvvenimenti', 'paMarket':'{"selectable-item":{"element":{},"$element":{"0":{},"context":{},"length":1},"left":356.890625,"top":375.109375,"right":551.890625,"bottom":406.109375,"startselected":false,"selected":false,"selecting":false,"unselecting":false},"info35":false,"scorecast":false,"defaultbetid":-1,"idtournament":' + league + ',"resultsnumber":3,"betid":"3_0","sportid":1}'}
                    url = "https://www.intralot.it/AJAX/Scommesse2014/DettaglioManifestazione.aspx?Ctkn={0}".format(proxy["csrf_token"])
                    league_page_request = requests.post(url, data=postdata, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                    self.save_cookies(league_page_request.cookies, proxy)

                    if not league_page_request.content:
                        if league not in self.bot_data["blacklist_leagues"]:
                            self.bot_data["blacklist_leagues"].append(league)
                        continue

                    league_page_json = json.loads(league_page_request.content)

                    self.init_betfair_events()
                    for j in league_page_json:
                        if j["Event"]["isXXLive"]:
                            continue
                        name = self.db.converter.escape(j["Event"]["EventDescription"])
                        home =  self.db.converter.escape(j["Event"]["Team1Description"])
                        away = self.db.converter.escape(j["Event"]["Team2Description"])
                        open_date = datetime.datetime.strptime(j["Event"]["EventDate"], "%Y-%m-%dT%H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
                        event_hash = hashlib.md5(open_date + name).hexdigest()

                        all_book_odds = self.get_empty_odds_dict()
                        all_book_odds["home"] = float(j["Odd"][0]["OddDescription"]) / 100
                        all_book_odds["draw"] = float(j["Odd"][1]["OddDescription"]) / 100
                        all_book_odds["away"] = float(j["Odd"][2]["OddDescription"]) / 100

                        bf_event = self.pair_match(all_book_odds,event_hash, home, away, open_date)
                        if not bf_event:
                            print name + " no bf_event, skipping"
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        if all_book_odds["home"] == "" or all_book_odds["away"] == "" or all_book_odds["draw"] == "":
                            continue

                        if bf_event["over_05"] != "None" or bf_event["under_05"] != "None" or bf_event["over_15"] != "None" or bf_event["under_15"] != "None" or bf_event["over_25"] != "None" or bf_event["under_25"] != "None" or bf_event["over_35"] != "None" or bf_event["under_35"] != "None" or bf_event["over_45"] != "None" or bf_event["under_45"] != "None" or bf_event["goal_odds"] != "None" or bf_event["no_goal_odds"] != "None":

                            proxy = self.get_initialized_proxy()
                            postdata = {'paMetodo':'getDettaglioEvento_quote', 'paMarket':'{"selectable-item":{"element":{},"$element":{"0":{},"context":{},"length":1},"left":358.734375,"top":311.828125,"right":447.734375,"bottom":347.828125,"startselected":false,"selected":false,"selecting":false,"unselecting":false},"groupid":125,"betidlistscorecast":0,"betidlist":0,"idevento":' + str(j["Event"]["IntralotEventId"]) + ',"idtournament":' + league + ',"idsport":1}'}
                            url = "https://www.intralot.it/AJAX/Scommesse2014/DettaglioEvento.aspx?Ctkn={0}".format(proxy["csrf_token"])
                            event_page_request = requests.post(url, data=postdata, headers=self.custom_headers, proxies=proxy["proxy"], cookies=proxy["cookies"])
                            self.save_cookies(event_page_request.cookies, proxy)
                            event_page_json = json.loads(event_page_request.content)

                            if "Odd" in event_page_json:
                                for o1 in event_page_json["Odd"]:
                                    if o1["BetDescription"] == "GOAL NOGOAL" and o1["ResultDescription"] == "GOAL":
                                        all_book_odds["goal"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "GOAL NOGOAL" and o1["ResultDescription"] == "NOGOAL":
                                        all_book_odds["no_goal"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 0,5" and o1["ResultDescription"] == "OVER":
                                        all_book_odds["over_05"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 0,5" and o1["ResultDescription"] == "UNDER":
                                        all_book_odds["under_05"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 1,5" and o1["ResultDescription"] == "OVER":
                                        all_book_odds["over_15"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 1,5" and o1["ResultDescription"] == "UNDER":
                                        all_book_odds["under_15"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 2,5" and o1["ResultDescription"] == "OVER":
                                        all_book_odds["over_25"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 2,5" and o1["ResultDescription"] == "UNDER":
                                        all_book_odds["under_25"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 3,5" and o1["ResultDescription"] == "OVER":
                                        all_book_odds["over_35"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 3,5" and o1["ResultDescription"] == "UNDER":
                                        all_book_odds["under_35"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 4,5" and o1["ResultDescription"] == "OVER":
                                        all_book_odds["over_45"] = float(o1["OddDescription"]) / 100
                                    elif o1["BetDescription"] == "U/O 4,5" and o1["ResultDescription"] == "UNDER":
                                        all_book_odds["under_45"] = float(o1["OddDescription"]) / 100

                        if not self.check_rating(all_book_odds, bf_event):
                            if event_hash not in self.bot_data["blacklist"]:
                                self.bot_data["blacklist"].append(event_hash)
                            continue

                        print name,open_date, all_book_odds["home"], all_book_odds["draw"], all_book_odds["away"], all_book_odds["under_05"], all_book_odds["over_05"], all_book_odds["under_15"], all_book_odds["over_15"], all_book_odds["under_25"], all_book_odds["over_25"], all_book_odds["under_35"], all_book_odds["over_35"], all_book_odds["under_45"], all_book_odds["over_45"], all_book_odds["goal"], all_book_odds["no_goal"]

                        event_found = True
                        self.save_data(event_hash, all_book_odds, bf_event)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

                finally:
                    if event_found == False and league not in self.bot_data["blacklist_leagues"]:
                        print "no event found in league {0}, blacklisting".format(league)
                        self.bot_data["blacklist_leagues"].append(league)

            sleep(random.randint(1, 10))

bot_data = {
    "host": "188.166.34.148",
    "blacklist": [],
    "blacklist_leagues": [],
    "region":"it",
}

t = Intralot(bot_data)
t.run()
