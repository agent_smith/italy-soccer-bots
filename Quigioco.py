import datetime
import hashlib
import json
import os
import random
import re
import sys
import traceback
from time import sleep, time

import dateparser
import requests
from bs4 import BeautifulSoup

from common import *

reload(sys)
sys.setdefaultencoding("UTF-8")

class Quigioco(object):
    def __init__(self, bot_data):
        self.bot_data = bot_data
        self.bot_data["book_name"] = self.__class__.__name__
        self.custom_headers = {
            "Accept":"application/json, text/plain, */*",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"it,en-US;q=0.7,en;q=0.3",
            "Connection":"keep-alive",
            "Content-Type":"application/json;charset=utf-8",
            "Host":"api.quigioco.it",
            "Origin":"https://www.quigioco.it",
            "Referer":"https://www.quigioco.it/bet",
            "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0",
        }

    def get_leagues(self):
        url = "https://api.quigioco.it/api/Palinsesto/GetPalinsesto2"
        post_data = {"Jolly":False, "Special":False, "indexDateTo":0, "DateFrom":None, "DateTo":None}
        main_page_request = requests.post(url, json=post_data, headers=self.custom_headers)
        main_page_json = json.loads(main_page_request.content)

        all_leagues = []
        for sport in main_page_json:
            if sport["sportId"] == 1:
                for country in sport["countries"]:
                    for league in country["groups"]:
                        if league["Group_Id"] not in all_leagues:
                            all_leagues.append(league["Group_Id"])

        return all_leagues

    def run(self):
        self.bot_data["db"] = get_database(self.bot_data)
        self.bot_data["book_data"] = get_book_data(self.bot_data)
        self.bot_data["matched_events"] = get_matched_events(self.bot_data)
        blacklist = self.bot_data["blacklist"]

        while True:
            try:
                all_leagues = self.get_leagues()
            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                traceback.print_exception(exc_type, exc_obj, exc_tb)
                continue

            for league in all_leagues:
                try:
                    url = "https://api.quigioco.it/api/Palinsesto/GetManifestazione"

                    post_data = {"eventType":0, "groupId":league, "GroupsId":None, "IndexDateTo":0, "isQuery":False, "preCache":False, "typeOnly":False}
                    league_page_request = requests.post(url, json=post_data, headers=self.custom_headers, proxies=get_proxy())
                    league_page_json = json.loads(league_page_request.content)

                    post_data = {"eventType":0, "groupId":league, "IndexDateTo":0, "isQuery":False, "typeHash":"4f581d09-d3ce-b401-4265-4dd8c23b3fc9", "typeOnly":True}
                    league_page_05_request = requests.post(url, json=post_data, headers=self.custom_headers, proxies=get_proxy())
                    league_page_05_json = json.loads(league_page_05_request.content)

                    post_data = {"eventType":0, "groupId":league, "IndexDateTo":0, "isQuery":False, "typeHash":"71403d01-ec66-a54f-6eb1-e1f8cbe183b9", "typeOnly":True}
                    league_page_15_request = requests.post(url, json=post_data, headers=self.custom_headers, proxies=get_proxy())
                    league_page_15_json = json.loads(league_page_15_request.content)

                    post_data = {"eventType":0, "groupId":league, "IndexDateTo":0, "isQuery":False, "typeHash":"d267d7b6-edf8-215d-a44b-0e5886680cb9", "typeOnly":True}
                    league_page_35_request = requests.post(url, json=post_data, headers=self.custom_headers, proxies=get_proxy())
                    league_page_35_json = json.loads(league_page_35_request.content)

                    post_data = {"eventType":0, "groupId":league, "IndexDateTo":0, "isQuery":False, "typeHash":"b0175104-a1e0-241a-2b97-65e79cbf113f", "typeOnly":True}
                    league_page_45_request = requests.post(url, json=post_data, headers=self.custom_headers, proxies=get_proxy())
                    league_page_45_json = json.loads(league_page_45_request.content)

                    try:
                        league_data = league_page_json[0]
                    except IndexError:
                        continue

                    if "7ec8cbec-5c4b-fee2-2830-8fd9f2a7baf3" not in [event_type["typeId"] for event_type in league_data["types"]]:
                        continue

                    try:
                        league_data_05 = league_page_05_json[0]
                        league_data_15 = league_page_15_json[0]
                        league_data_35 = league_page_35_json[0]
                        league_data_45 = league_page_45_json[0]
                    except IndexError:
                        continue
                    
                    self.init_betfair_events()
                    for date in league_data["dates"]:
                        for event in date["matches"]:
                            event_id = event["id"]
                            open_date = datetime.datetime.strptime(event["matchTime"], "%Y-%m-%dT%H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
                            home = event["hteam"]
                            away = event["ateam"]
                            name = home + " - " + away
                            event_hash = hashlib.md5(open_date + name).hexdigest()

                            bf_event = pair_match(self.bot_data, event_hash, home, away, open_date)
                            if not bf_event:
                                if event_hash not in blacklist:
                                    blacklist.append(event_hash)
                                continue

                            home_odds = None
                            away_odds = None
                            draw_odds = None
                            under05_odds = None
                            over05_odds = None
                            under15_odds = None
                            over15_odds = None
                            under25_odds = None
                            over25_odds = None
                            under35_odds = None
                            over35_odds = None
                            under45_odds = None
                            over45_odds = None
                            goal_odds = None
                            no_goal_odds = None
                            for outcome_section in league_data["data"]:
                                if outcome_section["matchId"] == event_id:
                                    for outcome in outcome_section["outcomes"]:
                                        if outcome["outcomeId"] == "d8643d3a-8080-1094-1810-9fbf97ff48d7" and outcome["bet_Stato"] != "chiuso":
                                            home_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "e1ae8fa1-f7ec-74fb-1224-418f7bafb744" and outcome["bet_Stato"] != "chiuso":
                                            away_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "70d0925a-c237-f0f2-1e5e-753b62df80ed" and outcome["bet_Stato"] != "chiuso":
                                            draw_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "1a9223d5-8cc6-46ef-06ee-15a234d04809" and outcome["bet_Stato"] != "chiuso":
                                            under25_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "415ff4c8-1228-92cd-ea89-aa8069780b0e" and outcome["bet_Stato"] != "chiuso":
                                            over25_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "0aa8aed7-f434-d65e-8c43-f256f34e7f86" and outcome["bet_Stato"] != "chiuso":
                                            goal_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "a6d6e835-ee26-c427-ba05-c4ac812b0b86" and outcome["bet_Stato"] != "chiuso":
                                            no_goal_odds = float(outcome["odd"])

                            for outcome_section in league_data_05["data"]:
                                if outcome_section["matchId"] == event_id:
                                    for outcome in outcome_section["outcomes"]:
                                        if outcome["outcomeId"] == "278aa1cb-49fa-eb55-01e2-4a7a5eb2e1e1" and outcome["bet_Stato"] != "chiuso":
                                            under05_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "b4948a1a-1c1e-8fe5-5e53-ff5f3ed71ac8" and outcome["bet_Stato"] != "chiuso":
                                            over05_odds = float(outcome["odd"])

                            for outcome_section in league_data_15["data"]:
                                if outcome_section["matchId"] == event_id:
                                    for outcome in outcome_section["outcomes"]:
                                        if outcome["outcomeId"] == "531a4b07-43e9-427b-ce57-304d21c23092" and outcome["bet_Stato"] != "chiuso":
                                            under15_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "0f0a1e04-9071-b6dc-d66a-fccfb5373752" and outcome["bet_Stato"] != "chiuso":
                                            over15_odds = float(outcome["odd"])

                            for outcome_section in league_data_35["data"]:
                                if outcome_section["matchId"] == event_id:
                                    for outcome in outcome_section["outcomes"]:
                                        if outcome["outcomeId"] == "1b809041-87f6-4714-c661-5de5dbde9cfd" and outcome["bet_Stato"] != "chiuso":
                                            under35_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "07721102-d6c1-5f6c-68c0-4d6166d86d38" and outcome["bet_Stato"] != "chiuso":
                                            over35_odds = float(outcome["odd"])

                            for outcome_section in league_data_45["data"]:
                                if outcome_section["matchId"] == event_id:
                                    for outcome in outcome_section["outcomes"]:
                                        if outcome["outcomeId"] == "5928fa0c-c9f8-d69e-8c64-2334735bcaf6" and outcome["bet_Stato"] != "chiuso":
                                            under45_odds = float(outcome["odd"])
                                        if outcome["outcomeId"] == "3589d885-6485-b458-db3f-bd423d0975be" and outcome["bet_Stato"] != "chiuso":
                                            over45_odds = float(outcome["odd"])

                            # print name, open_date, home_odds, draw_odds, away_odds, under05_odds, over05_odds, under15_odds, over15_odds, under25_odds, over25_odds, under35_odds, over35_odds, under45_odds, over45_odds, goal_odds, no_goal_odds

                            sql = ""
                            if home_odds:
                                sql += save_data(self.bot_data, event_hash, home_odds, bf_event, "home")
                            if away_odds:
                                sql += save_data(self.bot_data, event_hash, away_odds, bf_event, "away")
                            if draw_odds:
                                sql += save_data(self.bot_data, event_hash, draw_odds, bf_event, "draw")
                            if under05_odds:
                                sql += save_data(self.bot_data, event_hash, under05_odds, bf_event, "under05")
                            if over05_odds:
                                sql += save_data(self.bot_data, event_hash, over05_odds, bf_event, "over05")
                            if under15_odds:
                                sql += save_data(self.bot_data, event_hash, under15_odds, bf_event, "under15")
                            if over15_odds:
                                sql += save_data(self.bot_data, event_hash, over15_odds, bf_event, "over15")
                            if under25_odds:
                                sql += save_data(self.bot_data, event_hash, under25_odds, bf_event, "under25")
                            if over25_odds:
                                sql += save_data(self.bot_data, event_hash, over25_odds, bf_event, "over25")
                            if under35_odds:
                                sql += save_data(self.bot_data, event_hash, under35_odds, bf_event, "under35")
                            if over35_odds:
                                sql += save_data(self.bot_data, event_hash, over35_odds, bf_event, "over35")
                            if under45_odds:
                                sql += save_data(self.bot_data, event_hash, under45_odds, bf_event, "under45")
                            if over45_odds:
                                sql += save_data(self.bot_data, event_hash, over45_odds, bf_event, "over45")
                            if goal_odds:
                                sql += save_data(self.bot_data, event_hash, goal_odds, bf_event, "goal")
                            if no_goal_odds:
                                sql += save_data(self.bot_data, event_hash, no_goal_odds, bf_event, "no_goal")

                            if sql:
                                commit_changes(self.bot_data, sql)

                    sleep(random.randint(4, 10))

                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    traceback.print_exception(exc_type, exc_obj, exc_tb)

            sleep(random.randint(1, 10))

bot_data = {
    'username': '83fdd02e7115cd2',
    'password': '4e59d10211ad4d477b3cc1c1f7a67e28343185330',
    "database": "ninjabet",
    "host": "188.166.34.148",
    "blacklist": [],
}

t = Quigioco(bot_data)
t.run()
